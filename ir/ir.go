package ir

import (
	"discern/internal/db"
	"discern/internal/id"
	"discern/internal/util"
	"errors"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
)

type Instruction struct {
	Id         id.UUID   `json:"id"`
	Address    Address   `json:"address"`
	Size       uint      `json:"size"`
	Mnemonic   string    `json:"mnemonic"`
	Operations []id.UUID `json:"operations"`
}

type MemoryPermission string

const (
	READ               MemoryPermission = "READ"
	WRITE              MemoryPermission = "WRITE"
	EXECUTE            MemoryPermission = "EXECUTE"
	READ_WRITE         MemoryPermission = "READ_WRITE"
	READ_WRITE_EXECUTE MemoryPermission = "READ_WRITE_EXECUTE"
	WRITE_EXECUTE      MemoryPermission = "WRITE_EXECUTE"
	READ_EXECUTE       MemoryPermission = "READ_EXECUTE"
)

type Endianness string

const (
	LITTLE Endianness = "LITTLE"
	BIG    Endianness = "BIG"
)

type Section struct {
	Id           id.UUID          `json:"id"`
	Name         string           `json:"name"`
	Address      Address          `json:"address"`
	Size         uint64           `json:"size"`
	Permissions  MemoryPermission `json:"permissions"`
	Data         []byte           `json:"data"`
	Instructions []id.UUID        `json:"instructions"`
}

type Module struct {
	Id         id.UUID    `json:"id"`
	Filename   string     `json:"filename"`
	Endianness Endianness `json:"endianness"`
	Sections   []id.UUID  `json:"sections"`
}

type IRDB interface {
	Transaction() db.TransactionManager
	Common() db.Common

	SetModule(module Module, transaction db.Transaction)
	GetModule(uuid id.UUID) mo.Option[Module]
	SetSection(section Section, moduleId id.UUID, transaction db.Transaction)
	GetSection(uuid id.UUID) mo.Option[Section]
	SetInstruction(instruction Instruction, sectionId id.UUID, position uint, transaction db.Transaction)
	GetInstruction(uuid id.UUID) mo.Option[Instruction]
	GetInstructionOperations(uuid id.UUID) ([]Operation, error)
	GetInstructionByExactAddress(address Address) (Instruction, error)
	GetInstructionContaining(address Address) (Instruction, error)
	SetOperation(operation Operation, instructionId id.UUID, positionInInstruction uint, index uint, transaction db.Transaction) error
	GetOperation(uuid id.UUID) (Operation, error)
	GetOperationByIndex(index OperationIndex) (Operation, error)
	GetOperationsInRange(start OperationIndex, end OperationIndex) ([]Operation, error)
	SetOperand(operand Operand, operationId id.UUID, position int, transaction db.Transaction)
	GetOperand(uuid id.UUID) (Operand, error)
	GetOperands() ([]id.UUID, error)
	GetOperandWrites(operand id.UUID) ([]Operation, error)
	GetOperandReads(operand id.UUID) ([]Operation, error)
	GetInstructionPointer() (Operand, error)
	SetInstructionPointer(operand Operand) error

	ConvertAddressToOperationIndex(address Address) (OperationIndex, error)

	VerifyAll() error
	VerifySectionBoundaries() error
	VerifyInstructionBoundaries() error
}

type IRLoader struct {
	Db                 IRDB
	Modules            map[id.UUID]Module
	Sections           map[id.UUID]Section
	Instructions       map[id.UUID]Instruction
	Operations         map[id.UUID]Operation
	InstructionPointer Operand
}

func (ir *IRLoader) IntegrityCheck() error {
	instructionIds := util.NewUUIDHashset()
	for instructionId, _ := range ir.Instructions {
		if instructionIds.Has(instructionId) {
			return errors.New(fmt.Sprintf("[!] integrity check failed: instruction has a non-unique id: %s\n\n", instructionId))
		} else {
			instructionIds.Put(instructionId)
		}
	}

	instructionIds.Clear()

	var sectionInstructionCount uint = 0
	for _, section := range ir.Sections {
		for index, instructionId := range section.Instructions {
			if instructionIds.Has(instructionId) {
				return errors.New(fmt.Sprintf("[!] integrity check failed: instruction at index %d in section %s has a non-unique id: %s\n\n", index, section.Name, instructionId))
			} else {
				instructionIds.Put(instructionId)
				sectionInstructionCount++
			}
		}
	}

	if sectionInstructionCount != uint(len(ir.Instructions)) {
		return errors.New(fmt.Sprintf("[!] integrity check failed: sections have %d instructions but there are %d total instructions\n", sectionInstructionCount, len(ir.Instructions)))
	}

	return nil
}

func (ir *IRLoader) Load() error {
	if ir.Db == nil {
		return errors.New("IRDB unset")
	}

	ir.Db.Common().DeclareUUIDIndex("module_id", "Module", "id")
	ir.Db.Common().DeclareUUIDIndex("section_id", "Section", "id")
	ir.Db.Common().DeclareUUIDIndex("instruction_id", "Instruction", "id")
	ir.Db.Common().DeclareUUIDIndex("instruction_address", "Instruction", "address")
	ir.Db.Common().DeclareUUIDIndex("operation_id", "Operation", "id")
	ir.Db.Common().DeclareUUIDIndex("operation_linear_index", "Operation", "linear_index")
	ir.Db.Common().DeclareUUIDIndex("operand_id", "Operand", "id")

	transaction := ir.Db.Transaction().BeginTransaction()

	var operationIndex uint = 0

	for _, module := range ir.Modules {
		//fmt.Printf("module index: %d\n", mIndex)
		ir.Db.SetModule(module, transaction)

		for _, sectionId := range module.Sections {
			section, sectionFound := ir.Sections[sectionId]
			if !sectionFound {
				panic(fmt.Sprintf("[!] could not find section %s", sectionId))
			}
			//fmt.Printf("section index: %d\n", sIndex)
			ir.Db.SetSection(section, module.Id, transaction)

			if len(section.Instructions) > 0 {
				fmt.Printf("[d] section %s has %d instructions\n", section.Name, len(section.Instructions))
			}

			for insnIndex, instructionId := range section.Instructions {
				instruction, insnFound := ir.Instructions[instructionId]
				if !insnFound {
					panic(fmt.Sprintf("[!] could not find instruction %s", instructionId))
				}

				//fmt.Printf("insn index: %d\n", insnIndex)

				ir.Db.SetInstruction(instruction, section.Id, uint(insnIndex), transaction)

				// quick sanity check to verify the uniqueness of the operations
				insnOpMap := util.Slice2Map(instruction.Operations)
				if len(insnOpMap) != len(instruction.Operations) {
					panic(instruction.Operations)
				}

				for position, operationId := range instruction.Operations {
					operation, opFound := ir.Operations[operationId]
					if !opFound {
						fmt.Println(ir.Operations)
						panic(fmt.Sprintf("[!] could not find operation %s", operationId))
					}
					err := ir.Db.SetOperation(operation, instructionId, uint(position), operationIndex, transaction)
					if err != nil {
						ir.Db.Transaction().CancelTransaction(transaction)
						return err
					}

					operationIndex += 1
				}

				//if insnIndex%1000 == 0 {
				//	fmt.Printf("[d] loaded insn #%d\n", insnIndex)
				//}
			}
		}
	}

	if ir.InstructionPointer != nil {
		err := ir.Db.SetInstructionPointer(ir.InstructionPointer)
		if err != nil {
			panic(err)
		}
	}

	sectionCheck := ir.Db.VerifySectionBoundaries()
	if sectionCheck != nil {
		ir.Db.Transaction().CancelTransaction(transaction)
		return sectionCheck
	}
	insnCheck := ir.Db.VerifyInstructionBoundaries()
	if insnCheck != nil {
		ir.Db.Transaction().CancelTransaction(transaction)
		return insnCheck
	}

	return ir.Db.Transaction().CommitTransaction(transaction)
}

func NewIRLoader(modules []Module, sections []Section, instructions []Instruction, operations []Operation, instructionPointer Operand, irdatabase IRDB) IRLoader {
	return IRLoader{
		Db: irdatabase,
		Modules: lo.SliceToMap(modules, func(module Module) (id.UUID, Module) {
			return module.Id, module
		}),
		Sections: lo.SliceToMap(sections, func(section Section) (id.UUID, Section) {
			return section.Id, section
		}),
		Instructions: lo.SliceToMap(instructions, func(instruction Instruction) (id.UUID, Instruction) {
			return instruction.Id, instruction
		}),
		Operations: lo.SliceToMap(operations, func(operation Operation) (id.UUID, Operation) {
			return operation.Id, operation
		}),
		InstructionPointer: instructionPointer,
	}
}
