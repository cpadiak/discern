package ir

import (
	"discern/internal/db"
	"discern/internal/id"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"strings"
)

type IRNeo4j struct {
	*db.Neo4jDB
}

func (db *IRNeo4j) SetModule(module Module, transaction db.Transaction) {
	params := map[string]any{
		"id":         module.Id,
		"filename":   module.Filename,
		"endianness": module.Endianness,
	}
	_, err := db.RunWithTransaction("CREATE (m:Module {id: $id, filename: $filename, endianness: $endianness})", params, transaction)
	if err != nil {
		panic(err.Error())
	}
}

func (db *IRNeo4j) moduleDestructure(module string) string {
	return fmt.Sprintf("%s.id AS id, %s.name AS name, %s.endianness AS endianness", module, module, module)
}

func (db *IRNeo4j) GetModule(uuid id.UUID) mo.Option[Module] {
	params := map[string]any{
		"id": uuid,
	}
	result, err := db.Query("MATCH (mod:Module {id: $id})\n"+
		"RETURN "+db.moduleDestructure("mod"), params)
	if err != nil {
		return mo.None[Module]()
	}

	var module Module
	err = mapstructure.Decode(result.Records, &module)
	if err != nil {
		return mo.None[Module]()
	}

	return mo.Some(module)
}

func IsMemoryFree(sections []Section, start uint64, size uint64) bool {
	end := start + size - 1
	for _, section := range sections {
		sectionEnd := section.Address.Offset + section.Size - 1
		sectionStart := section.Address.Offset
		if start <= sectionEnd && end >= sectionStart {
			fmt.Printf("[d] [%d, %d] overlaps section %s [%d, %d]\n", start, end, section.Name, sectionStart, sectionEnd+section.Size)
			return false
		}
	}
	return true
}

func (db *IRNeo4j) SetSection(section Section, moduleId id.UUID, transaction db.Transaction) {
	params := map[string]any{
		"id":          section.Id,
		"name":        section.Name,
		"space":       section.Address.Space.Name,
		"offset":      section.Address.Offset,
		"size":        section.Size,
		"permissions": section.Permissions,
		"data":        section.Data,
		"source_id":   moduleId,
	}
	_, err := db.RunWithTransaction("MATCH (m:Module) WHERE m.id = $source_id\n"+
		"CREATE (s:Section {id: $id, name: $name, space: $space, offset: $offset, size: $size, permissions: $permissions, data: $data})<-[:HAS_SECTION]-(m)", params, transaction)
	if err != nil {
		panic(err.Error())
	}
}

func (db *IRNeo4j) sectionDestructure(section string) string {
	return fmt.Sprintf("%s.id AS id, %s.name AS name, %s.space AS space, %s.offset AS offset, %s.size AS size, %s.permissions AS permissions, %s.data AS data", section, section, section, section, section, section, section)
}

func (db *IRNeo4j) GetSection(uuid id.UUID) mo.Option[Section] {
	params := map[string]any{
		"id": uuid,
	}
	result, err := db.Query("MATCH (s:Section {id: $id})\n"+
		"RETURN "+db.sectionDestructure("s"), params)
	if err != nil {
		return mo.None[Section]()
	}

	var section Section
	err = mapstructure.Decode(result.Records, &section)
	if err != nil {
		return mo.None[Section]()
	}

	return mo.Some(section)
}

func (db *IRNeo4j) SetInstruction(instruction Instruction, sectionId id.UUID, position uint, transaction db.Transaction) {
	params := map[string]any{
		"id":        instruction.Id,
		"space":     instruction.Address.Space.Name,
		"offset":    instruction.Address.Offset,
		"size":      instruction.Size,
		"mnemonic":  instruction.Mnemonic,
		"source_id": sectionId,
		"position":  position,
	}
	_, err := db.RunWithTransaction("MATCH (s:Section {id: $source_id})\n"+
		"CREATE (insn:Instruction {id: $id, space: $space, offset: $offset, size: $size, mnemonic: $mnemonic, position: $position})<-[:HAS_INSTRUCTION {position: $position}]-(s)", params, transaction)
	if err != nil {
		panic(err)
	}
}

func (db *IRNeo4j) instructionDestructure(instruction string, operation string) string {
	return fmt.Sprintf("%s.id AS id, %s.space AS space, %s.offset AS offset, %s.size AS size, %s.mnemonic AS mnemonic, %s.position AS position, collect(%s.id) AS operations", instruction, instruction, instruction, instruction, instruction, instruction, operation)
}

func (db *IRNeo4j) instructionOperationsQuery(instruction string, operation string) string {
	return fmt.Sprintf("OPTIONAL MATCH (%s)-[r:HAS_OPERATION]-(%s:Operation)\n", instruction, operation)
}

func (db *IRNeo4j) GetInstruction(uuid id.UUID) mo.Option[Instruction] {
	params := map[string]any{
		"id": uuid,
	}
	result, err := db.Query("MATCH (insn:Instruction {id: $id})\n"+
		db.instructionOperationsQuery("insn", "operation")+
		"RETURN "+db.instructionDestructure("insn", "operation"), params)
	if err != nil {
		return mo.None[Instruction]()
	}

	var instruction Instruction
	err = mapstructure.Decode(result.Records, &instruction)
	if err != nil {
		return mo.None[Instruction]()
	}

	return mo.Some(instruction)
}

func (db *IRNeo4j) GetInstructionByExactAddress(address Address) (Instruction, error) {
	params := map[string]any{
		"space":  address.Space.Name,
		"offset": address.Offset,
	}
	result, err := db.Query("MATCH (insn:Instruction {space: $space, offset: $offset})\n"+
		db.instructionOperationsQuery("insn", "operation")+
		"RETURN "+db.instructionDestructure("insn", "operation"), params)
	if err != nil {
		return Instruction{}, err
	}

	if len(result.Records) == 0 {
		return Instruction{}, errors.New("instruction not found")
	}

	var instruction Instruction
	err = mapstructure.Decode(result.Records[0].AsMap(), &instruction)
	if err != nil {
		panic(err.Error())
	}

	return instruction, nil
}

// This function will ASSUME that the given address does not map exactly to an instruction's address
// and will go through routines to figure out what instruction CONTAINS the given address
func (db *IRNeo4j) GetInstructionContaining(address Address) (Instruction, error) {
	// We could do this brute force and just issue a batch request for every
	// instruction at every address BELOW the given address up to the platform's
	// maximum instruction length, but I'm going to implement this "properly".
	// NOTE: See if a brute force method would be any better for any reason

	params := map[string]any{
		"space":  address.Space.Name,
		"offset": address.Offset,
	}
	result, err := db.Query("MATCH (insn:Instruction)\n"+
		"WHERE insn.space = $space AND insn.offset <= $offset AND $offset < insn.offset + insn.size\n"+
		db.instructionOperationsQuery("insn", "operation")+
		"RETURN "+db.instructionDestructure("insn", "operation"), params)
	if err != nil {
		return Instruction{}, err
	}

	if len(result.Records) == 0 {
		return Instruction{}, errors.New("instruction not found")
	}

	var instruction Instruction
	err = mapstructure.Decode(result.Records[0].AsMap(), &instruction)
	if err != nil {
		panic(err.Error())
	}

	return instruction, nil
}

func (db *IRNeo4j) GetInstructionOperations(uuid id.UUID) ([]Operation, error) {
	params := map[string]any{
		"id": uuid,
	}
	result, err := db.Query("MATCH (insn:Instruction {id: $id})--(op:Operation)"+
		"OPTIONAL MATCH (input:Operand)-[:HAS_INPUT]-(op)-[:HAS_OUTPUT]-(output:Operand)\n"+
		"RETURN "+db.operationDestructure("op", "input", "output"), params)
	if err != nil {
		return nil, err
	}

	return lo.Map(result.Records, func(record *neo4j.Record, _ int) Operation {
		return decodeOperation(record)
	}), nil
}

func (db *IRNeo4j) SetOperation(operation Operation, instructionId id.UUID, positionInInstruction uint, operationIndex uint, transaction db.Transaction) error {
	params := map[string]any{
		"id":           operation.Id,
		"semantic":     operation.Semantic,
		"source_id":    instructionId,
		"position":     positionInInstruction,
		"linear_index": operationIndex,
		"has_output":   operation.Output != nil,
	}
	_, err := db.RunWithTransaction("MATCH (insn:Instruction {id: $source_id})\n"+
		"CREATE (op:Operation {id: $id, semantic: $semantic, linear_index: $linear_index, has_output: $has_output})<-[:HAS_OPERATION { position: $position }]-(insn)", params, transaction)
	for position, operand := range operation.Inputs {
		db.SetOperand(operand, operation.Id, position, transaction)
	}
	if operation.Output != nil {
		db.SetOperand(*(operation.Output), operation.Id, -1, transaction)
	}
	return err
}

func (db *IRNeo4j) operationDestructure(operation string, inputs string, output string) string {
	return fmt.Sprintf("%s.semantic AS semantic, %s.id AS id, %s.linear_index AS linear_index, collect(%s) AS inputs, %s AS output", operation, operation, operation, inputs, output)
}

func decodeOperation(record *neo4j.Record) Operation {
	rm := record.AsMap()
	var operation Operation
	operation.Id = id.UUID(rm["id"].(string))
	operation.Index = OperationIndex(rm["linear_index"].(int64))
	operation.Semantic = Semantic(rm["semantic"].(string))
	_, hasOutput := rm["output"]
	if hasOutput && rm["output"] != nil {
		operation.Output = decodeOperand(rm["output"].(neo4j.Node).GetProperties())
	}
	operation.Inputs = make([]Operand, 0)
	for _, input := range rm["inputs"].([]any) {
		operation.Inputs = append(operation.Inputs, *decodeOperand(input.(neo4j.Node).GetProperties()))
	}
	return operation
}

func (db *IRNeo4j) GetOperation(uuid id.UUID) (Operation, error) {
	result, err := db.Query("MATCH (op:Operation {id: $id})\n"+
		"OPTIONAL MATCH (op)-[:HAS_INPUT]-(input:Operand)\n"+
		"OPTIONAL MATCH (op)-[:HAS_OUTPUT]-(output:Operand)\n"+
		"RETURN "+db.operationDestructure("op", "input", "output"), map[string]any{"id": uuid})
	if err != nil {
		return Operation{}, err
	}

	if len(result.Records) == 0 {
		return Operation{}, errors.New(fmt.Sprintf("operation %s not found", uuid))
	}

	return decodeOperation(result.Records[0]), nil
}

func (db *IRNeo4j) GetOperationByIndex(index OperationIndex) (Operation, error) {
	params := map[string]any{
		"op_index": index,
	}
	result, err := db.Query("MATCH (op:Operation {linear_index: $op_index})\n"+
		"OPTIONAL MATCH (op)-[:HAS_INPUT]-(input:Operand)\n"+
		"OPTIONAL MATCH (op)-[:HAS_OUTPUT]-(output:Operand)\n"+
		"RETURN "+db.operationDestructure("op", "input", "output"), params)

	if err != nil {
		return Operation{}, err
	}
	return decodeOperation(result.Records[0]), nil
}

func (db *IRNeo4j) GetOperationsInRange(start OperationIndex, end OperationIndex) ([]Operation, error) {
	result, err := db.Query("MATCH (op:Operation)\n"+
		"WHERE $start <= op.linear_index AND op.linear_index <= $end\n"+
		"WITH op\n"+
		"OPTIONAL MATCH (op)-[:HAS_INPUT]-(input:Operand)\n"+
		"OPTIONAL MATCH (op)-[:HAS_OUTPUT]-(output:Operand)\n"+
		"RETURN "+db.operationDestructure("op", "input", "output"), map[string]any{"start": start, "end": end})
	if err != nil {
		return nil, err
	}

	if len(result.Records) == 0 {
		return []Operation{}, nil
	}

	return lo.Map(result.Records, func(record *neo4j.Record, _ int) Operation {
		return decodeOperation(record)
	}), nil
}

func decodeOperand(recordMap map[string]any) *Operand {
	operandType := recordMap["operand_type"].(string)

	operandId := id.UUID(recordMap["id"].(string))
	bitSize := uint(recordMap["bit_size"].(int64))
	representation := Representation(recordMap["representation"].(string))

	var operand Operand

	switch operandType {
	case "container":
		operand = &ContainerOperand{Id: operandId, BitSize: bitSize, Representation: representation}
		operand.(*ContainerOperand).Location.Address.Offset = uint64(recordMap["offset"].(int64))
		operand.(*ContainerOperand).Location.Address.Space = *GetAddressSpaceByName(recordMap["space"].(string))
	case "immediate":
		operand = &ImmediateOperand{Id: operandId, BitSize: bitSize, Representation: representation}
		operand.(*ImmediateOperand).ImmediateValue = recordMap["immediate_value"].([]byte)
	case "branch":
		operand = &BranchOperand{Id: operandId, BitSize: bitSize, Representation: representation}
		operand.(*BranchOperand).BranchMode = BranchMode(recordMap["branch_mode"].(string))
		switch operand.(*BranchOperand).BranchMode {
		case DIRECT:
			var target Address
			target.Offset = uint64(recordMap["direct_branch_target_offset"].(int64))
			target.Space = *GetAddressSpaceByName(recordMap["direct_branch_target_space"].(string))
			operand.(*BranchOperand).AbsoluteBranchTarget = mo.Some(target)
		case INDIRECT:
			var target Location
			target.Id = id.UUID(recordMap["indirect_branch_target_id"].(string))
			target.Address.Offset = uint64(recordMap["indirect_branch_target_offset"].(int64))
			target.Address.Space = *GetAddressSpaceByName(recordMap["indirect_branch_target_space"].(string))
			operand.(*BranchOperand).IndirectBranchTarget = mo.Some(target)
		case RELATIVE:
			operand.(*BranchOperand).RelativeBranchTarget = mo.Some(recordMap["relative_branch_target_offset"].(int64))
		}
	}

	return &operand
}

func (db *IRNeo4j) setOperand(operand Operand, transaction db.Transaction) error {
	params := map[string]any{
		"id":             operand.GetId(),
		"representation": operand.GetRepresentation(),
		"bit_size":       operand.GetBitSize(),
	}

	if operand.IsContainer() {
		params["offset"] = operand.GetContainerLocation().Address.Offset
		params["space"] = operand.GetContainerLocation().Address.Space.Name
		params["operand_type"] = "container"
	} else if operand.IsImmediate() {
		params["immediate_value"] = operand.GetImmediateValue()
		params["operand_type"] = "immediate"
	} else if operand.IsBranch() {
		params["branch_mode"] = operand.GetBranch().BranchMode
		switch operand.GetBranch().BranchMode {
		case DIRECT:
			params["direct_branch_target_offset"] = operand.GetBranch().AbsoluteBranchTarget.MustGet().Offset
			params["direct_branch_target_space"] = operand.GetBranch().AbsoluteBranchTarget.MustGet().Space.Name
		case INDIRECT:
			params["indirect_branch_target_id"] = operand.GetBranch().IndirectBranchTarget.MustGet().Id
			params["indirect_branch_target_offset"] = operand.GetBranch().IndirectBranchTarget.MustGet().Address.Offset
			params["indirect_branch_target_space"] = operand.GetBranch().IndirectBranchTarget.MustGet().Address.Space.Name
		case RELATIVE:
			params["relative_branch_target_offset"] = operand.GetBranch().RelativeBranchTarget.MustGet()
		}
		params["operand_type"] = "branch"
	}

	// Gather all operand attributes BEFORE adding the non-operand attributes to
	// the params map for neo4j
	operandAttributes := lo.Map(lo.Keys(params), func(attribute string, _ int) string {
		return fmt.Sprintf("%s: $%s", attribute, attribute)
	})

	_, err := db.RunWithTransaction(fmt.Sprintf("CREATE (operand:Operand {%s})", strings.Join(operandAttributes, ", ")), params, transaction)
	return err
}

func (db *IRNeo4j) SetOperand(operand Operand, operationId id.UUID, position int, transaction db.Transaction) {
	err := db.setOperand(operand, transaction)
	if err != nil {
		db.CancelTransaction(transaction)
		panic(err)
	}

	params := map[string]any{
		"id":       operand.GetId(),
		"op_id":    operationId,
		"position": position,
	}

	relationship := "HAS_INPUT {position: $position}"
	if position < 0 {
		relationship = "HAS_OUTPUT"
	}

	_, err = db.RunWithTransaction(fmt.Sprintf("MATCH (operand:Operand {id: $id}), (operation:Operation {id: $op_id})\n"+
		"CREATE (operand)<-[:%s]-(operation)", relationship), params, transaction)
	if err != nil {
		db.CancelTransaction(transaction)
		panic(err.Error())
	}
}

func (db *IRNeo4j) GetOperand(uuid id.UUID) (Operand, error) {
	params := map[string]any{"id": uuid}
	result, err := db.Query("MATCH (operand:Operand {id: $id})\n"+
		"RETURN operand.operand_type AS operand_type", params)
	if err != nil {
		return &ContainerOperand{}, err
	}

	if len(result.Records) == 0 {
		return &ContainerOperand{}, errors.New(fmt.Sprintf("operand %s not found", uuid))
	}

	return *decodeOperand(result.Records[0].AsMap()), nil
}

func (db *IRNeo4j) GetOperands() ([]id.UUID, error) {
	result, err := db.Query("MATCH (operand:Operand) RETURN operand", map[string]any{})
	if err != nil {
		return []id.UUID{}, err
	}
	return lo.Map(result.Records, func(record *neo4j.Record, _ int) id.UUID {
		return (*decodeOperand(record.AsMap())).GetId()
	}), nil
}

func (db *IRNeo4j) GetOperandWrites(operand id.UUID) ([]Operation, error) {
	result, err := db.Query("MATCH (operand:Operand {id: $id})-[:HAS_OUTPUT]-(op:Operation)\n"+
		"RETURN op", map[string]any{"id": operand})
	if err != nil {
		return []Operation{}, err
	}

	if len(result.Records) == 0 {
		return []Operation{}, nil
	}

	return lo.Map(result.Records, func(record *neo4j.Record, _ int) Operation {
		return decodeOperation(record)
	}), nil
}

func (db *IRNeo4j) GetOperandReads(operand id.UUID) ([]Operation, error) {
	result, err := db.Query("MATCH (operand:Operand {id: $id})-[:HAS_INPUT]-(op:Operation)\n"+
		"RETURN op", map[string]any{"id": operand})
	if err != nil {
		return []Operation{}, err
	}

	if len(result.Records) == 0 {
		return []Operation{}, nil
	}

	return lo.Map(result.Records, func(record *neo4j.Record, _ int) Operation {
		return decodeOperation(record)
	}), nil
}

func (db *IRNeo4j) GetInstructionPointer() (Operand, error) {
	result, err := db.Query("OPTIONAL MATCH (operand:Operand)\n"+
		"WHERE operand.is_instruction_pointer != null\n"+
		"RETURN operand", map[string]any{})
	if err != nil {
		return nil, err
	}
	return *decodeOperand(result.Records[0].AsMap()), nil
}

func (db *IRNeo4j) SetInstructionPointer(operand Operand) error {
	transaction := db.BeginTransaction()
	err := db.setOperand(operand, transaction)
	if err != nil {
		db.CancelTransaction(transaction)
		return err
	}

	err = db.CommitTransaction(transaction)
	if err != nil {
		return err
	}

	return nil
}

func (db *IRNeo4j) ConvertAddressToOperationIndex(address Address) (OperationIndex, error) {
	// TODO: This could be made faster if we either check or assume that the address is exact
	// and use that DB call instead/first
	insn, err := db.GetInstructionContaining(address)
	if err != nil {
		return OperationIndex(0), err
	}

	operations, err := db.GetInstructionOperations(insn.Id)
	if err != nil {
		return OperationIndex(0), err
	}
	if len(operations) == 0 {
		return OperationIndex(0), errors.New("no operation found at given address")
	}
	return operations[0].Index, nil
}

func (db *IRNeo4j) Transaction() db.TransactionManager {
	db.EnsureIsConnected()
	return db
}

func (db *IRNeo4j) Common() db.Common {
	db.EnsureIsConnected()
	return db
}

func (db *IRNeo4j) VerifySectionBoundaries() error {
	result, err := db.Query("MATCH (a:Section)\n"+
		"WITH a\n"+
		"MATCH (b:Section)\n"+
		"WHERE a.id <> b.id AND a.space = b.space AND a.offset < b.offset + b.size AND a.offset + a.size > b.offset\n"+
		"RETURN a.offset, a.name",
		make(map[string]any))
	if err != nil {
		return err
	}
	if len(result.Records) > 0 {
		return errors.New("overlapping sections found")
	}
	return nil
}

func (db *IRNeo4j) VerifyInstructionBoundaries() error {
	result, err := db.Query("MATCH (a:Instruction)\n"+
		"WITH a\n"+
		"MATCH (b:Instruction)\n"+
		"WHERE a.id <> b.id AND a.offset < b.offset + b.size AND a.offset + a.size > b.offset\n"+
		"RETURN a.offset, a.mnemonic", make(map[string]any))
	if err != nil {
		return err
	}
	if len(result.Records) > 0 {
		return errors.New("overlapping instructions found")
	}
	return nil
}

func (db *IRNeo4j) VerifyAll() error {
	err := db.VerifySectionBoundaries()
	if err != nil {
		return err
	}
	err = db.VerifyInstructionBoundaries()
	if err != nil {
		return err
	}
	return nil
}
