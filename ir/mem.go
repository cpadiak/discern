package ir

import (
	"discern/internal/db"
	"discern/internal/id"
	"encoding/json"
)

type IRMem struct {
	*db.MemDB
}

func (irdb *IRMem) SetModule(uuid id.UUID, module Module, transaction db.Transaction) {
	serialized, err := json.Marshal(module)
	if err != nil {
		panic(err)
	}
	irdb.DeclareLabel("module")
	irdb.Set(transaction, "module", uuid, serialized)
}
