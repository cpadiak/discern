package ir_test

import (
	"discern/internal/id"
	"discern/internal/testcommon"
	"discern/ir"
	"fmt"
	"github.com/go-playground/assert/v2"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"testing"
)

func TestNeo4jOperationWithOperandRelationship(t *testing.T) {
	containerInfo := testcommon.Neo4jContainer(t, "neo4j", "verylong")

	neo4jDB := containerInfo.N4DB

	irdb := ir.IRNeo4j{Neo4jDB: neo4jDB}

	for _, deference := range containerInfo.Deferences {
		defer deference()
	}

	var inputOperand ir.Operand = &ir.ContainerOperand{
		Id:             "10",
		BitSize:        32,
		Representation: ir.UINT,
		Location: ir.Location{
			Id: "20",
			Address: ir.Address{
				Offset: 0,
				Space:  ir.RegisterAddressSpace,
			},
		},
		ContainerName: mo.Some("input0"),
		IsIP:          false,
	}

	var outputOperand ir.Operand = &ir.ContainerOperand{
		Id:             "11",
		BitSize:        32,
		Representation: ir.UINT,
		Location: ir.Location{
			Id: "21",
			Address: ir.Address{
				Offset: 4,
				Space:  ir.RegisterAddressSpace,
			},
		},
		ContainerName: mo.Some("output0"),
		IsIP:          false,
	}

	operation := ir.Operation{
		Id:       "1",
		Semantic: ir.MOV,
		Inputs:   []ir.Operand{inputOperand},
		Output:   &outputOperand,
		Index:    0,
	}

	instructionId := id.UUID("100")

	transaction := irdb.Transaction().BeginTransaction()

	result, err := irdb.RunWithTransaction("CREATE (insn:Instruction {id: $id})", map[string]any{"id": instructionId}, transaction)
	assert.Equal(t, err, nil)
	_, err = result.Collect(*irdb.Ctx)
	assert.Equal(t, err, nil)

	err = irdb.SetOperation(operation, instructionId, 0, 0, transaction)
	assert.Equal(t, err, nil)

	err = irdb.CommitTransaction(transaction)
	assert.Equal(t, err, nil)

	fetchedOperation, err := irdb.GetOperation(operation.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, fetchedOperation.String(), operation.String())
}

func TestIRLoadComplex(t *testing.T) {
	com := testcommon.TestCommon{}
	tm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&tm, &com)

	tid := id.TestID{}

	irloader := testcommon.MakeComplex(&tid, irdb)

	err := irloader.Load()
	if err != nil {
		t.Fatal(err)
	}

	err = irdb.VerifyAll()
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, len(irdb.Modules), 1)
	assert.Equal(t, len(irdb.Sections), 1)
	assert.Equal(t, len(irdb.Instructions), 13)
	assert.Equal(t, len(irdb.LinearInstructions), len(irdb.Instructions))
	assert.Equal(t, len(irdb.InstructionAddresses), len(irdb.Instructions))
	assert.Equal(t, len(lo.Keys(irdb.InsnOps)), len(irdb.Instructions))
	assert.Equal(t, len(lo.Values(irdb.InsnOps)), len(irdb.Operations))
	assert.Equal(t, len(irdb.Operations), 13)
	assert.Equal(t, len(irdb.OperationIndices), len(irdb.Operations))
	assert.Equal(t, len(irdb.Operands), 18)
}

func TestIRLoadNeo4j(t *testing.T) {
	containerInfo := testcommon.Neo4jContainer(t, "neo4j", "verylong")

	neo4jDB := containerInfo.N4DB

	for _, deference := range containerInfo.Deferences {
		defer deference()
	}

	//fmt.Println("preparing irloader to load")

	tid := id.TestID{}

	irloader := testcommon.MakeOneInsnIR(&tid, &ir.IRNeo4j{Neo4jDB: neo4jDB})

	result := irloader.Load()
	if result != nil {
		t.Errorf("irloader load failed with error: %s", result.Error())
	}

	assert.Equal(t, nil, irloader.Db.VerifyAll())

	fmt.Println("finished loading IR")

	operation, err := irloader.Db.GetOperationByIndex(0)
	assert.Equal(t, err, nil)

	assert.Equal(t, operation.Semantic, ir.MOV)
	assert.Equal(t, len(operation.Inputs), 1)
	assert.Equal(t, operation.Inputs[0].String(), "{location: {address: 0x1:register}, representation: UINT, bit_size: 8}")
}
