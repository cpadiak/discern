package ir

import (
	"discern/internal/id"
	"encoding/json"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"strconv"
	"strings"
)

type AddressSpace struct {
	Name string
}

func (space *AddressSpace) String() string {
	return space.Name
}

var RegisterAddressSpace = AddressSpace{Name: "register"}
var MemoryAddressSpace = AddressSpace{Name: "memory"}
var TemporaryAddressSpace = AddressSpace{Name: "temporary"}

//var ConstantAddressSpace = AddressSpace{Name: "constant"}

func GetAddressSpaceByName(name string) *AddressSpace {
	switch name {
	case RegisterAddressSpace.Name:
		return &RegisterAddressSpace
	case TemporaryAddressSpace.Name:
		return &TemporaryAddressSpace
	case MemoryAddressSpace.Name:
		return &MemoryAddressSpace
	default:
		panic(name)
	}
}

type Address struct {
	Offset uint64       `json:"offset"`
	Space  AddressSpace `json:"space"`
}

func (addr *Address) String() string {
	return fmt.Sprintf("0x%s:%s", strconv.FormatUint(addr.Offset, 16), addr.Space.String())
}

func FromString(address string) Address {
	split := strings.Split(address, ":")
	offset, _ := strconv.ParseUint(split[0], 16, 64)
	space := split[1]
	return Address{Offset: offset, Space: AddressSpace{Name: space}}
}

type Location struct {
	Id      id.UUID `json:"id"`
	Address Address `json:"address"`
}

func (loc *Location) String() string {
	return loc.Address.String()
}

type Representation string

const (
	UINT   Representation = "UINT"
	SINT   Representation = "SINT"
	BOOL   Representation = "BOOL"
	FLOAT  Representation = "FLOAT"
	BINARY Representation = "BINARY"
	NAME   Representation = "NAME"
)

//type AddressMode string
//
//const (
//	IMMEDIATE AddressMode = "IMMEDIATE"
//	MEMORY    AddressMode = "MEMORY"
//	REGISTER  AddressMode = "REGISTER"
//)

type Operand interface {
	GetId() id.UUID
	String() string
	Name() string
	GetBitSize() uint
	GetRepresentation() Representation
	ToJSON() (string, error)

	IsInstructionPointer() bool

	IsContainer() bool
	GetContainerLocation() Location

	IsImmediate() bool
	GetImmediateValue() []byte

	IsBranch() bool
	GetBranch() *BranchOperand
}

type OperandCore struct {
	Id             id.UUID        `json:"id"`
	BitSize        uint           `json:"bit_size"`
	Representation Representation `json:"representation"`
}

type ContainerOperand struct {
	Id             id.UUID        `json:"id"`
	BitSize        uint           `json:"bit_size"`
	Representation Representation `json:"representation"`
	Location       Location       `json:"location"`
	ContainerName  mo.Option[string]
	IsIP           bool
}

func (operand *ContainerOperand) GetId() id.UUID {
	return operand.Id
}

func (operand *ContainerOperand) String() string {
	location := fmt.Sprintf("{address: %s}", operand.Location.Address.String())
	return fmt.Sprintf("{location: %s, representation: %s, bit_size: %d}", location, operand.Representation, operand.BitSize)
}

func (operand *ContainerOperand) Name() string {
	if operand.ContainerName.IsPresent() {
		return operand.ContainerName.MustGet()
	}
	return fmt.Sprintf("%s", operand.Location.String())
}

func (operand *ContainerOperand) IsContainer() bool {
	return true
}

func (operand *ContainerOperand) GetContainerLocation() Location {
	return operand.Location
}

func (operand *ContainerOperand) IsImmediate() bool {
	return false
}

func (operand *ContainerOperand) GetImmediateValue() []byte {
	panic("I'm not an immediate")
}

func (operand *ContainerOperand) GetBitSize() uint {
	return operand.BitSize
}

func (operand *ContainerOperand) GetRepresentation() Representation {
	return operand.Representation
}

func (operand *ContainerOperand) IsBranch() bool {
	return false
}

func (operand *ContainerOperand) GetBranch() *BranchOperand {
	panic("I'm not a branch")
}

func (operand *ContainerOperand) ToJSON() (string, error) {
	result, err := json.Marshal(operand)
	return string(result), err
}

func (operand *ContainerOperand) IsInstructionPointer() bool {
	return operand.IsIP
}

type ImmediateOperand struct {
	Id             id.UUID        `json:"id"`
	BitSize        uint           `json:"bit_size"`
	Representation Representation `json:"representation"`
	ImmediateValue []byte         `json:"immediate_value"`
}

func (operand *ImmediateOperand) GetId() id.UUID {
	return operand.Id
}

func (operand *ImmediateOperand) String() string {
	return fmt.Sprintf("%x", operand.ImmediateValue)
}

func (operand *ImmediateOperand) Name() string {
	return fmt.Sprintf("imm(%x)", operand.ImmediateValue)
}

func (operand *ImmediateOperand) IsContainer() bool {
	return false
}

func (operand *ImmediateOperand) GetContainerLocation() Location {
	panic("I'm not a container")
}

func (operand *ImmediateOperand) IsImmediate() bool {
	return true
}

func (operand *ImmediateOperand) GetImmediateValue() []byte {
	return operand.ImmediateValue
}

func (operand *ImmediateOperand) GetBitSize() uint {
	return operand.BitSize
}

func (operand *ImmediateOperand) GetRepresentation() Representation {
	return operand.Representation
}

func (operand *ImmediateOperand) IsBranch() bool {
	return false
}

func (operand *ImmediateOperand) GetBranch() *BranchOperand {
	panic("I'm not a branch")
}

func (operand *ImmediateOperand) ToJSON() (string, error) {
	result, err := json.Marshal(operand)
	return string(result), err
}

func (operand *ImmediateOperand) IsInstructionPointer() bool {
	return false
}

type BranchOperand struct {
	Id                   id.UUID             `json:"id"`
	BitSize              uint                `json:"bit_size"`
	Representation       Representation      `json:"representation"`
	BranchMode           BranchMode          `json:"branch_mode"`
	AbsoluteBranchTarget mo.Option[Address]  `json:"absolute_branch_target"`
	IndirectBranchTarget mo.Option[Location] `json:"indirect_branch_target"`
	RelativeBranchTarget mo.Option[int64]    `json:"relative_branch_target"`
}

func (operand *BranchOperand) GetId() id.UUID {
	return operand.Id
}

func (operand *BranchOperand) String() string {
	return fmt.Sprintf("%s -> %x", operand.BranchMode, operand.AbsoluteBranchTarget.MustGet())
}

func (operand *BranchOperand) Name() string {
	switch operand.BranchMode {
	case DIRECT:
		addr := operand.AbsoluteBranchTarget.MustGet()
		return addr.String()
	case INDIRECT:
		location := operand.IndirectBranchTarget.MustGet()
		return location.String()
	case RELATIVE:
		return fmt.Sprintf("%d", operand.RelativeBranchTarget.MustGet())
	default:
		panic(operand.BranchMode)
	}
}

func (operand *BranchOperand) IsContainer() bool {
	return false
}

func (operand *BranchOperand) GetContainerLocation() Location {
	panic("I'm not a container")
}

func (operand *BranchOperand) IsImmediate() bool {
	return false
}

func (operand *BranchOperand) GetImmediateValue() []byte {
	panic("I'm not an immediate")
}

func (operand *BranchOperand) GetBitSize() uint {
	return operand.BitSize
}

func (operand *BranchOperand) GetRepresentation() Representation {
	return operand.Representation
}

func (operand *BranchOperand) IsBranch() bool {
	return true
}

func (operand *BranchOperand) GetBranch() *BranchOperand {
	return operand
}

func (operand *BranchOperand) ToJSON() (string, error) {
	result, err := json.Marshal(operand)
	return string(result), err
}

func (operand *BranchOperand) IsInstructionPointer() bool {
	return false
}

type OperationIndex uint

func (oi *OperationIndex) Add(difference uint64) OperationIndex {
	return OperationIndex(uint64(*oi) + difference)
}

func (oi *OperationIndex) Sub(difference uint64) OperationIndex {
	return OperationIndex(uint64(*oi) - difference)
}

type Semantic string

const (
	MOV  Semantic = "MOV"
	CAST Semantic = "CAST"
	CONV Semantic = "CONV"
	CMOV Semantic = "CMOV"

	ADD   Semantic = "ADD"
	SUB   Semantic = "SUB"
	NEG   Semantic = "NEG"
	MUL   Semantic = "MUL"
	DIV   Semantic = "DIV"
	MOD   Semantic = "MOD"
	CEIL  Semantic = "CEIL"
	FLOOR Semantic = "FLOOR"
	ROUND Semantic = "ROUND"
	SQRT  Semantic = "SQRT"
	EXP   Semantic = "EXP"

	LOAD  Semantic = "LOAD"
	STORE Semantic = "STORE"

	UOF Semantic = "UOF"
	SOF Semantic = "SOF"
	SUF Semantic = "SUF"

	JMP Semantic = "JMP"
	JT  Semantic = "JT"
	JF  Semantic = "JF"

	LT  Semantic = "LT"
	LE  Semantic = "LE"
	EQ  Semantic = "EQ"
	NEQ Semantic = "NEQ"
	GE  Semantic = "GE"
	GT  Semantic = "GT"

	XOR      Semantic = "XOR"
	AND      Semantic = "AND"
	OR       Semantic = "OR"
	SHL      Semantic = "SHL"
	SHR      Semantic = "SHR"
	ROR      Semantic = "ROR"
	ROL      Semantic = "ROL"
	POPCOUNT Semantic = "POPCOUNT"
	EXTEND   Semantic = "EXTEND"
	TRUNC    Semantic = "TRUNC"
	EXTRACT  Semantic = "EXTRACT"
	CONCAT   Semantic = "CONCAT"

	NACV Semantic = "NACV"

	USERDEFINED Semantic = "USERDEFINED"
)

type Operation struct {
	Id       id.UUID        `json:"id"`
	Semantic Semantic       `json:"semantic"`
	Inputs   []Operand      `json:"inputs"`
	Output   *Operand       `json:"output"`
	Index    OperationIndex `json:"linear_index"`
}

func (op *Operation) String() string {
	getOperandString := func(input Operand, _ int) string {
		return input.String()
	}
	inputsString := strings.Join(lo.Map(op.Inputs, getOperandString), ", ")
	if op.Output != nil {
		return fmt.Sprintf("Operation(%d %s [%s] %s)", op.Index, op.Semantic, inputsString, *op.Output)
	}
	return fmt.Sprintf("Operation(%d %s [%s])", op.Index, op.Semantic, inputsString)
}

var BranchSemantics = map[Semantic]struct{}{
	JMP: {},
	JT:  {},
	JF:  {},
}

func (operation *Operation) IsBranch() bool {
	_, present := BranchSemantics[operation.Semantic]
	return present
}

// unsigned addition overflow & sometimes subtraction underflow
const CF = "CF"

// signed addition overflow
const OF = "OF"

type BranchMode string

const (
	DIRECT      BranchMode = "DIRECT"
	INDIRECT    BranchMode = "INDIRECT"
	RELATIVE    BranchMode = "RELATIVE"
	FALLTHROUGH BranchMode = "FALLTHROUGH"
)
