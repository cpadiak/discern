package main

import (
	"context"
	"discern/internal/db"
	"discern/internal/server"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

type Config struct {
	DbHost     string `toml:"db_host"`
	DbUser     string `toml:"db_user"`
	DbPassword string `toml:"db_password"`
}

func main() {
	configPath := flag.String("config", "", "filepath to configuration file")

	flag.Parse()

	configData, err := os.ReadFile(*configPath)
	if err != nil {
		log.Fatal(fmt.Sprintf("invalid config file path: %s", *configPath))
	}

	var config Config
	_, err = toml.Decode(string(configData), &config)
	if err != nil {
		log.Fatal("invalid toml config")
	}

	fmt.Println("[i] config read")

	ctx := context.Background()

	n4j := db.Neo4jDB{DbName: "neo4j", Ctx: &ctx, Transactions: make(map[db.Transaction]db.Neo4jTransactionInfo)}
	err = n4j.Connect(config.DbHost, &config.DbUser, &config.DbPassword)
	if err != nil {
		panic(err.Error())
	}
	defer func(n4j db.Neo4jDB) {
		err := n4j.Disconnect()
		if err != nil {
			panic(err.Error())
		}
	}(n4j)

	server := server.Server{Ctx: &ctx, Db: &server.Neo4j{Db: &n4j}}

	router := gin.Default()
	err = router.SetTrustedProxies(make([]string, 0))
	if err != nil {
		fmt.Println("[!] could not set trusted proxies")
		return
	}
	router.PUT("/import/ghidra", server.ImportGhidra)
	router.POST("/analyze", server.Analyze)

	fmt.Println("[i] server created")

	fmt.Println("[i] server running")

	router.Run("localhost:4444")

	fmt.Println("Done!")
}
