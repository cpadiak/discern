from pathlib import Path
import pyhidra
import json
import argparse
import copy


get_register_name = None


def fill_template(tmplt, s):
    d = copy.deepcopy(tmplt)
    for k, v in s.items():
        assert k in d
        d[k] = v
    return d


def varnode_to_json(spec, varnode):
    if varnode is None:
        return None

    offset = varnode.getOffset()
    space = varnode.getAddress().getAddressSpace().getName()
    size = varnode.getSize()

    name = None
    if space == "register":
        name = get_register_name(offset, size)

    return fill_template(spec["varnode_format"], {
        "offset": offset,
        "space": space,
        "size": size,
        "name": name
    })


def pcode_to_json(spec, pcode):
    if pcode is None:
        return "None"

    return fill_template(spec["pcode_format"], {
        "operation": pcode.getMnemonic(),
        "inputs": [varnode_to_json(spec, varnode) for varnode in pcode.getInputs()],
        "output": varnode_to_json(spec, pcode.getOutput()),
    })


def insn_to_json(spec, insn):
    return fill_template(spec["instruction_format"], {
        "address": insn.getAddress().toString(),
        "mnemonic": insn.toString(),
        "pcode": [pcode_to_json(spec, pcode) for pcode in insn.getPcode()],
    })


def space_to_json(spec, space):
    return fill_template(spec["address_space_format"], {
        "name": space.getName(),
        "id": space.getSpaceID(),
    })


def block_to_json(spec, block):
    block_addr = block.getStart().toString()
    if "::" in block_addr:
        block_addr = block_addr.split("::")[-1]
    return fill_template(spec["memory_block_format"], {
        "name": block.getName(),
        "address": block_addr,
        "size": block.getSize(),
        "read": block.isRead(),
        "write": block.isWrite(),
        "execute": block.isExecute(),
    })


def main():
    parser = argparse.ArgumentParser("Ghidra P-Code JSON dumper")
    parser.add_argument("program", type=str)
    parser.add_argument("--project-path", type=Path, required=True)
    parser.add_argument("--project-name", type=str, required=True)
    parser.add_argument("--output", type=Path, required=True)
    parser.add_argument("--specification", type=Path, required=True)

    args = parser.parse_args()

    assert args.specification is not None

    with open(args.specification.expanduser().absolute(), 'r') as fh:
        spec = json.load(fh)

    program = {}

    print(f"opening program...")

    with pyhidra.open_program(args.program, project_name=args.project_name, project_location=args.project_path) as flat_api:
        print(f"opened")

        current_program = flat_api.getCurrentProgram()

        program["filename"] = Path(args.program).expanduser().absolute().name

        if current_program.getMemory().isBigEndian():
            program["endianness"] = "big"
        else:
            program["endianness"] = "little"

        print(f"set endianness")

        print(f"getting metadata")

        addr_factory = current_program.getAddressFactory()
        const_space = addr_factory.getConstantSpace().getName()
        register_space = addr_factory.getRegisterSpace().getName()
        unique_space = addr_factory.getUniqueSpace().getName()
        program["address_spaces"] = [const_space, register_space, unique_space]

        global get_register_name
        get_register_name = lambda o, s: current_program.getRegister(addr_factory.getRegisterSpace().getAddress(o)).getName()

        print(f"set metadata")

        print(f"getting memory blocks")

        blocks = []
        for block in current_program.getMemory().getBlocks():
            blocks.append(block_to_json(spec, block))
        z_blocks = [block for block in blocks if int(block["address"], 16) == 0]
        if len(z_blocks) > 1:
            biggest = {"size": 0}
            for block in z_blocks:
                if int(block["size"]) > biggest["size"]:
                    biggest = block
            blocks = [block for block in blocks if block not in z_blocks]
            blocks.append(biggest)
        program["memory_blocks"] = blocks

        print(f"set memory blocks")

        print(f"getting instructions...")

        program["instructions"] = []
        for insn in flat_api.getCurrentProgram().getListing().getInstructions(True):
            program["instructions"].append(insn_to_json(spec, insn))

        print(f"instructions set")

        print(f"getting address spaces...")

        program["address_spaces"] = []
        for space in flat_api.getCurrentProgram().getAddressFactory().getAddressSpaces():
            program["address_spaces"].append(space_to_json(spec, space))

        print(f"address spaces set")

    print(f"writing to json file...")
    # write json file
    with open(args.output.expanduser().absolute(), 'w') as fh:
        json.dump(program, fh)

    print(f"done")
    return


if __name__ == '__main__':
    main()
