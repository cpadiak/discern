package ast

import (
	"discern/cfg"
	"discern/ir"
	"discern/ssa"
	"fmt"
)

type Variable struct {
	Name string
}

type StatementName string

const (
	IF      StatementName = "if"
	IFELSE  StatementName = "if_else"
	WHILE   StatementName = "while"
	DOWHILE StatementName = "do_while"
	GOTO    StatementName = "goto"
	ASSIGN  StatementName = "assign"
)

type Statement interface {
	Name() StatementName
	String() string
}

type AssignStatement struct {
	Output Variable
	Input  Expression
}

func (AssignStatement) Name() StatementName {
	return ASSIGN
}

func (stmt *AssignStatement) String() string {
	return fmt.Sprintf("%s := %s", stmt.Output.Name, stmt.Input)
}

type GotoStatement struct {
	Destination Expression
}

func (GotoStatement) Name() StatementName {
	return GOTO
}

func (stmt *GotoStatement) String() string {
	return fmt.Sprintf("goto %s", stmt.Destination.String())
}

type IfElseStatement struct {
	Condition       Expression
	IfDestination   Expression
	ElseDestination Expression
}

func (IfElseStatement) Name() StatementName {
	return IFELSE
}

func (stmt *IfElseStatement) String() string {
	return fmt.Sprintf("if %s -> %s else %s", stmt.Condition.String(), stmt.IfDestination.String(), stmt.ElseDestination.String())
}

type ExpressionOperator string

const (
	VARIABLE ExpressionOperator = ""
	CONSTANT ExpressionOperator = ""

	PLUS   ExpressionOperator = "+"
	MINUS  ExpressionOperator = "-"
	TIMES  ExpressionOperator = "*"
	DIVIDE ExpressionOperator = "/"

	LSHIFT ExpressionOperator = "<<"
	RSHIFT ExpressionOperator = ">>"
	BAND   ExpressionOperator = "&"
	BOR    ExpressionOperator = "|"
	BNOT   ExpressionOperator = "~"
	XOR    ExpressionOperator = "^"

	LESSTHAN     ExpressionOperator = "<"
	LESSEQUAL    ExpressionOperator = "<="
	GREATERTHAN  ExpressionOperator = ">"
	GREATEREQUAL ExpressionOperator = ">="
	EQUALS       ExpressionOperator = "="

	AND ExpressionOperator = "&&"
	OR  ExpressionOperator = "||"
	NOT ExpressionOperator = "!"
)

type Expression interface {
	Operator() ExpressionOperator
	String() string
}

type VariableExpression struct {
	Variable Variable
}

func (*VariableExpression) Operator() ExpressionOperator {
	return VARIABLE
}

func (expr *VariableExpression) String() string {
	return expr.Variable.Name
}

type ConstantExpression struct {
	IntegerValue uint64
}

func (ConstantExpression) Operator() ExpressionOperator {
	return CONSTANT
}

func (expr *ConstantExpression) String() string {
	return fmt.Sprint(expr.IntegerValue)
}

type UnaryExpression struct {
	ExpressionOperator ExpressionOperator
	Input              Expression
}

func (ue *UnaryExpression) Operator() ExpressionOperator {
	return ue.ExpressionOperator
}

func (expr *UnaryExpression) String() string {
	return fmt.Sprintf("%s%s", expr.ExpressionOperator, expr.Input.String())
}

type BinaryExpression struct {
	ExpressionOperator ExpressionOperator
	Left               Expression
	Right              Expression
}

func (be *BinaryExpression) Operator() ExpressionOperator {
	return be.ExpressionOperator
}

func (expr *BinaryExpression) String() string {
	return fmt.Sprintf("%s %s %s", expr.Left.String(), expr.Operator(), expr.Right.String())
}

type ASTDB interface {
	GetIr() ir.IRDB
	GetCfg() *cfg.CFG
	GetSsa() *ssa.SSA

	SetSequence(name string, statements []Statement) error
	GetSequence(name string) ([]Statement, error)
}

type AST struct {
	ASTDB
}

func (a *AST) Build() {
	err := a.GetCfg().DepthFirstTraversal(func(block cfg.Block) {
		blockStatements := MakeBlockStatements(a.GetSsa(), a.GetCfg(), block)
		err := a.SetSequence(string(block.Id), blockStatements)
		if err != nil {
			panic(err)
		}
	})
	if err != nil {
		panic(err)
	}
}

var semanticOperatorTable = map[ir.Semantic]ExpressionOperator{
	ir.MOV:  VARIABLE,
	ir.LOAD: VARIABLE,

	ir.ADD: PLUS,
	ir.SUB: MINUS,
	ir.MUL: TIMES,
	ir.DIV: DIVIDE,

	ir.AND: BAND,
	ir.OR:  BOR,
	ir.SHL: LSHIFT,
	ir.SHR: RSHIFT,
	ir.XOR: XOR,

	ir.LT: LESSTHAN,
	ir.GT: GREATERTHAN,
}

func MakeBlockStatements(s *ssa.SSA, cfgraph *cfg.CFG, block cfg.Block) []Statement {
	entries, err := s.Db.GetBlockEntries(block.Id)
	if err != nil {
		panic(err)
	}

	statements := make([]Statement, 0)

	for _, entry := range entries {
		if entry.SsaSemantic != ssa.OPERATION {
			// TODO: do we need to convert any ssa-specific stuff?
			// it'd probably be helpful in some cases but I don't think
			// it's necessary for now
			fmt.Printf("[w] warning: SSA semantic %s not converted to AST\n", entry.SsaSemantic)
			continue
		}

		semantic := entry.IrSemantic.MustGet()

		var stmt Statement

		if semantic == ir.JT || semantic == ir.JF {
			conditionalSuccessors, err := cfgraph.GetConditionalSuccessors(block.Id)
			if err != nil {
				panic(err)
			}

			stmt = &IfElseStatement{
				Condition:       &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&entry.Inputs[0])},
				IfDestination:   &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&entry.Inputs[1])},
				ElseDestination: &ConstantExpression{IntegerValue: uint64(conditionalSuccessors.Fallthrough.Start)},
			}
		} else if semantic == ir.JMP {
			stmt = &GotoStatement{Destination: &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&entry.Inputs[0])}}
		} else if semantic == ir.TRUNC {
			stmt = &AssignStatement{
				Output: MakeASTVariableFromSSAVariable(&entry.Defined),
				Input: &BinaryExpression{
					ExpressionOperator: BAND,
					Left:               &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&entry.Inputs[0])},
					Right:              &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&entry.Inputs[1])},
				},
			}
		} else if expr, hasDirectTranslation := semanticOperatorTable[semantic]; hasDirectTranslation {
			inputExpressions := make([]Expression, 0)
			for _, input := range entry.Inputs {
				inputExpressions = append(inputExpressions, &VariableExpression{Variable: MakeASTVariableFromSSAVariable(&input)})
			}

			if len(inputExpressions) == 1 {
				stmt = &AssignStatement{
					Output: MakeASTVariableFromSSAVariable(&entry.Defined),
					Input: &UnaryExpression{
						ExpressionOperator: expr,
						Input:              inputExpressions[0],
					},
				}
			} else if len(inputExpressions) == 2 {
				stmt = &AssignStatement{
					Output: MakeASTVariableFromSSAVariable(&entry.Defined),
					Input: &BinaryExpression{
						ExpressionOperator: expr,
						Left:               inputExpressions[0],
						Right:              inputExpressions[1],
					},
				}
			} else {
				fmt.Printf("[w] warning: unsupported SSA entry %s\n", entry.String())
				continue
			}
		} else {
			panic(fmt.Sprintf("[!] error: unsupported semantic %s", semantic))
		}

		statements = append(statements, stmt)
	}

	return statements
}

func MakeASTVariableFromSSAVariable(sv *ssa.Variable) Variable {
	return Variable{
		Name: sv.FullName(),
	}
}
