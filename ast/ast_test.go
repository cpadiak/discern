package ast_test

import (
	"discern/ast"
	"discern/cfg"
	"discern/domtree"
	"discern/internal/id"
	"discern/internal/testcommon"
	"discern/ir"
	"discern/ssa"
	"github.com/go-playground/assert/v2"
	"testing"
)

func TestAstConstructionIterFib(t *testing.T) {
	tid := id.TestID{}

	dbcommon := testcommon.TestCommon{}
	dbtm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&dbtm, &dbcommon)
	loader := testcommon.MakeIterFib(&tid, irdb)
	err := loader.Load()
	if err != nil {
		panic(err)
	}

	cfgdb := testcommon.NewCFGDBTest(irdb)

	cfgraph := cfg.CFG{CFGDB: cfgdb}
	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0x10113a, Space: ir.MemoryAddressSpace})
	if err != nil {
		panic(err)
	}

	dtdb := testcommon.NewTestDomTreeDB(&dbcommon, &dbtm, &cfgraph)
	dt := domtree.DomTree{
		Db: dtdb,
	}

	dt.Build()

	ssadb := testcommon.NewTestSSADB(irdb, &cfgraph, &dt)

	s := ssa.SSA{Db: ssadb}

	s.Build()

	astdb := testcommon.NewTestASTDB(irdb, &cfgraph, &s)

	ast := ast.AST{ASTDB: &astdb}

	ast.Build()

	setupBlock, err := cfgraph.GetBlockContaining(ir.Address{
		Offset: 0x10113a,
		Space:  ir.MemoryAddressSpace,
	})
	assert.Equal(t, err, nil)

	loopBlock, err := cfgraph.GetBlockContaining(ir.Address{
		Offset: 0x10114c,
		Space:  ir.MemoryAddressSpace,
	})
	assert.Equal(t, err, nil)

	retBlock, err := cfgraph.GetBlockContaining(ir.Address{
		Offset: 0x101157,
		Space:  ir.MemoryAddressSpace,
	})
	assert.Equal(t, err, nil)

	setupAst := astdb.Sequences[string(setupBlock.Id)]
	loopAst := astdb.Sequences[string(loopBlock.Id)]
	retAst := astdb.Sequences[string(retBlock.Id)]

	assert.Equal(t, len(setupAst), 6)
	assert.Equal(t, len(loopAst), 7)
	assert.Equal(t, len(retAst), 3)
}

func TestAstConvertOneBlock(t *testing.T) {
	tid := id.TestID{}

	dbcommon := testcommon.TestCommon{}
	dbtm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&dbtm, &dbcommon)

	loader := testcommon.MakeOneBlock(&tid, irdb)
	err := loader.Load()
	assert.Equal(t, err, nil)

	cfgdb := testcommon.NewCFGDBTest(irdb)

	cfgraph := cfg.CFG{CFGDB: cfgdb}
	err = cfgraph.Build()
	assert.Equal(t, err, nil)

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0, Space: ir.MemoryAddressSpace})
	assert.Equal(t, err, nil)

	dtdb := testcommon.NewTestDomTreeDB(&dbcommon, &dbtm, &cfgraph)
	dt := domtree.DomTree{
		Db: dtdb,
	}

	dt.Build()

	ssadb := testcommon.NewTestSSADB(irdb, &cfgraph, &dt)

	s := ssa.SSA{Db: ssadb}

	s.Build()

	statements := ast.MakeBlockStatements(&s, &cfgraph, cfgraph.GetBlock(cfgraph.RootBlockId.MustGet()))
	assert.Equal(t, len(statements), 4)

	assert.Equal(t, statements[0].String(), "0x0:register.1 := imm(0000000000000004).0")
	assert.Equal(t, statements[1].String(), "0x1:register.1 := imm(0000000000000002).0")
	assert.Equal(t, statements[2].String(), "0x0:register.2 := 0x0:register.1 + 0x1:register.1")
	assert.Equal(t, statements[3].String(), "goto 0xa:register.0")
}
