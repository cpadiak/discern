FROM docker.io/golang:1.23 as builder

COPY . .

ENV CGO_ENABLED=0
RUN go build -o ./discern discern && chmod +x discern

#FROM ubuntu:22.04 as debug
#
#COPY --from=builder /go/bin/discern /bin/discern

#FROM scratch
#
#COPY --from=builder /go/bin/discern /bin/discern
#
#CMD ["/bin/discern", "-config", "/config/config.toml"]
