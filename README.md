# Discern

<img src="https://i.imgur.com/YUxfibT.png" width="20" height="20"> **What has been, it is what will be,\
And what has been done, it is what will be done.\
So there is nothing new under the sun.** <img src="https://i.imgur.com/i4FcYTX.png" width="20" height="14"> </br>

Discern is a decompiler and static analysis framework.

## Contributing

Discern is not currently open to contributions.

## Donations

Discern is not currently open to donations.

## License

This code is licensed under the MIT license attached to the repository.
The libraries linked against but not included in source form in this repository
are mostly MIT with a couple Boost and BSD 3 Clause derivative licensed works.
All of these licenses can be considered as "permissive" for most intents and
purposes, but this readme does not constitute legal advice in any way and if
you are unsure about using this project you should seek your own legal representation.
