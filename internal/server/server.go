package server

import (
	"context"
	"discern/cfg"
	"discern/internal/db"
	"discern/internal/loaders/ghidra"
	"discern/ir"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Neo4j struct {
	Db *db.Neo4jDB
}

func (n *Neo4j) IR() ir.IRDB {
	return &ir.IRNeo4j{Neo4jDB: n.Db}
}

func (n *Neo4j) CFG() cfg.CFGDB {
	return &cfg.CFGNeo4j{Neo4jDB: n.Db}
}

type DB interface {
	IR() ir.IRDB
	CFG() cfg.CFGDB
}

type Server struct {
	Ctx *context.Context
	Db  DB
}

func (s *Server) importIR(c *gin.Context, ir ir.IRLoader) {
	ir.Db = s.Db.IR()
	err := ir.Db.VerifyAll()
	if err != nil {
		fmt.Printf("[w] ir failed integrity check with %s\n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintf("ir failed integrity check: %s", err.Error())})
		return
	}

	c.JSON(http.StatusAccepted, gin.H{})

	err = ir.Load()
	if err != nil {
		fmt.Println("[!] ir load failed")
	} else {
		fmt.Println("[i] loaded ir in db")
	}
}

func (s *Server) ImportGhidra(c *gin.Context) {
	var binary ghidra.Binary
	err := c.BindJSON(&binary)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	s.importIR(c, binary.ToIR())
}

type target struct {
	Module string `json:"module"`
}

func (s *Server) Analyze(c *gin.Context) {
	var target target
	err := c.BindJSON(&target)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Printf("[d] analyzing %s\n", target.Module)

	cfgraph := cfg.CFG{ModuleFilename: target.Module, CFGDB: s.Db.CFG()}
	if !cfgraph.IsPresentInDB(cfgraph.ModuleFilename) {
		fmt.Println("[d] no CFG, building")
		err = cfgraph.Build()
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
	}

	c.JSON(http.StatusAccepted, gin.H{})

	fmt.Println("[i] analysis completed")
}
