package db

import (
	"context"
	"discern/internal/id"
	"errors"
	"fmt"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"strings"
)

type Neo4jTransactionInfo struct {
	Transaction neo4j.ExplicitTransaction
	Session     neo4j.SessionWithContext
}

type Neo4jDB struct {
	driver       neo4j.DriverWithContext
	Ctx          *context.Context
	DbName       string
	Transactions map[Transaction]Neo4jTransactionInfo
}

func (db *Neo4jDB) Connect(host string, user *string, password *string) error {
	if db.Ctx == nil {
		fmt.Println("no context")
		return errors.New("db doesn't have context")
	}

	if db.driver != nil {
		fmt.Println("already connected")
		return errors.New("already connected to a DB")
	}

	auth := neo4j.NoAuth()
	if user == nil || password == nil {
		fmt.Printf("[d] user or password not given, using no-auth\n")
	} else {
		auth = neo4j.BasicAuth(*user, *password, "")
	}

	driver, err := neo4j.NewDriverWithContext(host, auth)

	if driver == nil || err != nil {
		fmt.Println("could not connect")
		return errors.New(fmt.Sprintf("could not connect to database: %s", err.Error()))
	}

	err = driver.VerifyConnectivity(*db.Ctx)
	if err != nil {
		driver.Close(*db.Ctx)
		fmt.Println("could not gain connectivity")
		return errors.New(fmt.Sprintf("failed to establish connectivity to database: %s", err.Error()))
	}

	db.driver = driver
	db.EnsureIsConnected()

	fmt.Println("[i] connected to db")
	return nil
}

func (db *Neo4jDB) Disconnect() error {
	if db.driver == nil {
		return nil
	}
	defer func() {
		db.driver = nil
	}()
	return db.driver.Close(*db.Ctx)
}

func (db *Neo4jDB) IsConnected() bool {
	return db.driver != nil
}

func (db *Neo4jDB) EnsureIsConnected() {
	if db.IsConnected() {
		return
	}
	panic("not connected")
}

func (db *Neo4jDB) Wipe() error {
	db.EnsureIsConnected()
	_, err := db.Query("MATCH (n)\n"+
		"DETACH DELETE n", map[string]any{})
	return err
}

func (db *Neo4jDB) BeginTransaction() Transaction {
	db.EnsureIsConnected()

	session := db.driver.NewSession(*db.Ctx, neo4j.SessionConfig{DatabaseName: db.DbName})
	transaction, err := session.BeginTransaction(*db.Ctx)
	if err != nil {
		panic(err.Error())
	}
	transactionID := Transaction(id.MakeUUID())
	db.Transactions[transactionID] = Neo4jTransactionInfo{transaction, session}
	return transactionID
}

func (db *Neo4jDB) CancelTransaction(transaction Transaction) {
	info, present := db.Transactions[transaction]
	if !present {
		panic(transaction)
	}
	info.Transaction.Close(*db.Ctx)
	info.Session.Close(*db.Ctx)
}

func (db *Neo4jDB) CommitTransaction(transaction Transaction) error {
	info, present := db.Transactions[transaction]
	if !present {
		return errors.New("transaction unknown")
	}
	err := info.Transaction.Commit(*db.Ctx)
	if err != nil {
		return err
	}
	err = info.Session.Close(*db.Ctx)
	if err != nil {
		return err
	}
	return nil
}

func (db *Neo4jDB) Query(query string, params map[string]any) (*neo4j.EagerResult, error) {
	if db.Ctx == nil {
		return nil, errors.New("not connected to a db")
	}
	return neo4j.ExecuteQuery(*db.Ctx, db.driver, query, params, neo4j.EagerResultTransformer, neo4j.ExecuteQueryWithDatabase(db.DbName))
}

func (db *Neo4jDB) RunWithTransaction(query string, params map[string]any, transaction Transaction) (neo4j.ResultWithContext, error) {
	return db.Transactions[transaction].Transaction.Run(*db.Ctx, query, params)
}

func (db *Neo4jDB) DeclareUUIDIndex(name string, label string, field string) error {
	db.createUniqueConstraint(name+"_unique", label, field)
	db.createIndex(name+"_index", label, field, "TEXT")
	return nil
}

func (db *Neo4jDB) createUniqueConstraint(name string, tag string, property string) {
	_, err := db.Query(fmt.Sprintf("CREATE CONSTRAINT %s\n"+
		"FOR (n:%s) REQUIRE n.%s IS UNIQUE", name, tag, property), make(map[string]any))
	if err != nil {
		fmt.Printf("[d] %s - %s %s constraint already exists\n", name, tag, property)
	}
}

func (db *Neo4jDB) createIndex(name string, tag string, property string, indexType string) {
	_, err := db.Query(fmt.Sprintf("CREATE %s INDEX %s\n"+
		"FOR (n:%s) ON n.%s", indexType, name, tag, property), make(map[string]any))
	if err != nil {
		fmt.Printf("[d] %s - %s %s index already exists\n", name, tag, property)
	}
}

func (db *Neo4jDB) IsPresent(label string, propertyName string, propertyValue any) (bool, error) {
	result, err := db.GetNodes(mo.Some(label), map[string]any{propertyName: propertyValue})
	if err != nil {
		return false, err
	}
	return len(result) > 0, nil
}

func (db *Neo4jDB) SetNode(label string, uuid id.UUID, fields map[string]any) error {
	var properties []string

	for property := range fields {
		properties = append(properties, fmt.Sprintf("%s: $%s", property, property))
	}

	params := fields
	params["id"] = uuid
	params["label"] = label

	query := fmt.Sprintf("CREATE (o:$label {id: $id, %s})", strings.Join(properties, ", "))
	_, err := db.Query(query, params)
	return err
}

func (db *Neo4jDB) GetNodeByID(uuid id.UUID) ([]map[string]any, error) {
	result, err := db.Query("MATCH (o) WHERE o.id = $id", map[string]any{"id": uuid})
	if err != nil {
		if len(result.Records) > 1 {
			return nil, errors.New("more than one record returned for uuid")
		}
		return []map[string]any{result.Records[0].AsMap()}, nil
	}
	return nil, err
}

func (db *Neo4jDB) GetNodes(label mo.Option[string], fields map[string]any) ([]map[string]any, error) {
	labelq := ""
	if label.IsPresent() {
		labelq = label.MustGet()
	}

	fieldsq := ""
	if len(fields) > 0 {
		fieldsq = fmt.Sprintf("{%s}", strings.Join(lo.MapToSlice(fields, func(property string, value any) string {
			return fmt.Sprintf("%s: $%s", property, property)
		}), ", "))
	}

	params := fields
	params["label"] = labelq

	result, err := db.Query(fmt.Sprintf("MATCH (o:$label %s)", fieldsq), params)
	if err == nil && result != nil {
		return lo.Map(result.Records, func(record *neo4j.Record, _ int) map[string]any {
			return record.AsMap()
		}), nil
	}
	return nil, err
}
