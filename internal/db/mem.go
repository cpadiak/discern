package db

import (
	"discern/internal/id"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"github.com/zyedidia/generic/hashset"
)

type MemDBSet interface {
}

type MemDBChange interface {
	Commit()
	Rollback()
}

type MemDBTransactionInfo struct {
	Changes []MemDBChange
}

type MemDBRelationship struct {
	Left      id.UUID
	Right     id.UUID
	Direction mo.Option[string]
	Label     mo.Option[string]
}

type MemDB struct {
	DbName        string
	Transactions  map[Transaction]MemDBTransactionInfo
	Data          map[string]map[id.UUID][]byte
	Relationships hashset.Set[MemDBRelationship]
}

func (db *MemDB) BeginTransaction() Transaction {
	transaction := Transaction(id.MakeUUID())
	db.Transactions[transaction] = MemDBTransactionInfo{}
	return transaction
}

func (db *MemDB) CancelTransaction(transaction Transaction) {
	info, present := db.Transactions[transaction]
	if !present {
		panic(transaction)
	}
	lo.ForEach(info.Changes, func(change MemDBChange, _ int) {
		change.Rollback()
	})
}

func (db *MemDB) CommitTransaction(transaction Transaction) {
	info, present := db.Transactions[transaction]
	if !present {
		panic(transaction)
	}
	lo.ForEach(info.Changes, func(change MemDBChange, _ int) { change.Commit() })
}

func (db *MemDB) DeclareUUIDIndex(name string, label string, field string) error {
	return nil
}

func (db *MemDB) Wipe() error {
	db.Transactions = map[Transaction]MemDBTransactionInfo{}
	db.DbName = ""
	db.Data = map[string]map[id.UUID][]byte{}
	db.Relationships = hashset.Set[MemDBRelationship]{}
	return nil
}

func (db *MemDB) DeclareLabel(label string) {
	_, present := db.Data[label]
	if !present {
		db.Data[label] = map[id.UUID][]byte{}
	}
}

func (db *MemDB) Set(transaction Transaction, label string, uuid id.UUID, value []byte) {
	db.Data[label][uuid] = value
}

func (db *MemDB) Get(label string, uuid id.UUID) []byte {
	objects, present := db.Data[label]
	if !present {
		panic(label)
	}
	value, present := objects[uuid]
	if !present {
		panic(uuid)
	}
	return value
}

func (db *MemDB) Relate(left id.UUID, right id.UUID, direction mo.Option[string], label mo.Option[string]) {
	db.Relationships.Put(MemDBRelationship{
		Left:      left,
		Right:     right,
		Direction: direction,
		Label:     label,
	})
}
