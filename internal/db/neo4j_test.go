package db

import (
	"context"
	"fmt"
	"github.com/go-playground/assert/v2"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/neo4j"
	"log"
	"testing"
)

func TestNeo4jConnectDisconnect(t *testing.T) {
	ctx := context.Background()

	user := "neo4j"
	password := "verylong"

	container, err := neo4j.RunContainer(
		ctx,
		testcontainers.WithImage("docker.io/neo4j:latest"),
		neo4j.WithLogger(testcontainers.TestLogger(t)),
		neo4j.WithAdminPassword(password),
		neo4j.WithLabsPlugin(neo4j.Apoc),
		neo4j.WithNeo4jSetting("dbms.tx_log.rotation.size", "42M"),
	)
	if err != nil {
		panic(err.Error())
	}
	defer func() {
		println("terminating container")
		if err := container.Terminate(ctx); err != nil {
			panic(err.Error())
		}
	}()

	containerIP, err := container.ContainerIP(ctx)
	if err != nil {
		log.Fatalf("failed to get container ip: %s", err.Error())
	}

	neo4jDB := Neo4jDB{DbName: "neo4j", Ctx: &ctx}
	err = neo4jDB.Connect(fmt.Sprintf("neo4j://%s", containerIP), &user, &password)
	if err != nil {
		panic(err.Error())
	}

	assert.Equal(t, true, neo4jDB.IsConnected())

	assert.Equal(t, nil, neo4jDB.Disconnect())
}
