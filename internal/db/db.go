package db

import (
	"discern/internal/id"
)

type Transaction id.UUID

type TransactionManager interface {
	BeginTransaction() Transaction
	CancelTransaction(transaction Transaction)
	CommitTransaction(transaction Transaction) error
}

type Common interface {
	DeclareUUIDIndex(name string, label string, field string) error
	Wipe() error
}
