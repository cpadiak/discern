package loaders_test

import (
	"discern/internal/loaders/ghidra"
	"discern/internal/testcommon"
	"discern/ir"
	"encoding/json"
	"fmt"
	"os"
	"testing"
)

func loadFileToBinary(filepath string) (ghidra.Binary, error) {
	var binary ghidra.Binary
	// path is relative to this test file
	file, err := os.ReadFile(filepath)
	if err != nil {
		return binary, err
	}
	err = json.Unmarshal(file, &binary)
	if err != nil {
		return binary, err
	}

	return binary, nil
}

func TestGhidraLoadLS(t *testing.T) {
	// path is relative to this test file
	binary, err := loadFileToBinary("../../data/ls.json")
	if err != nil {
		panic(err)
	}

	bir := binary.ToIR()
	bir.Db = testcommon.NewIRDBTest(&testcommon.TestTransactionManager{}, &testcommon.TestCommon{})
	err = bir.IntegrityCheck()
	if err != nil {
		panic(err)
	}

	err = bir.Load()
	if err != nil {
		panic(err)
	}
}

func TestGhidraLoadNeo4jLS(t *testing.T) {
	containerInfo := testcommon.Neo4jContainer(t, "neo4j", "verylong")

	neo4jDB := containerInfo.N4DB

	for _, deference := range containerInfo.Deferences {
		defer deference()
	}

	// path is relative to this test file
	binary, err := loadFileToBinary("../../data/ls.json")
	if err != nil {
		panic(err)
	}

	bir := binary.ToIR()
	bir.Db = &ir.IRNeo4j{Neo4jDB: neo4jDB}
	err = bir.IntegrityCheck()
	if err != nil {
		panic(err)
	}
	fmt.Println("[d] passed integrity check")

	err = bir.Load()
	if err != nil {
		panic(err)
	}
}
