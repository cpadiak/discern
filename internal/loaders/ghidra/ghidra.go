package ghidra

import (
	"discern/internal/id"
	"discern/internal/util"
	"discern/ir"
	"errors"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"strconv"
	"strings"
)

type Varnode struct {
	Offset int64  `json:"offset"`
	Space  string `json:"space"`
	Size   uint64 `json:"size"`
	Name   string `json:"name"`
}

func (v *Varnode) String() string {
	if v.Name != "" {
		return v.Name
	}
	return fmt.Sprintf("%s:%x:%d", v.Space, v.Offset, v.Size)
}

type PCode struct {
	Operation string    `json:"operation"`
	Inputs    []Varnode `json:"inputs"`
	Output    *Varnode  `json:"output"`
}

func (p *PCode) String() string {
	output := ""
	if p.Output != nil {
		output = fmt.Sprintf("%s = ", p.Output.String())
	}
	return fmt.Sprintf("%s%s %s", output, p.Operation, strings.Join(lo.Map(p.Inputs, func(vn Varnode, index int) string {
		return vn.String()
	}), ", "))
}

type Instruction struct {
	Address  string  `json:"address"`
	Mnemonic string  `json:"mnemonic"`
	Pcode    []PCode `json:"pcode"`
}

type AddressSpace struct {
	Name string `json:"name"`
	Id   int    `json:"id"`
}

type MemoryBlock struct {
	Name    string `json:"name"`
	Address string `json:"address"`
	Size    uint64 `json:"size"`
	Read    bool   `json:"read"`
	Write   bool   `json:"write"`
	Execute bool   `json:"execute"`
}

type Binary struct {
	Filename      string         `json:"filename"`
	Endianness    string         `json:"endianness"`
	AddressSpaces []AddressSpace `json:"address_spaces"`
	Instructions  []Instruction  `json:"instructions"`
	MemoryBlocks  []MemoryBlock  `json:"memory_blocks"`
}

func translateAddress(address string) ir.Address {
	offset, err := strconv.ParseUint(address, 16, 64)
	if err != nil {
		panic(err.Error())
	}
	return ir.Address{Offset: offset, Space: ir.MemoryAddressSpace}
}

var DirectOperationTranslation = map[string]ir.Semantic{
	"BOOL_AND":     ir.AND,
	"BOOL_NEGATE":  ir.NEG,
	"BOOL_OR":      ir.OR,
	"BOOL_XOR":     ir.XOR,
	"CALLOTHER":    ir.USERDEFINED,
	"COPY":         ir.MOV,
	"FLOAT2FLOAT":  ir.CONV,
	"FLOAT_ADD":    ir.ADD,
	"FLOAT_DIV":    ir.DIV,
	"FLOAT_EQUAL":  ir.EQ,
	"FLOAT_LESS":   ir.LT,
	"FLOAT_MULT":   ir.MUL,
	"FLOAT_NAN":    ir.NACV,
	"FLOAT_SUB":    ir.SUB,
	"INT_2COMP":    ir.NEG,
	"INT2FLOAT":    ir.CONV,
	"INT_ADD":      ir.ADD,
	"INT_AND":      ir.AND,
	"INT_CARRY":    ir.UOF,
	"INT_DIV":      ir.DIV,
	"INT_EQUAL":    ir.EQ,
	"INT_LEFT":     ir.SHL,
	"INT_LESS":     ir.LT,
	"INT_MULT":     ir.MUL,
	"INT_NEGATE":   ir.NEG,
	"INT_NOTEQUAL": ir.NEQ,
	"INT_OR":       ir.OR,
	"INT_REM":      ir.MOD,
	"INT_RIGHT":    ir.SHR,
	"INT_SBORROW":  ir.SUF,
	"INT_SCARRY":   ir.SOF,
	"INT_SDIV":     ir.DIV,
	"INT_SEXT":     ir.EXTEND,
	"INT_SLESS":    ir.LT,
	"INT_SREM":     ir.MOD,
	"INT_SRIGHT":   ir.SHR,
	"INT_SUB":      ir.SUB,
	"INT_XOR":      ir.XOR,
	"INT_ZEXT":     ir.EXTEND,
	"LOAD":         ir.LOAD,
	"PIECE":        ir.CONCAT,
	"POPCOUNT":     ir.POPCOUNT,
	"ROUND":        ir.ROUND,
	"STORE":        ir.STORE,
	"SUBPIECE":     ir.TRUNC,
	"TRUNC":        ir.CONV,
}

var OperationRepresentationTranslation = map[string]map[int]ir.Representation{
	"BOOL_AND": {
		0:  ir.BOOL,
		1:  ir.BOOL,
		-1: ir.BOOL,
	},
	"BOOL_NEGATE": {
		0:  ir.BOOL,
		1:  ir.BOOL,
		-1: ir.BOOL,
	},
	"BOOL_OR": {
		0:  ir.BOOL,
		1:  ir.BOOL,
		-1: ir.BOOL,
	},
	"BOOL_XOR": {
		0:  ir.BOOL,
		1:  ir.BOOL,
		-1: ir.BOOL,
	},
	"COPY": {
		0:  ir.BINARY,
		-1: ir.BINARY,
	},
	"FLOAT2FLOAT": {
		0:  ir.FLOAT,
		-1: ir.FLOAT,
	},
	"FLOAT_ADD": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.FLOAT,
	},
	"FLOAT_DIV": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.FLOAT,
	},
	"FLOAT_EQUAL": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.BOOL,
	},
	"FLOAT_LESS": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.BOOL,
	},
	"FLOAT_MULT": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.FLOAT,
	},
	"FLOAT_NAN": {
		0:  ir.FLOAT,
		-1: ir.BOOL,
	},
	"FLOAT_SUB": {
		0:  ir.FLOAT,
		1:  ir.FLOAT,
		-1: ir.FLOAT,
	},
	"INT_2COMP": {
		0:  ir.SINT,
		-1: ir.SINT,
	},
	"INT2FLOAT": {
		0:  ir.SINT,
		-1: ir.FLOAT,
	},
	"INT_ADD": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_AND": {
		0:  ir.BINARY,
		1:  ir.BINARY,
		-1: ir.BINARY,
	},
	"INT_CARRY": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.BOOL,
	},
	"INT_DIV": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_EQUAL": {
		0:  ir.BINARY,
		1:  ir.BINARY,
		-1: ir.BOOL,
	},
	"INT_LEFT": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.UINT,
	},
	"INT_LESS": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.BOOL,
	},
	"INT_MULT": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_NEGATE": {
		0:  ir.BINARY,
		-1: ir.BINARY,
	},
	"INT_NOTEQUAL": {
		0:  ir.BINARY,
		1:  ir.BINARY,
		-1: ir.BOOL,
	},
	"INT_OR": {
		0:  ir.BINARY,
		1:  ir.BINARY,
		-1: ir.BINARY,
	},
	"INT_REM": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.UINT,
	},
	"INT_RIGHT": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.UINT,
	},
	"INT_SBORROW": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.BOOL,
	},
	"INT_SCARRY": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.BOOL,
	},
	"INT_SDIV": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_SEXT": {
		0:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_SLESS": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.BOOL,
	},
	"INT_SREM": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_SRIGHT": {
		0:  ir.SINT,
		1:  ir.UINT,
		-1: ir.SINT,
	},
	"INT_SUB": {
		0:  ir.SINT,
		1:  ir.SINT,
		-1: ir.SINT,
	},
	"INT_XOR": {
		0:  ir.BINARY,
		1:  ir.BINARY,
		-1: ir.BINARY,
	},
	"INT_ZEXT": {
		0:  ir.BINARY,
		-1: ir.BINARY,
	},
	"PIECE": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.UINT,
	},
	"POPCOUNT": {
		0:  ir.BINARY,
		-1: ir.UINT,
	},
	"ROUND": {
		0:  ir.FLOAT,
		1:  ir.BINARY,
		-1: ir.FLOAT,
	},
	"SUBPIECE": {
		0:  ir.UINT,
		1:  ir.UINT,
		-1: ir.UINT,
	},
	"TRUNC": {
		0:  ir.SINT,
		-1: ir.FLOAT,
	},
}

func translateVarnodeLocation(varnode Varnode) ir.Location {
	var location ir.Location

	switch varnode.Space {
	case "register":
		location.Address = ir.Address{
			Offset: uint64(varnode.Offset),
			Space:  ir.RegisterAddressSpace,
		}
	case "ram":
		location.Address = ir.Address{
			Offset: uint64(varnode.Offset),
			Space:  ir.MemoryAddressSpace,
		}
	case "unique":
		location.Address = ir.Address{
			Offset: uint64(varnode.Offset),
			Space:  ir.TemporaryAddressSpace,
		}
	case "const":
		fmt.Printf("[!] should have been unreachable")
	default:
		fmt.Printf("[w] unknown varnode space: %s\n", varnode.Space)
	}

	location.Id = id.MakeUUID()

	return location
}

func translateVarnodeRepresentation(operation string, varnode Varnode, varnodePosition int) (ir.Representation, error) {
	if operation == "CALLOTHER" {
		if varnodePosition == 0 {
			return ir.NAME, nil
		}
		return ir.BINARY, nil
	}
	representation, present := OperationRepresentationTranslation[operation][varnodePosition]
	if !present {
		fmt.Printf("[w] could not find representation for %s %s %d\n", operation, varnode.String(), varnodePosition)
		return "", errors.New("no representation found")
	}
	return representation, nil
}

var BranchingPcodes = map[string]struct{}{
	"BRANCH":    {},
	"CALL":      {},
	"BRANCHIND": {},
	"CALLIND":   {},
	"RETURN":    {},
	"CBRANCH":   {},
}

func translateBranch(pcode PCode, operationIndex ir.OperationIndex) ir.Operation {
	target := pcode.Inputs[0]

	op := pcode.Operation

	var operands []ir.Operand

	bitSize := uint(target.Size)
	representation := ir.UINT

	var mode ir.BranchMode
	var semantic ir.Semantic
	var absoluteTarget mo.Option[ir.Address]
	var location mo.Option[ir.Location]

	if target.Space == "const" {
		mode = ir.RELATIVE
	} else {
		switch op {
		case "BRANCH", "CALL", "CBRANCH":
			if op == "CBRANCH" {
				semantic = ir.JT
			} else {
				semantic = ir.JMP
			}

			mode = ir.DIRECT
			absoluteTarget = mo.Some(translateVarnodeLocation(target).Address)
		case "BRANCHIND", "CALLIND", "RETURN":
			mode = ir.INDIRECT
			location = mo.Some(translateVarnodeLocation(target))
		default:
			panic(fmt.Sprintf("unsupported operation: %s", op))
		}
	}

	branchTarget := ir.BranchOperand{
		Id:                   id.MakeUUID(),
		BranchMode:           mode,
		AbsoluteBranchTarget: absoluteTarget,
		IndirectBranchTarget: location,
		Representation:       representation,
		BitSize:              bitSize,
	}

	operands = append(operands, &branchTarget)

	// leave the check for jf in case we use it at some point
	// in the future
	if semantic == ir.JT || semantic == ir.JF {
		conditionVarnode := pcode.Inputs[1]

		var conditionOperand ir.Operand

		if conditionVarnode.Space == "const" {
			conditionOperand = &ir.ImmediateOperand{Id: id.MakeUUID(), ImmediateValue: util.ToBytesBE(uint64(conditionVarnode.Offset)), BitSize: uint(conditionVarnode.Size), Representation: ir.BOOL}
		} else {
			conditionOperand = &ir.ContainerOperand{
				Id:             id.MakeUUID(),
				Location:       translateVarnodeLocation(conditionVarnode),
				Representation: ir.BOOL,
				BitSize:        uint(conditionVarnode.Size),
			}
		}

		operands = append(operands, conditionOperand)
	}

	return ir.Operation{
		Id:       id.MakeUUID(),
		Semantic: semantic,
		Inputs:   operands,
		Output:   nil,
		Index:    operationIndex,
	}
}

func translateVarnodeWithRepresentation(varnode Varnode, representation ir.Representation) (ir.Operand, error) {
	if varnode.Space == "const" {
		return &ir.ImmediateOperand{Id: id.MakeUUID(), ImmediateValue: util.ToBytesBE(uint64(varnode.Offset)), BitSize: uint(varnode.Size), Representation: representation}, nil
	}

	location := translateVarnodeLocation(varnode)

	return &ir.ContainerOperand{
		Id:             id.MakeUUID(),
		Location:       location,
		Representation: representation,
		BitSize:        uint(varnode.Size),
	}, nil
}

func translateVarnode(operation string, varnode Varnode, varnodePosition int) (ir.Operand, error) {
	representation, reperr := translateVarnodeRepresentation(operation, varnode, varnodePosition)

	if reperr != nil {
		fmt.Printf("[!] failed to determine representation for %s %s\n", operation, varnode.String())
		return nil, errors.New("no representation")
	}

	return translateVarnodeWithRepresentation(varnode, representation)
}

func (b *Binary) ToIR() ir.IRLoader {
	module := ir.Module{Filename: b.Filename, Endianness: ir.Endianness(b.Endianness), Sections: make([]id.UUID, 0)}

	instructions := make([]ir.Instruction, 0)
	operations := make([]ir.Operation, 0)

	var operationIndex ir.OperationIndex = 0

	for _, instruction := range b.Instructions {
		localOperations := make([]ir.Operation, 0)
		for _, pcode := range instruction.Pcode {
			if pcode.Operation == "STORE" {
				pointer, err := translateVarnodeWithRepresentation(pcode.Inputs[1], ir.UINT)
				if err != nil {
					panic(err)
				}
				data, err := translateVarnodeWithRepresentation(pcode.Inputs[2], ir.BINARY)
				if err != nil {
					panic(err)
				}
				operation := ir.Operation{Id: id.MakeUUID(), Semantic: ir.STORE, Inputs: []ir.Operand{data}, Output: &pointer, Index: operationIndex}
				localOperations = append(localOperations, operation)
				operationIndex += 1
				continue
			} else if pcode.Operation == "LOAD" {
				pointer, err := translateVarnodeWithRepresentation(pcode.Inputs[1], ir.UINT)
				if err != nil {
					panic(err)
				}
				data, err := translateVarnodeWithRepresentation(*pcode.Output, ir.BINARY)
				if err != nil {
					panic(err)
				}
				operation := ir.Operation{Id: id.MakeUUID(), Semantic: ir.LOAD, Inputs: []ir.Operand{pointer}, Output: &data, Index: operationIndex}
				localOperations = append(localOperations, operation)
				operationIndex += 1
				continue
			}

			if _, isBranch := BranchingPcodes[pcode.Operation]; isBranch {
				localOperations = append(localOperations, translateBranch(pcode, operationIndex))
				operationIndex += 1
				//fmt.Printf("translated branch %s -> %s\n\n", pcode.String(), operations[len(operations)-1].String())
				continue
			}

			semantic, present := DirectOperationTranslation[pcode.Operation]
			if !present {
				fmt.Printf("[w] unknown pcode operation: %s\n", pcode.Operation)
				semantic = ir.USERDEFINED
			}

			var inputs []ir.Operand

			for position, inputVarnode := range pcode.Inputs {
				vn, err := translateVarnode(pcode.Operation, inputVarnode, position)
				if err != nil {
					fmt.Printf("[w] inserting nil for failed input varnode translation\n")
					inputs = append(inputs, nil)
					continue
				}
				inputs = append(inputs, vn)
			}

			var output *ir.Operand = nil
			if pcode.Output != nil {
				translatedOutput, err := translateVarnode(pcode.Operation, *pcode.Output, -1)
				if err != nil {
					fmt.Printf("[w] inserting nil for failed output varnode translation\n")
					output = nil
				} else {
					output = &translatedOutput
				}
			}

			operation := ir.Operation{Id: id.MakeUUID(), Semantic: semantic, Inputs: inputs, Output: output, Index: operationIndex}
			localOperations = append(localOperations, operation)
			operationIndex += 1
		}

		insn := ir.Instruction{Id: id.MakeUUID(), Address: translateAddress(instruction.Address), Mnemonic: instruction.Mnemonic, Operations: lo.Map(localOperations, func(operation ir.Operation, _ int) id.UUID {
			return operation.Id
		})}
		instructions = append(instructions, insn)
		operations = append(operations, localOperations...)
	}

	var sections []ir.Section
	for _, block := range b.MemoryBlocks {
		permissions := ""
		if block.Read {
			permissions += "READ"
		}
		if block.Write {
			permissions += "_WRITE_"
		}
		if block.Execute {
			permissions += "_EXECUTE"
		}
		permissions, _ = strings.CutPrefix(permissions, "_")
		permissions, _ = strings.CutSuffix(permissions, "_")

		address := translateAddress(block.Address)

		sectionInstructions := make([]id.UUID, 0)
		//var insnIndices []int
		for _, insn := range instructions {
			if address.Offset <= insn.Address.Offset && insn.Address.Offset <= address.Offset+block.Size {
				sectionInstructions = append(sectionInstructions, insn.Id)
				//insnIndices = append(insnIndices, i)
			}
		}

		sectionId := id.MakeUUID()
		section := ir.Section{Id: sectionId, Permissions: ir.MemoryPermission(permissions), Address: address, Size: block.Size, Data: make([]byte, 0), Name: block.Name, Instructions: sectionInstructions}

		if !ir.IsMemoryFree(sections, section.Address.Offset, section.Size) {
			fmt.Printf("[!] integrity check failed: section %s overlaps an existing section. skipping\n", section.Name)
			continue
		}

		sections = append(sections, section)
		module.Sections = append(module.Sections, sectionId)
	}

	totalInsns := len(lo.Flatten(lo.Map(sections, func(section ir.Section, index int) []id.UUID {
		return section.Instructions
	})))

	expectedInsns := len(b.Instructions)
	if totalInsns != expectedInsns {
		fmt.Printf("[!] integrity check failed: expected %d instructions, had %d\n", expectedInsns, totalInsns)
	}

	return ir.NewIRLoader([]ir.Module{module}, sections, instructions, operations, nil, nil)
}
