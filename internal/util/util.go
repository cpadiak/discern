package util

import (
	"discern/internal/id"
	"encoding/binary"
	"encoding/json"
	"github.com/zyedidia/generic"
	"github.com/zyedidia/generic/hashset"
)

func ToBytesBE(n uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, n)
	return b
}

func FromBytesBE(b []byte) uint64 {
	return binary.BigEndian.Uint64(b)
}

func ToBytesLE(n uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, n)
	return b
}

func FromBytesLE(b []byte) uint64 {
	return binary.LittleEndian.Uint64(b)
}

func NewUUIDHashset() *hashset.Set[id.UUID] {
	return hashset.New[id.UUID](0, generic.Equals[id.UUID], func(i id.UUID) uint64 {
		return generic.HashString(string(i))
	})
}

func Slice2Map[E comparable](s []E) map[E]struct{} {
	result := make(map[E]struct{}, len(s))
	for _, e := range s {
		result[e] = struct{}{}
	}
	return result
}

func MapToPrettyJson(m map[string]any) string {
	result, err := json.MarshalIndent(m, "", "\t")
	if err != nil {
		panic(err)
	}
	return string(result)
}
