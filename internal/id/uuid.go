package id

import (
	"github.com/google/uuid"
	"strconv"
)

type UUID string

type Generator interface {
	MakeUUID() UUID
}

func MakeUUID() UUID {
	return UUID(uuid.New().String())
}

type TestID struct {
	Count uint64
}

func (tid *TestID) MakeUUID() UUID {
	tid.Count += 1
	return UUID(strconv.FormatUint(tid.Count, 10))
}
