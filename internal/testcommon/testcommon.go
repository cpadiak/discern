package testcommon

import (
	"context"
	"discern/ast"
	"discern/cfg"
	"discern/domtree"
	"discern/internal/db"
	"discern/internal/id"
	"discern/internal/util"
	"discern/ir"
	"discern/ssa"
	"errors"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/neo4j"
	"github.com/testcontainers/testcontainers-go/wait"
	"github.com/zyedidia/generic/mapset"
	"log"
	"slices"
	"testing"
)

func loc(id id.UUID, offset uint64, space ir.AddressSpace) ir.Location {
	return ir.Location{Id: id, Address: ir.Address{Offset: offset, Space: space}}
}

func mov(id id.UUID, dst ir.Operand, src ir.Operand) ir.Operation {
	return ir.Operation{Id: id, Semantic: ir.MOV, Inputs: []ir.Operand{src}, Output: &dst}
}

func generic(id id.UUID, semantic ir.Semantic, left ir.Operand, right ir.Operand, output ir.Operand) ir.Operation {
	return ir.Operation{Id: id, Semantic: semantic, Inputs: []ir.Operand{left, right}, Output: &output}
}

func jmp(id id.UUID, dst ir.Operand) ir.Operation {
	if dst.GetBranch() == nil {
		panic("bad branch operand")
	}
	return ir.Operation{Id: id, Semantic: ir.JMP, Inputs: []ir.Operand{dst}}
}

func jt(id id.UUID, dst ir.Operand, condition ir.Operand) ir.Operation {
	if dst.GetBranch() == nil {
		panic("bad branch operand")
	}
	return ir.Operation{
		Id:       id,
		Semantic: ir.JT,
		Inputs:   []ir.Operand{dst, condition},
		Output:   nil,
	}
}

func branch(id id.UUID, bitSize uint, mode ir.BranchMode, target mo.Option[ir.Address], location mo.Option[ir.Location]) *ir.BranchOperand {
	branch := ir.BranchOperand{Id: id, BitSize: bitSize, BranchMode: mode, AbsoluteBranchTarget: target, IndirectBranchTarget: location, Representation: ir.BINARY}
	return &branch
}

func load(tid id.Generator, src ir.Operand, dst ir.Operand) ir.Operation {
	return ir.Operation{
		Id:       tid.MakeUUID(),
		Semantic: ir.LOAD,
		Inputs:   []ir.Operand{src},
		Output:   &dst,
	}
}

func reg(id id.UUID, lid id.UUID, offset uint64, bitSize uint, isIp bool) ir.Operand {
	return &ir.ContainerOperand{Id: id, Location: loc(lid, offset, ir.RegisterAddressSpace), Representation: ir.UINT, BitSize: bitSize, IsIP: isIp}
}

func tmp(tid id.Generator, offset uint64, bitSize uint) ir.Operand {
	return &ir.ContainerOperand{Id: tid.MakeUUID(), Location: loc(tid.MakeUUID(), offset, ir.TemporaryAddressSpace), Representation: ir.UINT, BitSize: bitSize}
}

func imm(id id.UUID, bitSize uint, value mo.Option[[]byte]) ir.Operand {
	if value.IsAbsent() {
		value = mo.Some(make([]byte, 0))
	}
	return &ir.ImmediateOperand{
		Id:             id,
		BitSize:        bitSize,
		Representation: ir.UINT,
		ImmediateValue: value.MustGet(),
	}
}

func insn(uuid id.UUID, mnemonic string, offset uint64, size uint, operations []id.UUID) ir.Instruction {
	return ir.Instruction{Id: uuid, Address: ir.Address{Offset: offset, Space: ir.MemoryAddressSpace}, Size: size, Mnemonic: mnemonic, Operations: operations}
}

type TestTransactionManager struct {
}

func (tm *TestTransactionManager) BeginTransaction() db.Transaction {
	return db.Transaction(id.MakeUUID())
}

func (tm *TestTransactionManager) CancelTransaction(transaction db.Transaction) {

}

func (tm *TestTransactionManager) CommitTransaction(transaction db.Transaction) error {
	return nil
}

type TestCommon struct{}

func (db *TestCommon) DeclareUUIDIndex(name string, label string, field string) error {
	return nil
}

func (db *TestCommon) Wipe() error {
	return nil
}

type IRDBTest struct {
	DbTransactionManager *TestTransactionManager
	DbCommon             *TestCommon
	Modules              map[id.UUID]ir.Module
	Sections             map[id.UUID]ir.Section
	Instructions         map[id.UUID]ir.Instruction
	LinearInstructions   map[uint]ir.Instruction
	InstructionAddresses map[ir.Address]ir.Instruction
	InsnOps              map[id.UUID][]id.UUID
	Operations           map[id.UUID]ir.Operation
	OperationIndices     map[ir.OperationIndex]ir.Operation
	Operands             map[id.UUID]ir.Operand
}

func NewIRDBTest(transactionManager *TestTransactionManager, common *TestCommon) *IRDBTest {
	var irdb IRDBTest
	irdb.DbTransactionManager = transactionManager
	irdb.DbCommon = common
	irdb.Modules = make(map[id.UUID]ir.Module)
	irdb.Sections = make(map[id.UUID]ir.Section)
	irdb.Instructions = make(map[id.UUID]ir.Instruction)
	irdb.LinearInstructions = make(map[uint]ir.Instruction)
	irdb.InstructionAddresses = make(map[ir.Address]ir.Instruction)
	irdb.InsnOps = make(map[id.UUID][]id.UUID)
	irdb.Operations = make(map[id.UUID]ir.Operation)
	irdb.OperationIndices = make(map[ir.OperationIndex]ir.Operation)
	irdb.Operands = make(map[id.UUID]ir.Operand)
	return &irdb
}

func (irdb *IRDBTest) Transaction() db.TransactionManager {
	return irdb.DbTransactionManager
}

func (irdb *IRDBTest) Common() db.Common {
	return irdb.DbCommon
}

func (irdb *IRDBTest) SetModule(module ir.Module, transaction db.Transaction) {
	irdb.Modules[module.Id] = module
}

func (irdb *IRDBTest) GetModule(uuid id.UUID) mo.Option[ir.Module] {
	return mo.Some(irdb.Modules[uuid])
}

func (irdb *IRDBTest) SetSection(section ir.Section, moduleId id.UUID, transaction db.Transaction) {
	irdb.Sections[section.Id] = section
}

func (irdb *IRDBTest) GetSection(uuid id.UUID) mo.Option[ir.Section] {
	return mo.Some(irdb.Sections[uuid])
}

func (irdb *IRDBTest) SetInstruction(instruction ir.Instruction, sectionId id.UUID, position uint, transaction db.Transaction) {
	if _, exists := irdb.Instructions[instruction.Id]; exists {
		panic(fmt.Sprintf("instruction %s already exists", instruction.Id))
	}
	irdb.Instructions[instruction.Id] = instruction
	irdb.LinearInstructions[position] = instruction
	irdb.InstructionAddresses[instruction.Address] = instruction
	irdb.InsnOps[instruction.Id] = make([]id.UUID, 0)
}

func (irdb *IRDBTest) GetInstruction(uuid id.UUID) mo.Option[ir.Instruction] {
	return mo.Some(irdb.Instructions[uuid])
}

func (irdb *IRDBTest) GetInstructionOperations(uuid id.UUID) ([]ir.Operation, error) {
	ops := irdb.InsnOps[uuid]
	return lo.Map(ops, func(opid id.UUID, index int) ir.Operation {
		return irdb.Operations[opid]
	}), nil
}

func (irdb *IRDBTest) GetInstructionByExactAddress(address ir.Address) (ir.Instruction, error) {
	return irdb.InstructionAddresses[address], nil
}

func (irdb *IRDBTest) GetInstructionContaining(address ir.Address) (ir.Instruction, error) {
	for addr, instruction := range irdb.InstructionAddresses {
		if addr.Space != address.Space {
			continue
		}
		if addr.Offset <= address.Offset && address.Offset < addr.Offset+uint64(instruction.Size) {
			return instruction, nil
		}
	}
	return ir.Instruction{}, errors.New("instruction not found")
}

func (irdb *IRDBTest) SetOperation(operation ir.Operation, instructionId id.UUID, instructionPosition uint, totalPosition uint, transaction db.Transaction) error {
	if _, exists := irdb.Operations[operation.Id]; exists {
		return errors.New(fmt.Sprintf("operation %s already exists", operation.Id))
	}
	if operation.Index == ir.OperationIndex(0) && instructionPosition != 0 {
		return errors.New(fmt.Sprintf("[!] error: are operation indexes getting set? operation at %d did not have an index field\n", totalPosition))
	}
	operation.Index = ir.OperationIndex(totalPosition)
	irdb.Operations[operation.Id] = operation
	irdb.OperationIndices[operation.Index] = operation
	irdb.InsnOps[instructionId] = append(irdb.InsnOps[instructionId], operation.Id)

	for inputPos, input := range operation.Inputs {
		irdb.SetOperand(input, operation.Id, inputPos, transaction)
	}

	if operation.Output != nil {
		irdb.SetOperand(*operation.Output, operation.Id, -1, transaction)
	}

	return nil
}

func (irdb *IRDBTest) GetOperation(uuid id.UUID) (ir.Operation, error) {
	return irdb.Operations[uuid], nil
}

func (irdb *IRDBTest) GetOperationByIndex(index ir.OperationIndex) (ir.Operation, error) {
	return irdb.OperationIndices[index], nil
}

func (irdb *IRDBTest) GetOperationsInRange(start ir.OperationIndex, end ir.OperationIndex) ([]ir.Operation, error) {
	operations := lo.Filter(lo.Values(irdb.Operations), func(operation ir.Operation, _ int) bool {
		return start <= operation.Index && operation.Index <= end
	})
	slices.SortFunc(operations, func(a, b ir.Operation) int {
		return int(a.Index - b.Index)
	})
	return operations, nil
}

func (irdb *IRDBTest) SetOperand(operand ir.Operand, operationId id.UUID, position int, transaction db.Transaction) {
	irdb.Operands[operand.GetId()] = operand
}

func (irdb *IRDBTest) GetOperand(uuid id.UUID) (ir.Operand, error) {
	return irdb.Operands[uuid], nil
}

func (irdb *IRDBTest) GetOperands() ([]id.UUID, error) {
	return lo.Keys(irdb.Operands), nil
}

func (irdb *IRDBTest) GetOperandWrites(operand id.UUID) ([]ir.Operation, error) {
	return lo.Filter(lo.Values(irdb.Operations), func(operation ir.Operation, _ int) bool {
		if operation.Output == nil {
			return false
		}
		output := *operation.Output
		return output.GetId() == operand
	}), nil
}

func (irdb *IRDBTest) GetOperandReads(operand id.UUID) ([]ir.Operation, error) {
	return lo.Filter(lo.Values(irdb.Operations), func(operation ir.Operation, _ int) bool {
		return lo.Contains(lo.Map(operation.Inputs, func(input ir.Operand, _ int) id.UUID {
			return input.GetId()
		}), operand)
	}), nil
}

func (irdb *IRDBTest) GetInstructionPointer() (ir.Operand, error) {
	ip, found := lo.Find(lo.Values(irdb.Operands), func(operand ir.Operand) bool {
		return operand.IsInstructionPointer()
	})
	if !found {
		return nil, errors.New("no instruction pointer known in db")
	}
	return ip, nil
}

func (irdb *IRDBTest) SetInstructionPointer(operand ir.Operand) error {
	irdb.Operands[operand.GetId()] = operand
	return nil
}

func (irdb *IRDBTest) ConvertAddressToOperationIndex(address ir.Address) (ir.OperationIndex, error) {
	insn, err := irdb.GetInstructionContaining(address)
	if err != nil {
		return ir.OperationIndex(0), err
	}
	op, err := irdb.GetOperation(insn.Operations[0])
	if err != nil {
		return ir.OperationIndex(0), err
	}
	//fmt.Printf("%s maps to %d\n", address.String(), op.Index)
	return op.Index, nil
}

func (irdb *IRDBTest) VerifyAll() error {
	err := irdb.VerifySectionBoundaries()
	if err != nil {
		return err
	}
	err = irdb.VerifyInstructionBoundaries()
	if err != nil {
		return err
	}
	return nil
}

func (irdb *IRDBTest) VerifySectionBoundaries() error {
	return nil
}

func (irdb *IRDBTest) VerifyInstructionBoundaries() error {
	return nil
}

type CFGDBTest struct {
	Ir     *IRDBTest
	Blocks map[id.UUID]cfg.Block
	Edges  map[cfg.Edge]struct{}
}

func NewCFGDBTest(irdb *IRDBTest) *CFGDBTest {
	var cfgdb CFGDBTest
	cfgdb.Ir = irdb
	cfgdb.Blocks = make(map[id.UUID]cfg.Block)
	cfgdb.Edges = make(map[cfg.Edge]struct{})
	return &cfgdb
}

func (cfgdb *CFGDBTest) Transaction() db.TransactionManager {
	return cfgdb.Ir.Transaction()
}

func (cfgdb *CFGDBTest) Common() db.Common {
	return cfgdb.Ir.Common()
}

func (cfgdb *CFGDBTest) GetIr() ir.IRDB {
	return cfgdb.Ir
}

func (cfgdb *CFGDBTest) IsPresentInDB(moduleFilename string) bool {
	return lo.ContainsBy(lo.Values(cfgdb.Ir.Modules), func(module ir.Module) bool {
		return module.Filename == moduleFilename
	})
}

func (cfgdb *CFGDBTest) GetControlFlowIndices() map[ir.OperationIndex]struct{} {
	result := map[ir.OperationIndex]struct{}{}
	for index, operation := range cfgdb.Ir.OperationIndices {
		if operation.IsBranch() {
			result[index] = struct{}{}
		}
	}
	return result
}

func (cfgdb *CFGDBTest) GetFallthroughIndices() map[ir.OperationIndex]struct{} {
	result := map[ir.OperationIndex]struct{}{}
	for index, operation := range cfgdb.Ir.OperationIndices {
		nextIndex := index + 1
		_, nextPresent := cfgdb.Ir.OperationIndices[nextIndex]
		if operation.IsBranch() && nextPresent {
			result[nextIndex] = struct{}{}
		}
	}
	return result
}

func (cfgdb *CFGDBTest) GetFirstIndex() ir.OperationIndex {
	return lo.Min(lo.Keys(cfgdb.Ir.OperationIndices))
}

func (cfgdb *CFGDBTest) GetLastIndex() ir.OperationIndex {
	return lo.Max(lo.Keys(cfgdb.Ir.OperationIndices))
}

func (cfgdb *CFGDBTest) GetOperationAtBlockIndex(block cfg.Block, opIndex uint64) ir.Operation {
	index := block.Start + ir.OperationIndex(opIndex)
	operation, err := cfgdb.Ir.GetOperationByIndex(index)
	if err != nil {
		panic(err)
	}
	return operation
}

func (cfgdb *CFGDBTest) SetBlocks(blocks []cfg.Block) {
	lo.ForEach(blocks, func(block cfg.Block, index int) {
		cfgdb.Blocks[block.Id] = block
	})
}

func (cfgdb *CFGDBTest) GetBlock(blockId id.UUID) cfg.Block {
	return cfgdb.Blocks[blockId]
}

func (cfgdb *CFGDBTest) GetBlockContainingIndex(index ir.OperationIndex) (cfg.Block, error) {
	block, found := lo.Find(lo.Values(cfgdb.Blocks), func(block cfg.Block) bool {
		return block.Start <= index && index <= block.End
	})
	if !found {
		return cfg.Block{}, errors.New("block not found")
	}
	return block, nil
}

func (cfgdb *CFGDBTest) VisitBlocks(visitor func(block cfg.Block)) {
	lo.ForEach(lo.Values(cfgdb.Blocks), func(block cfg.Block, index int) {
		visitor(block)
	})
}

func (cfgdb *CFGDBTest) CountBlocks() uint {
	return uint(len(cfgdb.Blocks))
}

func (cfgdb *CFGDBTest) SetEdges(edges []cfg.Edge) {
	lo.ForEach(edges, func(edge cfg.Edge, index int) {
		cfgdb.Edges[edge] = struct{}{}
	})
}

func (cfgdb *CFGDBTest) VisitEdges(visitor func(edge cfg.Edge)) {
	lo.ForEach(lo.Keys(cfgdb.Edges), func(edge cfg.Edge, index int) {
		visitor(edge)
	})
}

func (cfgdb *CFGDBTest) CountEdges() uint {
	return uint(len(cfgdb.Edges))
}

func (cfgdb *CFGDBTest) GetEdgesTo(blockId id.UUID) []cfg.Edge {
	start := cfgdb.GetBlock(blockId).Start
	end := cfgdb.GetBlock(blockId).End
	return lo.Filter(lo.Keys(cfgdb.Edges), func(edge cfg.Edge, index int) bool {
		return edge.Destination >= start && edge.Destination <= end
	})
}

func (cfgdb *CFGDBTest) GetPredecessors(blockId id.UUID) []cfg.Block {
	return lo.Map(cfgdb.GetEdgesTo(blockId), func(edge cfg.Edge, index int) cfg.Block {
		block, err := cfgdb.GetBlockContainingIndex(edge.Source)
		if err != nil {
			panic(err)
		}
		return block
	})
}

func (cfgdb *CFGDBTest) GetEdgesFrom(blockId id.UUID) []cfg.Edge {
	end := cfgdb.GetBlock(blockId).End
	return lo.Filter(lo.Keys(cfgdb.Edges), func(edge cfg.Edge, index int) bool {
		return edge.Source == end
	})
}

func (cfgdb *CFGDBTest) GetSuccessors(blockId id.UUID) []cfg.Block {
	return lo.Map(cfgdb.GetEdgesFrom(blockId), func(edge cfg.Edge, index int) cfg.Block {
		block, err := cfgdb.GetBlockContainingIndex(edge.Destination)
		if err != nil {
			panic(err)
		}
		return block
	})
}

func (cfgdb *CFGDBTest) GetConditionalSuccessors(blockId id.UUID) (cfg.ConditionalSuccessors, error) {
	block := cfgdb.GetBlock(blockId)
	successors := cfgdb.GetSuccessors(blockId)
	if len(successors) != 2 {
		return cfg.ConditionalSuccessors{}, errors.New(fmt.Sprintf("given block has %d successors, not 2", len(successors)))
	}
	var result cfg.ConditionalSuccessors
	if successors[0].Start == block.End+1 {
		result.Fallthrough = successors[0]
		result.ConditionalTrue = successors[1]
	} else if successors[1].Start == block.End+1 {
		result.Fallthrough = successors[1]
		result.ConditionalTrue = successors[0]
	} else {
		panic(successors)
	}
	return result, nil
}

func (cfgdb *CFGDBTest) GetAbsoluteEdges() []cfg.Edge {
	return lo.Filter(lo.Keys(cfgdb.Edges), func(edge cfg.Edge, index int) bool {
		return edge.Logic == cfg.UNCONDITIONAL
	})
}

func (cfgdb *CFGDBTest) GetConditionalEdges() []cfg.Edge {
	return lo.Filter(lo.Keys(cfgdb.Edges), func(edge cfg.Edge, index int) bool {
		return edge.Logic == cfg.CONDITIONAL
	})
}

func (cfgdb *CFGDBTest) VerifyAll() error {
	err := cfgdb.VerifyBasicBlockBoundaries()
	if err != nil {
		return err
	}
	return nil
}

func (cfgdb *CFGDBTest) VerifyBasicBlockBoundaries() error {
	panic("todo")
	return nil
}

type TestDomTreeDB struct {
	*TestCommon
	*TestTransactionManager
	Cfg                 *cfg.CFG
	semiDominators      map[id.UUID]id.UUID
	ImmediateDominators map[id.UUID]id.UUID
	DominanceFrontiers  map[id.UUID]mapset.Set[id.UUID]
}

func NewTestDomTreeDB(common *TestCommon, tm *TestTransactionManager, cfgraph *cfg.CFG) *TestDomTreeDB {
	var dtdb TestDomTreeDB
	dtdb.TestCommon = common
	dtdb.TestTransactionManager = tm
	dtdb.ImmediateDominators = make(map[id.UUID]id.UUID)
	dtdb.semiDominators = make(map[id.UUID]id.UUID)
	dtdb.DominanceFrontiers = make(map[id.UUID]mapset.Set[id.UUID])
	dtdb.Cfg = cfgraph
	return &dtdb
}

func (dtdb *TestDomTreeDB) GetCfg() *cfg.CFG {
	return dtdb.Cfg
}

func (dtdb *TestDomTreeDB) SetSemiDominator(block id.UUID, semiDominator id.UUID) {
	if semiDominator == "" {
		panic(fmt.Sprintf("cannot set empty semidominator for block %s", block))
	}
	dtdb.semiDominators[block] = semiDominator
}

func (dtdb *TestDomTreeDB) GetSemiDominator(block id.UUID) id.UUID {
	sdom, found := dtdb.semiDominators[block]
	if !found {
		panic(fmt.Sprintf("cannot get semidominator for block %s", block))
	}
	return sdom
}

func (dtdb *TestDomTreeDB) SetImmediateDominator(block id.UUID, immediateDominator id.UUID) error {
	if immediateDominator == "" {
		panic(fmt.Sprintf("cannot set empty immediatedominator for block %s", block))
	}
	dtdb.ImmediateDominators[block] = immediateDominator
	return nil
}

func (dtdb *TestDomTreeDB) GetImmediateDominator(block id.UUID) (id.UUID, error) {
	idom, found := dtdb.ImmediateDominators[block]
	if !found {
		panic(fmt.Sprintf("cannot get immediate dominator for block %s", block))
	}
	return idom, nil
}

func (dtdb *TestDomTreeDB) GetDominated(block id.UUID) ([]id.UUID, error) {
	dominatedBlocks := make([]id.UUID, 0)
	for dominated, idom := range dtdb.ImmediateDominators {
		if idom != block {
			continue
		}
		dominatedBlocks = append(dominatedBlocks, dominated)
	}
	return dominatedBlocks, nil
}

func (dtdb *TestDomTreeDB) SetDominanceFrontier(block id.UUID, dominanceFrontier mapset.Set[id.UUID]) error {
	dtdb.DominanceFrontiers[block] = dominanceFrontier
	return nil
}

func (dtdb *TestDomTreeDB) GetDominanceFrontier(block id.UUID) (mapset.Set[id.UUID], error) {
	frontier, found := dtdb.DominanceFrontiers[block]
	if !found {
		return mapset.New[id.UUID](), errors.New("given block has no dominance frontier, are you sure it has at least 2 incoming edges?")
	}
	return frontier, nil
}

type TestSSADB struct {
	Ir      ir.IRDB
	Cfg     *cfg.CFG
	Dt      *domtree.DomTree
	Entries map[id.UUID][]ssa.Entry
}

func NewTestSSADB(irdb ir.IRDB, cfgraph *cfg.CFG, dt *domtree.DomTree) *TestSSADB {
	var ssadb TestSSADB
	ssadb.Ir = irdb
	ssadb.Cfg = cfgraph
	ssadb.Dt = dt
	ssadb.Entries = make(map[id.UUID][]ssa.Entry)
	return &ssadb
}

func (ssadb *TestSSADB) TestStrings(blockId id.UUID) []string {
	testString := func(entry ssa.Entry, _ int) string {
		return entry.String()
	}
	return lo.Map(ssadb.Entries[blockId], testString)
}

func (ssadb *TestSSADB) GetIr() ir.IRDB {
	return ssadb.Ir
}

func (ssadb *TestSSADB) GetCfg() *cfg.CFG {
	return ssadb.Cfg
}

func (ssadb *TestSSADB) GetDomTree() *domtree.DomTree {
	return ssadb.Dt
}

func (ssadb *TestSSADB) SetBlockEntries(entries []ssa.Entry) error {
	if len(entries) == 0 {
		return errors.New("no entries given")
	}
	block := entries[0].Block
	for _, entry := range entries {
		if entry.Block != block {
			return errors.New(fmt.Sprintf("inconsistent block specified on entry %s.%d", entry.Defined.Name, entry.Defined.Version))
		}
	}

	ssadb.Entries[block] = entries
	return nil
}

func (ssadb *TestSSADB) GetBlockEntries(block id.UUID) ([]ssa.Entry, error) {
	entries, found := ssadb.Entries[block]
	if !found {
		return nil, errors.New(fmt.Sprintf("block %s not in testdb", block))
	}
	return entries, nil
}

func (ssadb *TestSSADB) GetDefinition(defined ssa.Variable) (ssa.Entry, error) {
	for _, entries := range ssadb.Entries {
		for _, entry := range entries {
			if entry.Defined == defined {
				return entry, nil
			}
		}
	}
	return ssa.Entry{}, errors.New(fmt.Sprintf("unknown definition %s.%d", defined.Name, defined.Version))
}

func (ssadb *TestSSADB) GetUses(defined ssa.Variable) ([]ssa.Entry, error) {
	uses := make([]ssa.Entry, 0)
	for _, entries := range ssadb.Entries {
		for _, entry := range entries {
			if lo.Contains(entry.Inputs, defined) {
				uses = append(uses, entry)
			}
		}
	}
	return uses, nil
}

type TestASTDB struct {
	Ir        ir.IRDB
	Cfgraph   *cfg.CFG
	Ssa       *ssa.SSA
	Sequences map[string][]ast.Statement
}

func (tast *TestASTDB) GetIr() ir.IRDB {
	return tast.Ir
}

func (tast *TestASTDB) GetCfg() *cfg.CFG {
	return tast.Cfgraph
}

func (tast *TestASTDB) GetSsa() *ssa.SSA {
	return tast.Ssa
}

func (tast *TestASTDB) SetSequence(name string, statements []ast.Statement) error {
	if _, exists := tast.Sequences[name]; exists {
		return errors.New(fmt.Sprintf("duplicate sequence %s", name))
	}
	tast.Sequences[name] = statements
	return nil
}

func (tast *TestASTDB) GetSequence(name string) ([]ast.Statement, error) {
	if _, exists := tast.Sequences[name]; !exists {
		return nil, errors.New(fmt.Sprintf("undefined sequence %s", name))
	}
	return tast.Sequences[name], nil
}

func NewTestASTDB(irdb ir.IRDB, cfgraph *cfg.CFG, s *ssa.SSA) TestASTDB {
	return TestASTDB{
		Ir:        irdb,
		Cfgraph:   cfgraph,
		Ssa:       s,
		Sequences: make(map[string][]ast.Statement),
	}
}

func MakeGraph(tid id.Generator, orderedBlocks []string, condEdges map[string]string, uncondEdges map[string]string, insnSize uint) ([]ir.Instruction, []ir.Operation) {
	blockPositions := make(map[string]uint64)
	for pos, blockName := range orderedBlocks {
		blockPositions[blockName] = uint64(pos)
	}

	var instructions []ir.Instruction
	var operations []ir.Operation

	calcOffset := func(blockName string) uint64 {
		return blockPositions[blockName] * uint64(insnSize)
	}

	for _, blockName := range orderedBlocks {
		var instruction ir.Instruction
		var operation ir.Operation
		operation.Id = tid.MakeUUID()
		if condDst, hasEdge := condEdges[blockName]; hasEdge {
			instruction = insn(tid.MakeUUID(), fmt.Sprintf("jt %s i%s", condDst, condDst), calcOffset(blockName), insnSize, []id.UUID{operation.Id})
			operation = jt(operation.Id, branch(tid.MakeUUID(), insnSize*8, ir.DIRECT, mo.Some(ir.Address{Offset: calcOffset(condDst), Space: ir.MemoryAddressSpace}), mo.None[ir.Location]()), imm(tid.MakeUUID(), insnSize*8, mo.None[[]byte]()))
		} else if absDst, hasEdge := uncondEdges[blockName]; hasEdge {
			instruction = insn(tid.MakeUUID(), fmt.Sprintf("jmp %s", absDst), calcOffset(blockName), insnSize, []id.UUID{operation.Id})
			operation = jmp(operation.Id, branch(tid.MakeUUID(), insnSize*8, ir.DIRECT, mo.Some(ir.Address{Offset: calcOffset(absDst), Space: ir.MemoryAddressSpace}), mo.None[ir.Location]()))
		} else {
			// jmp to self if we don't have anything else to do
			fmt.Printf("[w] warning: generating infinite self-jmp loop for %s\n", blockName)
			instruction = insn(tid.MakeUUID(), fmt.Sprintf("jmp %s", blockName), calcOffset(blockName), insnSize, []id.UUID{operation.Id})
			operation = jmp(operation.Id, branch(tid.MakeUUID(), insnSize*8, ir.DIRECT, mo.Some(ir.Address{Offset: calcOffset(blockName), Space: ir.MemoryAddressSpace}), mo.None[ir.Location]()))
		}
		instructions = append(instructions, instruction)
		operations = append(operations, operation)
	}

	return instructions, operations
}

func MakeTextSection(sectionId id.UUID, instructions []id.UUID) ir.Section {
	return ir.Section{
		Id:           sectionId,
		Name:         ".text",
		Address:      ir.Address{Offset: 0, Space: ir.MemoryAddressSpace},
		Size:         1000,
		Permissions:  ir.READ_EXECUTE,
		Data:         make([]byte, 0),
		Instructions: instructions,
	}
}

func MakeTestModule(moduleId id.UUID, sections []id.UUID) ir.Module {
	return ir.Module{
		Id:         moduleId,
		Filename:   "test.bin",
		Endianness: ir.LITTLE,
		Sections:   sections,
	}
}

func MakeOneInsnIR(tid id.Generator, db ir.IRDB) ir.IRLoader {
	ra := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 8, false)
	rb := reg(tid.MakeUUID(), tid.MakeUUID(), 1, 8, false)

	sectionId := tid.MakeUUID()
	insnId := tid.MakeUUID()
	opId := tid.MakeUUID()

	return ir.NewIRLoader(
		[]ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId})},
		[]ir.Section{MakeTextSection(sectionId, []id.UUID{insnId})},
		[]ir.Instruction{insn(insnId, "mov a, b", 0, 4, []id.UUID{opId})},
		[]ir.Operation{mov(opId, ra, rb)},
		nil,
		db,
	)
}

func MakeOneJmpIR(tid id.Generator, db ir.IRDB) ir.IRLoader {
	dst := branch(tid.MakeUUID(), 64, ir.DIRECT, mo.Some(ir.Address{Offset: 0x0, Space: ir.MemoryAddressSpace}), mo.None[ir.Location]())

	sectionId := tid.MakeUUID()
	insnId := tid.MakeUUID()
	opId := tid.MakeUUID()

	return ir.NewIRLoader(
		[]ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId})},
		[]ir.Section{MakeTextSection(sectionId, []id.UUID{insnId})},
		[]ir.Instruction{insn(insnId, "jmp a", 0, 4, []id.UUID{opId})},
		[]ir.Operation{jmp(opId, dst)},
		nil,
		db,
	)
}

func MakeOneBasicIR(tid id.Generator, db ir.IRDB) ir.IRLoader {
	ra := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 8, false)
	rb := reg(tid.MakeUUID(), tid.MakeUUID(), 1, 8, false)
	dst := branch(tid.MakeUUID(), 8, ir.DIRECT, mo.Some(ir.Address{Offset: 0, Space: ir.MemoryAddressSpace}), mo.None[ir.Location]())

	sectionId := tid.MakeUUID()

	movId := tid.MakeUUID()
	jmpId := tid.MakeUUID()
	movOpId := tid.MakeUUID()
	jmpOpId := tid.MakeUUID()

	return ir.NewIRLoader(
		[]ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId})},
		[]ir.Section{MakeTextSection(sectionId, []id.UUID{movId, jmpId})},
		[]ir.Instruction{
			insn(movId, "mov a, b", 0, 4, []id.UUID{movOpId}),
			insn(jmpId, "jmp c", 4, 4, []id.UUID{jmpOpId}),
		},
		[]ir.Operation{mov(movOpId, ra, rb), jmp(jmpOpId, dst)},
		nil,
		db,
	)
}

//func MakeTwoJmpIR(tid id.Generator, db ir.IRDB) ir.IRLoader {
//	ra := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 8)
//	rb := reg(tid.MakeUUID(), tid.MakeUUID(), 1, 8)
//
//	sectionId := tid.MakeUUID()
//
//	firstJmpId := tid.MakeUUID()
//	secondJmpId := tid.MakeUUID()
//
//	return ir.IRLoader{
//		Db:       db,
//		Modules:  []ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId})},
//		Sections: []ir.Section{MakeTextSection(sectionId, []id.UUID{firstJmpId, secondJmpId})},
//		Instructions: []ir.Instruction{
//			insn(firstJmpId, "jmp a", 0, 4, []ir.Operation{jmp(tid.MakeUUID(), ra)}),
//			insn(secondJmpId, "jmp b", 4, 4, []ir.Operation{jmp(tid.MakeUUID(), rb)}),
//		},
//	}
//}
//
//func MakeThreeJmpIR(tid id.Generator, db ir.IRDB) ir.IRLoader {
//	ra := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 8)
//	rb := reg(tid.MakeUUID(), tid.MakeUUID(), 1, 8)
//	rc := reg(tid.MakeUUID(), tid.MakeUUID(), 2, 8)
//
//	sectionId := tid.MakeUUID()
//
//	firstJmpId := tid.MakeUUID()
//	secondJmpId := tid.MakeUUID()
//	thirdJmpId := tid.MakeUUID()
//
//	return ir.IRLoader{
//		Db:       db,
//		Modules:  []ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId})},
//		Sections: []ir.Section{MakeTextSection(sectionId, []id.UUID{firstJmpId, secondJmpId, thirdJmpId})},
//		Instructions: []ir.Instruction{
//			insn(firstJmpId, "jmp a", 0, 4, []ir.Operation{jmp(tid.MakeUUID(), ra)}),
//			insn(secondJmpId, "jmp b", 4, 4, []ir.Operation{jmp(tid.MakeUUID(), rb)}),
//			insn(thirdJmpId, "jmp c", 8, 4, []ir.Operation{jmp(tid.MakeUUID(), rc)}),
//		},
//	}
//}

// https://tanujkhattar.wordpress.com/2016/01/11/dominator-tree-of-a-directed-graph/
func MakeComplex(tid id.Generator, db ir.IRDB) ir.IRLoader {
	// Had to adjust this to not have nodes
	// with 3 outgoing edges

	// Control flow graph (sorted)
	// R -> B, C
	// A -> D
	// B -> A, E
	// C -> F, G
	// D -> L
	// E -> H
	// F -> I
	// G -> I, J
	// H -> K
	// I -> K
	// J -> I
	// K -> R, I
	// L -> H

	// Control flow graph (linear)
	// 0 K -> I, fR
	// 1 R -> C, fB
	// 2 B -> E, fA
	// 3 A -> D
	// 4 C -> F, fG
	// 5 G -> I, fJ
	// 6 J -> I
	// 7 I -> K
	// 8 D -> L
	// 9 E -> H
	// 10 F -> I
	// 11 H -> K
	// 12 L -> H

	// Dominator tree (all connections are idoms)
	// K -> R, I
	// R -> B, C
	// B -> A, E, H
	// A -> D
	// D -> L
	// C -> G, F
	// G -> J

	// Dominance Frontiers
	// K -> [] w/ err
	// I -> [F, C, R, G, J]
	// H -> [E, A, D, L]
	// everything else empty

	textSectionId := tid.MakeUUID()

	blockPositions := []string{
		"K",
		"R",
		"B",
		"A",
		"C",
		"G",
		"J",
		"I",
		"D",
		"E",
		"F",
		"H",
		"L",
	}

	condEdges := map[string]string{
		"K": "I",
		"R": "C",
		"B": "E",
		"C": "F",
		"G": "I",
	}
	uncondEdges := map[string]string{
		"A": "D",
		"J": "I",
		"I": "K",
		"D": "L",
		"E": "H",
		"F": "I",
		"H": "K",
		"L": "H",
	}

	instructions, operations := MakeGraph(tid, blockPositions, condEdges, uncondEdges, 4)

	instructionIds := lo.Map(instructions, func(insn ir.Instruction, _ int) id.UUID {
		return insn.Id
	})

	return ir.NewIRLoader(
		[]ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{textSectionId})},
		[]ir.Section{MakeTextSection(textSectionId, instructionIds)},
		instructions,
		operations,
		nil,
		db,
	)
}

func MakeOneBlock(tid id.Generator, db ir.IRDB) ir.IRLoader {
	ra := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 8, false)
	rb := reg(tid.MakeUUID(), tid.MakeUUID(), 1, 8, false)
	ret := reg(tid.MakeUUID(), tid.MakeUUID(), 10, 8, false)
	retAddr := branch(tid.MakeUUID(), 8, ir.INDIRECT, mo.None[ir.Address](), mo.Some(ret.GetContainerLocation()))

	operations := []ir.Operation{
		mov(tid.MakeUUID(), ra, imm(tid.MakeUUID(), 8, mo.Some(util.ToBytesBE(4)))),
		mov(tid.MakeUUID(), rb, imm(tid.MakeUUID(), 8, mo.Some(util.ToBytesBE(2)))),
		generic(tid.MakeUUID(), ir.ADD, ra, rb, ra),
		jmp(tid.MakeUUID(), retAddr),
	}

	instructions := []ir.Instruction{
		insn(tid.MakeUUID(), "mov ra, 4", 0, 2, []id.UUID{operations[0].Id}),
		insn(tid.MakeUUID(), "mov rb, 2", 2, 2, []id.UUID{operations[1].Id}),
		insn(tid.MakeUUID(), "add rc, ra, rb", 4, 2, []id.UUID{operations[2].Id}),
		insn(tid.MakeUUID(), "jmp [ret]", 6, 2, []id.UUID{operations[3].Id}),
	}

	instructionIds := lo.Map(instructions, func(insn ir.Instruction, _ int) id.UUID { return insn.Id })

	textSectionId := tid.MakeUUID()

	return ir.NewIRLoader(
		[]ir.Module{MakeTestModule(tid.MakeUUID(), []id.UUID{textSectionId})},
		[]ir.Section{MakeTextSection(textSectionId, instructionIds)},
		instructions,
		operations,
		reg(tid.MakeUUID(), tid.MakeUUID(), 100, 8, true),
		db,
	)
}

func MakeIterFib(tid id.Generator, db ir.IRDB) ir.IRLoader {
	//iter_fib:
	//	mov     edx, 2
	//	xor     eax, eax
	//	mov     ecx, 1
	//	xor     esi, esi
	//.L9:
	//	cmp     edi, edx
	//	jb      .L11
	//	lea     eax, [rsi+rcx]
	//	inc     edx
	//	mov     esi, ecx
	//	mov     ecx, eax
	//	jmp     .L9
	//.L11:
	//	ret

	eax := reg(tid.MakeUUID(), tid.MakeUUID(), 0, 32, false)
	ecx := reg(tid.MakeUUID(), tid.MakeUUID(), 8, 32, false)
	rcx := reg(tid.MakeUUID(), tid.MakeUUID(), 8, 64, false)
	edx := reg(tid.MakeUUID(), tid.MakeUUID(), 16, 32, false)
	esi := reg(tid.MakeUUID(), tid.MakeUUID(), 24, 32, false)
	rsi := reg(tid.MakeUUID(), tid.MakeUUID(), 24, 64, false)
	edi := reg(tid.MakeUUID(), tid.MakeUUID(), 32, 32, false)
	rsp := reg(tid.MakeUUID(), tid.MakeUUID(), 40, 64, false)
	rip := reg(tid.MakeUUID(), tid.MakeUUID(), 48, 64, true)

	cf := reg(tid.MakeUUID(), tid.MakeUUID(), 1000, 1, false)

	t1 := tmp(tid, 0x1000, 64)
	t2 := tmp(tid, 0x2000, 64)

	operations := []ir.Operation{
		// 0010113a
		mov(tid.MakeUUID(), edx, imm(tid.MakeUUID(), 32, mo.Some(util.ToBytesLE(2)))),
		generic(tid.MakeUUID(), ir.XOR, eax, eax, eax),
		mov(tid.MakeUUID(), ecx, imm(tid.MakeUUID(), 32, mo.Some(util.ToBytesLE(1)))),
		generic(tid.MakeUUID(), ir.XOR, esi, esi, esi),

		// LAB_00101148
		generic(tid.MakeUUID(), ir.LT, edi, edx, cf),
		jt(tid.MakeUUID(), branch(tid.MakeUUID(), 64, ir.DIRECT, mo.Some(ir.Address{Offset: 0x101157, Space: ir.MemoryAddressSpace}), mo.None[ir.Location]()), cf),
		// lea eax, [rsi + rcx*0x1]
		generic(tid.MakeUUID(), ir.MUL, rcx, imm(tid.MakeUUID(), 64, mo.Some(util.ToBytesLE(1))), t1),
		generic(tid.MakeUUID(), ir.ADD, rsi, t1, t2),
		generic(tid.MakeUUID(), ir.TRUNC, t2, imm(tid.MakeUUID(), 64, mo.Some(util.ToBytesLE(32))), eax),

		generic(tid.MakeUUID(), ir.ADD, edx, imm(tid.MakeUUID(), 32, mo.Some(util.ToBytesLE(1))), edx),
		mov(tid.MakeUUID(), esi, ecx),
		mov(tid.MakeUUID(), ecx, eax),
		jmp(tid.MakeUUID(), branch(tid.MakeUUID(), 64, ir.DIRECT, mo.Some(ir.Address{Offset: 0x101148, Space: ir.MemoryAddressSpace}), mo.None[ir.Location]())),

		// LAB_00101157
		// RET
		load(tid, rsp, rip),
		generic(tid.MakeUUID(), ir.ADD, rsp, imm(tid.MakeUUID(), 64, mo.Some(util.ToBytesLE(8))), rsp),
		jmp(tid.MakeUUID(), branch(tid.MakeUUID(), 64, ir.INDIRECT, mo.None[ir.Address](), mo.Some(rip.GetContainerLocation()))),
	}

	operations = lo.Map(operations, func(op ir.Operation, index int) ir.Operation {
		op.Index = ir.OperationIndex(index)
		return op
	})

	instructions := []ir.Instruction{
		insn(tid.MakeUUID(), "mov edx, 2", 0x10113a, 5, []id.UUID{operations[0].Id}),
		insn(tid.MakeUUID(), "xor eax, eax", 0x10113f, 2, []id.UUID{operations[1].Id}),
		insn(tid.MakeUUID(), "mov ecx, 1", 0x101141, 5, []id.UUID{operations[2].Id}),
		insn(tid.MakeUUID(), "xor esi, esi", 0x101146, 2, []id.UUID{operations[3].Id}),
		insn(tid.MakeUUID(), "cmp edi, edx", 0x101148, 2, []id.UUID{operations[4].Id}),
		insn(tid.MakeUUID(), "jc LAB_00101157", 0x10114a, 2, []id.UUID{operations[5].Id}),
		insn(tid.MakeUUID(), "lea eax, [rsi + rcx*0x1]", 0x10114c, 3, []id.UUID{
			operations[6].Id,
			operations[7].Id,
			operations[8].Id,
		}),
		insn(tid.MakeUUID(), "inc edx", 0x10114f, 2, []id.UUID{operations[9].Id}),
		insn(tid.MakeUUID(), "mov esi, ecx", 0x101151, 2, []id.UUID{operations[10].Id}),
		insn(tid.MakeUUID(), "mov ecx, eax", 0x101153, 2, []id.UUID{operations[11].Id}),
		insn(tid.MakeUUID(), "jmp LAB_00101148", 0x101155, 2, []id.UUID{operations[12].Id}),
		insn(tid.MakeUUID(), "ret", 0x101157, 1, []id.UUID{
			operations[13].Id,
			operations[14].Id,
			operations[15].Id,
		}),
	}

	sectionId := tid.MakeUUID()

	return ir.NewIRLoader(
		[]ir.Module{
			MakeTestModule(tid.MakeUUID(), []id.UUID{sectionId}),
		},
		[]ir.Section{
			MakeTextSection(sectionId, lo.Map(instructions, func(instruction ir.Instruction, _ int) id.UUID {
				return instruction.Id
			})),
		},
		instructions,
		operations,
		rip,
		db,
	)
}

func WipeAndLoad(irdb ir.IRDB, tid id.Generator, loader func(tid id.Generator, db ir.IRDB) ir.IRLoader) error {
	err := irdb.Common().Wipe()
	if err != nil {
		return err
	}

	irep := loader(tid, irdb)

	return irep.Load()
}

type Neo4jContainerInfo struct {
	Deferences []func()
	Container  *neo4j.Neo4jContainer
	N4DB       *db.Neo4jDB
}

func Neo4jContainer(t *testing.T, user string, password string) Neo4jContainerInfo {
	ctx := context.Background()

	container, err := neo4j.RunContainer(
		ctx,
		testcontainers.WithImage("docker.io/neo4j:latest"),
		neo4j.WithLogger(testcontainers.TestLogger(t)),
		neo4j.WithAdminPassword(password),
		neo4j.WithLabsPlugin(neo4j.Apoc),
		neo4j.WithNeo4jSetting("dbms.tx_log.rotation.size", "42M"),
		testcontainers.WithWaitStrategy(wait.ForListeningPort("7687")),
		testcontainers.WithWaitStrategy(wait.ForLog("Started.")),
	)
	if err != nil {
		panic(err.Error())
	}

	containerTerminator := func() {
		println("terminating container")
		if err := container.Terminate(ctx); err != nil {
			panic(err.Error())
		}
	}

	containerIP, err := container.ContainerIP(ctx)
	if err != nil {
		log.Fatalf("failed to get container ip: %s", err.Error())
	}

	neo4jDB := db.Neo4jDB{DbName: "neo4j", Ctx: &ctx, Transactions: make(map[db.Transaction]db.Neo4jTransactionInfo)}
	err = neo4jDB.Connect(fmt.Sprintf("neo4j://%s", containerIP), &user, &password)
	if err != nil {
		panic(err.Error())
	}
	containerDisconnector := func() {
		println("disconnecting from DB")
		err := neo4jDB.Disconnect()
		if err != nil {
			panic(err.Error())
		}
	}

	neo4jDB.EnsureIsConnected()

	return Neo4jContainerInfo{Deferences: []func(){containerTerminator, containerDisconnector}, Container: container, N4DB: &neo4jDB}
}
