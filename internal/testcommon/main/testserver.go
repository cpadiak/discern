package main

import (
	"context"
	"discern/internal/db"
	"discern/internal/id"
	"discern/internal/server"
	"discern/internal/testcommon"
	"discern/ir"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func main() {
	configPath := flag.String("config", "", "filepath to configuration file")

	flag.Parse()

	configData, err := os.ReadFile(*configPath)
	if err != nil {
		log.Fatal(fmt.Sprintf("invalid config file path: %s", *configPath))
	}

	type Config struct {
		DbHost     string `toml:"db_host"`
		DbUser     string `toml:"db_user"`
		DbPassword string `toml:"db_password"`
	}

	var config Config
	_, err = toml.Decode(string(configData), &config)
	if err != nil {
		log.Fatal("invalid toml config")
	}

	fmt.Println("[i] config read")

	ctx := context.Background()

	n4j := db.Neo4jDB{DbName: "neo4j", Ctx: &ctx, Transactions: make(map[db.Transaction]db.Neo4jTransactionInfo)}
	err = n4j.Connect(config.DbHost, &config.DbUser, &config.DbPassword)
	if err != nil {
		panic(err.Error())
	}
	defer func(n4j db.Neo4jDB) {
		err := n4j.Disconnect()
		if err != nil {
			panic(err.Error())
		}
	}(n4j)

	server := server.Server{Ctx: &ctx, Db: &server.Neo4j{Db: &n4j}}

	type TestRequest struct {
		IRLoader string `json:"ir_loader"`
	}

	testLoad := func(c *gin.Context) {
		var req TestRequest

		err := c.BindJSON(&req)
		if err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}

		tid := id.TestID{}
		irdb := ir.IRNeo4j{Neo4jDB: &n4j}

		loaders := map[string]func(tid id.Generator, db ir.IRDB) ir.IRLoader{
			"one_jmp_insn": testcommon.MakeOneJmpIR,
		}

		getLoader, exists := loaders[req.IRLoader]
		if !exists {
			c.JSON(400, gin.H{"error": "invalid IR loader " + req.IRLoader})
			return
		}

		err = irdb.Wipe()
		if err != nil {
			c.JSON(500, gin.H{"error": err.Error()})
			return
		}

		loader := getLoader(&tid, &irdb)
		err = loader.Load()
		if err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}

		c.JSON(200, gin.H{"message": "success"})
	}

	router := gin.Default()
	err = router.SetTrustedProxies(make([]string, 0))
	if err != nil {
		fmt.Println("[!] could not set trusted proxies")
		return
	}
	router.PUT("/import/ghidra", server.ImportGhidra)
	router.POST("/analyze", server.Analyze)
	router.POST("/test/load", testLoad)

	fmt.Println("[i] server created")

	fmt.Println("[i] server running")

	router.Run("localhost:4444")

	fmt.Println("Done!")
}
