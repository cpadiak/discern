package cfg_test

import (
	"discern/cfg"
	"discern/internal/id"
	"discern/internal/testcommon"
	"discern/internal/util"
	"discern/ir"
	"github.com/go-playground/assert/v2"
	"github.com/samber/mo"
	"testing"
)

func MakeCFG(generator id.Generator, irmaker func(generator id.Generator, irdb ir.IRDB) ir.IRLoader) (cfg.CFG, error) {
	irdb := testcommon.NewIRDBTest(&testcommon.TestTransactionManager{}, &testcommon.TestCommon{})
	ir := irmaker(generator, irdb)
	err := ir.Load()
	if err != nil {
		return cfg.CFG{}, err
	}
	cfgdb := testcommon.NewCFGDBTest(irdb)
	return cfg.CFG{
		CFGDB:          cfgdb,
		ModuleFilename: "one_insn",
		RootBlockId:    mo.None[id.UUID](),
	}, nil
}

func TestCFGBuildOneInsn(t *testing.T) {
	tid := id.TestID{}

	cfg, err := MakeCFG(&tid, testcommon.MakeOneInsnIR)
	if err != nil {
		panic(err)
	}

	err = cfg.Build()
	if err != nil {
		panic(err)
	}
}

func TestCFGBuildOneBlock(t *testing.T) {
	tid := id.TestID{}

	cfgraph, err := MakeCFG(&tid, testcommon.MakeOneBlock)
	if err != nil {
		panic(err)
	}

	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	assert.Equal(t, cfgraph.CountBlocks(), uint(1))
	assert.Equal(t, cfgraph.CountEdges(), uint(0))

	block, err := cfgraph.GetBlockContainingIndex(0)
	assert.Equal(t, err, nil)

	assert.Equal(t, len(block.Operations), 4)
}

func TestCFGBuildComplex(t *testing.T) {
	tid := id.TestID{}

	cfgraph, err := MakeCFG(&tid, testcommon.MakeComplex)
	if err != nil {
		panic(err)
	}

	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	assert.Equal(t, cfgraph.CountBlocks(), uint(13))
	assert.Equal(t, cfgraph.CountEdges(), uint(13+5))

	getBlock := func(index int) cfg.Block {
		b, err := cfgraph.GetBlockContainingIndex(ir.OperationIndex(index))
		if err != nil {
			panic(err)
		}
		return b
	}

	k := getBlock(0)
	r := getBlock(1)
	b := getBlock(2)
	a := getBlock(3)
	c := getBlock(4)
	g := getBlock(5)
	j := getBlock(6)
	i := getBlock(7)
	d := getBlock(8)
	e := getBlock(9)
	f := getBlock(10)
	h := getBlock(11)
	l := getBlock(12)

	assert.Equal(t, k.Start, k.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(k.Id)), map[cfg.Edge]struct{}{
		{Source: k.End, Destination: i.Start, BranchMode: ir.DIRECT, Logic: cfg.CONDITIONAL}:      {},
		{Source: k.End, Destination: r.Start, BranchMode: ir.FALLTHROUGH, Logic: cfg.CONDITIONAL}: {},
	})

	assert.Equal(t, r.Start, r.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(r.Id)), map[cfg.Edge]struct{}{
		{Source: r.End, Destination: c.Start, BranchMode: ir.DIRECT, Logic: cfg.CONDITIONAL}:      {},
		{Source: r.End, Destination: b.Start, BranchMode: ir.FALLTHROUGH, Logic: cfg.CONDITIONAL}: {},
	})

	assert.Equal(t, b.Start, b.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(b.Id)), map[cfg.Edge]struct{}{
		{Source: b.End, Destination: e.Start, BranchMode: ir.DIRECT, Logic: cfg.CONDITIONAL}:      {},
		{Source: b.End, Destination: a.Start, BranchMode: ir.FALLTHROUGH, Logic: cfg.CONDITIONAL}: {},
	})

	assert.Equal(t, a.Start, a.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(a.Id)), map[cfg.Edge]struct{}{
		{Source: a.End, Destination: d.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, c.Start, c.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(c.Id)), map[cfg.Edge]struct{}{
		{Source: c.End, Destination: f.Start, BranchMode: ir.DIRECT, Logic: cfg.CONDITIONAL}:      {},
		{Source: c.End, Destination: g.Start, BranchMode: ir.FALLTHROUGH, Logic: cfg.CONDITIONAL}: {},
	})

	assert.Equal(t, g.Start, g.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(g.Id)), map[cfg.Edge]struct{}{
		{Source: g.End, Destination: i.Start, BranchMode: ir.DIRECT, Logic: cfg.CONDITIONAL}:      {},
		{Source: g.End, Destination: j.Start, BranchMode: ir.FALLTHROUGH, Logic: cfg.CONDITIONAL}: {},
	})

	assert.Equal(t, j.Start, j.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(j.Id)), map[cfg.Edge]struct{}{
		{Source: j.End, Destination: i.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, i.Start, i.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(i.Id)), map[cfg.Edge]struct{}{
		{Source: i.End, Destination: k.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, d.Start, d.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(d.Id)), map[cfg.Edge]struct{}{
		{Source: d.End, Destination: l.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, e.Start, e.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(e.Id)), map[cfg.Edge]struct{}{
		{Source: e.End, Destination: h.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, f.Start, f.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(f.Id)), map[cfg.Edge]struct{}{
		{Source: f.End, Destination: i.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, h.Start, h.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(h.Id)), map[cfg.Edge]struct{}{
		{Source: h.End, Destination: k.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})

	assert.Equal(t, l.Start, l.End)
	assert.Equal(t, util.Slice2Map(cfgraph.GetEdgesFrom(l.Id)), map[cfg.Edge]struct{}{
		{Source: l.End, Destination: h.Start, BranchMode: ir.DIRECT, Logic: cfg.UNCONDITIONAL}: {},
	})
}

func TestCFGBuildIterFib(t *testing.T) {
	tid := id.TestID{}

	cfgraph, err := MakeCFG(&tid, testcommon.MakeIterFib)
	if err != nil {
		panic(err)
	}

	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	assert.Equal(t, cfgraph.CountBlocks(), uint(3))
	assert.Equal(t, cfgraph.CountEdges(), uint(3))

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0x10113a, Space: ir.MemoryAddressSpace})
	assert.Equal(t, err, nil)

	rootId := cfgraph.RootBlockId.MustGet()

	root := cfgraph.GetBlock(rootId)

	firstOp, err := cfgraph.GetIr().GetOperation(root.Operations[0])
	assert.Equal(t, err, nil)
	assert.Equal(t, firstOp.Index, ir.OperationIndex(0))
	assert.Equal(t, firstOp.Id, id.UUID("25"))
}

func TestCFGNeo4jBuild(t *testing.T) {
	containerInfo := testcommon.Neo4jContainer(t, "neo4j", "verylong")

	neo4jDB := containerInfo.N4DB

	tid := id.TestID{}

	var irdb ir.IRDB = &ir.IRNeo4j{Neo4jDB: neo4jDB}

	// ONE INSN IR
	err := testcommon.WipeAndLoad(irdb, &tid, testcommon.MakeOneInsnIR)
	if err != nil {
		panic(err.Error())
	}

	cfgraph := cfg.CFG{CFGDB: &cfg.CFGNeo4j{Neo4jDB: neo4jDB, Ir: irdb}, ModuleFilename: "test_one_insn", RootBlockId: mo.None[id.UUID]()}

	err = cfgraph.Build()
	assert.Equal(t, err, nil)

	result := cfgraph.VerifyAll()
	assert.Equal(t, result, nil)

	assert.Equal(t, cfgraph.CountBlocks(), uint(1))
	assert.Equal(t, cfgraph.CountEdges(), uint(0))

	// ONE JMP IR
	err = testcommon.WipeAndLoad(irdb, &tid, testcommon.MakeOneJmpIR)
	if err != nil {
		panic(err.Error())
	}

	err = cfgraph.Build()
	assert.Equal(t, err, nil)

	result = cfgraph.VerifyAll()
	assert.Equal(t, result, nil)

	assert.Equal(t, cfgraph.CountBlocks(), uint(1))
	assert.Equal(t, cfgraph.CountEdges(), uint(1))

	// ONE BASIC IR
	err = testcommon.WipeAndLoad(irdb, &tid, testcommon.MakeOneBasicIR)
	if err != nil {
		panic(err.Error())
	}

	err = cfgraph.Build()
	assert.Equal(t, err, nil)

	result = cfgraph.VerifyAll()
	assert.Equal(t, result, nil)

	assert.Equal(t, cfgraph.CountBlocks(), uint(1))
	assert.Equal(t, cfgraph.CountEdges(), uint(1))

}
