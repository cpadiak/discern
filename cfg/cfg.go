package cfg

import (
	"discern/internal/db"
	"discern/internal/id"
	"discern/ir"
	"errors"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"github.com/zyedidia/generic/set"
	"maps"
	"slices"
)

type ConditionalSuccessors struct {
	ConditionalTrue Block
	Fallthrough     Block
}

type CFGDB interface {
	Transaction() db.TransactionManager
	Common() db.Common
	GetIr() ir.IRDB

	IsPresentInDB(moduleFilename string) bool
	GetControlFlowIndices() map[ir.OperationIndex]struct{}
	GetFallthroughIndices() map[ir.OperationIndex]struct{}
	GetFirstIndex() ir.OperationIndex
	GetLastIndex() ir.OperationIndex
	GetOperationAtBlockIndex(block Block, opIndex uint64) ir.Operation

	SetBlocks(blocks []Block)
	GetBlock(blockId id.UUID) Block
	GetBlockContainingIndex(index ir.OperationIndex) (Block, error)
	VisitBlocks(visitor func(block Block))
	CountBlocks() uint
	SetEdges(edges []Edge)
	VisitEdges(visitor func(edge Edge))
	CountEdges() uint

	GetEdgesTo(blockId id.UUID) []Edge
	GetPredecessors(blockId id.UUID) []Block
	GetEdgesFrom(blockId id.UUID) []Edge
	GetSuccessors(blockId id.UUID) []Block
	GetConditionalSuccessors(blockId id.UUID) (ConditionalSuccessors, error)

	GetAbsoluteEdges() []Edge
	GetConditionalEdges() []Edge

	//FindNonFallthroughEdges() map[ir.OperationIndex]ir.OperationIndex

	VerifyAll() error
	VerifyBasicBlockBoundaries() error
}

type CFG struct {
	CFGDB
	ModuleFilename string
	RootBlockId    mo.Option[id.UUID]
}

func (cfg *CFG) SetEntrypoint(address ir.Address) error {
	rootBlock, err := cfg.GetBlockContaining(address)
	if err != nil {
		return err
	}
	cfg.RootBlockId = mo.Some(rootBlock.Id)
	return nil
}

func (cfg *CFG) DepthFirstTraversal(visitor func(block Block)) error {
	if cfg.RootBlockId.IsAbsent() {
		return errors.New("entrypoint must be set")
	}

	qu := []Block{cfg.GetBlock(cfg.RootBlockId.MustGet())}
	visited := set.NewMapset[id.UUID]()

	for len(qu) > 0 {
		currentBlock := qu[0]
		qu = qu[1:]

		if visited.Has(currentBlock.Id) {
			continue
		}
		visited.Put(currentBlock.Id)

		visitor(currentBlock)

		successors := cfg.GetSuccessors(currentBlock.Id)
		//fmt.Printf("%d is succeeded by: %s\n", currentBlock.Start, lo.Map(successors, func(b Block, _ int) string {
		//	return strconv.Itoa(int(b.Start))
		//}))
		qu = append(qu, successors...)
	}

	if uint(visited.Size()) != cfg.CountBlocks() {
		return errors.New(fmt.Sprintf("visited %d blocks but there are %d", visited.Size(), cfg.CountBlocks()))
	}

	return nil
}

func (cfg *CFG) ReverseDepthFirstTraversal(visitor func(block Block)) error {
	if cfg.RootBlockId.IsAbsent() {
		return errors.New("entrypoint must be set")
	}

	qu := []Block{cfg.GetBlock(cfg.RootBlockId.MustGet())}
	visited := set.NewMapset[id.UUID]()

	for len(qu) > 0 {
		currentBlock := qu[0]

		unvisitedSuccessors := lo.Filter(cfg.GetSuccessors(currentBlock.Id), func(block Block, _ int) bool {
			return !lo.Contains(lo.Map(qu, func(block Block, _ int) id.UUID {
				return block.Id
			}), block.Id) && !visited.Has(block.Id)
		})

		if len(unvisitedSuccessors) != 0 {
			qu = append(unvisitedSuccessors, qu...)
			continue
		}

		visitor(currentBlock)
		visited.Put(currentBlock.Id)
		qu = qu[1:]
	}

	return nil
}

func (cfg *CFG) GetBlockContaining(address ir.Address) (Block, error) {
	getOpIndex := func(operationId id.UUID) ir.OperationIndex {
		operation, err := cfg.GetIr().GetOperation(operationId)
		if err != nil {
			panic(err)
		}
		return operation.Index
	}

	instruction, err := cfg.GetIr().GetInstructionByExactAddress(address)
	if err == nil {
		return cfg.GetBlockContainingIndex(getOpIndex(instruction.Operations[0]))
	}
	instruction, err = cfg.GetIr().GetInstructionContaining(address)
	if err != nil {
		return Block{}, err
	}
	return cfg.GetBlockContainingIndex(getOpIndex(instruction.Operations[0]))
}

type Block struct {
	Id         id.UUID
	Start      ir.OperationIndex
	End        ir.OperationIndex
	Operations []id.UUID
}

func (block *Block) String() string {
	return fmt.Sprintf("{id: %s, start: %d, end: %d}", block.Id, block.Start, block.End)
}

type EdgeLogic string

const (
	UNCONDITIONAL EdgeLogic = "UNCONDITIONAL"
	CONDITIONAL   EdgeLogic = "CONDITIONAL"
)

type Edge struct {
	Source      ir.OperationIndex
	Destination ir.OperationIndex
	BranchMode  ir.BranchMode
	Logic       EdgeLogic
}

func (e *Edge) String() string {
	return fmt.Sprintf("%d -> %d %s %s", e.Source, e.Destination, e.BranchMode, e.Logic)
}

func (cfg *CFG) Build() error {
	cfg.Common().DeclareUUIDIndex("block_id", "Block", "id")
	cfg.Common().DeclareUUIDIndex("block_start", "Block", "start")
	cfg.Common().DeclareUUIDIndex("block_end", "Block", "end")

	// define basic blocks
	blockStarts := cfg.GetFallthroughIndices()
	blockStarts[cfg.GetFirstIndex()] = struct{}{}

	blockEnds := cfg.GetControlFlowIndices()
	blockEnds[cfg.GetLastIndex()] = struct{}{}

	fmt.Printf("[d] CFG: block starts: %d\n", len(blockStarts))
	fmt.Printf("[d] CFG: block ends: %d\n", len(blockEnds))

	if len(blockStarts) != len(blockEnds) {
		starts := lo.Keys(blockStarts)
		slices.Sort(starts)
		ends := lo.Keys(blockEnds)
		slices.Sort(ends)

		var lastEnd ir.OperationIndex = 0

		for _, p := range lo.Zip2(starts, ends) {
			start, end := p.Unpack()
			fmt.Printf("[d] start->end: %d %d\n", start, end)

			if start != lastEnd+1 && lastEnd != 0 {
				fmt.Printf("[w] non-contiguous block bounds, last end: %d current start: %d\n", lastEnd, start)
				break
			}

			if start > end {
				fmt.Printf("[w] illegal start->end: %d %d\n", start, end)
				break
			}

			lastEnd = end
		}

		return errors.New(fmt.Sprintf("expected same number of block starts and ends: %d %d", len(blockStarts), len(blockEnds)))
	}

	var blocks []Block

	for _, blockBounds := range lo.Zip2(slices.Sorted(maps.Keys(blockStarts)), slices.Sorted(maps.Keys(blockEnds))) {
		start, end := blockBounds.Unpack()
		operations, err := cfg.GetIr().GetOperationsInRange(start, end)
		if err != nil {
			panic(err)
		}
		block := Block{Id: id.MakeUUID(), Start: start, End: end, Operations: lo.Map(operations, func(operation ir.Operation, _ int) id.UUID {
			return operation.Id
		})}
		blocks = append(blocks, block)
	}

	fmt.Printf("[d] constructed %d blocks\n", len(blocks))

	cfg.SetBlocks(blocks)

	// define edges
	var edges []Edge

	for cfidx, _ := range cfg.GetControlFlowIndices() {
		cfop, err := cfg.GetIr().GetOperationByIndex(cfidx)
		if err != nil {
			panic(err.Error())
		}

		if len(cfop.Inputs) == 0 {
			panic(fmt.Sprintf("control flow op had no inputs?! %s", cfop.String()))
		}

		branchTarget := cfop.Inputs[0].GetBranch()
		if branchTarget.BranchMode == ir.INDIRECT {
			// TODO: Obviously this requires a much greater analysis than this routine
			// could ever handle
			continue
		}

		destination, err := cfg.GetIr().ConvertAddressToOperationIndex(branchTarget.AbsoluteBranchTarget.MustGet())
		if err != nil {
			// TODO: Sometimes a jmp will point off into the void and
			// that is a different kind of error than not being able
			// to find the corresponding insn because of a DB issue
			panic(err.Error())
		}

		fallthroughIndex := cfidx + 1
		if fallthroughIndex == destination {
			fmt.Printf("[w] warning: fallthrough was same as destination %d %d %d\n", cfidx, fallthroughIndex, destination)
		}

		logic := UNCONDITIONAL
		if cfop.Semantic != ir.JMP {
			logic = CONDITIONAL
			edges = append(edges, Edge{cfidx, fallthroughIndex, ir.FALLTHROUGH, CONDITIONAL})
		}

		edges = append(edges, Edge{cfidx, destination, branchTarget.BranchMode, logic})
	}

	//for source, destination := range cfg.GetAbsoluteEdges() {
	//	sourceIndex, _ := cfg.GetIr().ConvertAddressToOperationIndex(source)
	//	destinationIndex, _ := cfg.GetIr().ConvertAddressToOperationIndex(destination)
	//	edges = append(edges, Edge{sourceIndex, destinationIndex, ir.DIRECT, UNCONDITIONAL})
	//}
	//
	//for source, destinations := range cfg.GetConditionalEdges() {
	//	sourceIndex, _ := cfg.GetIr().ConvertAddressToOperationIndex(source)
	//	trueDestinationIndex, _ := cfg.GetIr().ConvertAddressToOperationIndex(destinations[true])
	//	falseDestinationIndex, _ := cfg.GetIr().ConvertAddressToOperationIndex(destinations[true])
	//	edges = append(edges, Edge{sourceIndex, trueDestinationIndex, ir.DIRECT, CONDITIONAL})
	//	edges = append(edges, Edge{sourceIndex, falseDestinationIndex, ir.DIRECT, CONDITIONAL})
	//}

	fmt.Printf("[d] constructed %d edges\n", len(edges))

	cfg.SetEdges(edges)

	return nil
}
