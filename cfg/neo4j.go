package cfg

import (
	"discern/internal/db"
	"discern/internal/id"
	"discern/ir"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/samber/lo"
)

type CFGNeo4j struct {
	*db.Neo4jDB
	Ir ir.IRDB
}

func (db *CFGNeo4j) Transaction() db.TransactionManager {
	db.EnsureIsConnected()
	return db
}

func (db *CFGNeo4j) Common() db.Common {
	db.EnsureIsConnected()
	return db
}

func (db *CFGNeo4j) GetIr() ir.IRDB {
	return db.Ir
}

func (db *CFGNeo4j) IsPresentInDB(moduleFilename string) bool {
	result, err := db.IsPresent("Cfg", "module_filename", moduleFilename)
	return err == nil && result
}

func (db *CFGNeo4j) GetControlFlowIndices() map[ir.OperationIndex]struct{} {
	result, err := db.Query("MATCH (op:Operation)-[r:HAS_OPERATION]-(insn:Instruction)\n"+
		"WHERE op.semantic IN ['JMP', 'JT', 'JF']\n"+
		"RETURN op.linear_index", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	return lo.SliceToMap(lo.Map(result.Records, func(record *neo4j.Record, _ int) ir.OperationIndex {
		value, _ := record.Get("op.linear_index")
		return ir.OperationIndex(value.(int64))
	}), func(index ir.OperationIndex) (ir.OperationIndex, struct{}) {
		return index, struct{}{}
	})
}

func (db *CFGNeo4j) GetFallthroughIndices() map[ir.OperationIndex]struct{} {
	result, err := db.Query("MATCH (op:Operation)\n"+
		"WHERE op.semantic IN ['JMP', 'JT', 'JF']\n"+
		"WITH op.linear_index AS cfi\n"+
		"MATCH (fop:Operation)-[:HAS_OPERATION]-(insn:Instruction)\n"+
		"WHERE fop.linear_index = cfi + 1\n"+
		"RETURN fop.linear_index", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	return lo.SliceToMap(lo.Map(result.Records, func(record *neo4j.Record, _ int) ir.OperationIndex {
		value, _ := record.Get("fop.linear_index")
		return ir.OperationIndex(value.(int64))
	}), func(index ir.OperationIndex) (ir.OperationIndex, struct{}) {
		return index, struct{}{}
	})
}

func (db *CFGNeo4j) GetFirstIndex() ir.OperationIndex {
	result, err := db.Query("MATCH (op:Operation)\n"+
		"RETURN MIN(op.linear_index) AS index", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	return ir.OperationIndex(result.Records[0].AsMap()["index"].(int64))
}

func (db *CFGNeo4j) GetLastIndex() ir.OperationIndex {
	result, err := db.Query("MATCH (op:Operation)\n"+
		"RETURN MAX(op.linear_index) AS index", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	if len(result.Records) != 1 {
		panic(errors.New("should only have gotten one record"))
	}
	return ir.OperationIndex(result.Records[0].AsMap()["index"].(int64))
}

func (db *CFGNeo4j) GetOperationAtBlockIndex(block Block, opIndex uint64) ir.Operation {
	absoluteIndexFromStart := block.Start.Add(opIndex)
	absoluteIndexFromEnd := block.End.Sub(opIndex)

	if absoluteIndexFromStart != absoluteIndexFromEnd {
		panic(fmt.Sprintf("from start %d, from end %d, looking for %d in %s", absoluteIndexFromStart, absoluteIndexFromEnd, opIndex, block.String()))
	}

	result, _ := db.Ir.GetOperationByIndex(absoluteIndexFromStart)
	return result
}

func (db *CFGNeo4j) SetBlocks(blocks []Block) {
	transaction := db.BeginTransaction()

	for _, block := range blocks {
		params := map[string]any{
			"id":    block.Id,
			"start": block.Start,
			"end":   block.End,
		}
		_, err := db.RunWithTransaction("CREATE (block:Block {id: $id, start: $start, end: $end})", params, transaction)
		if err != nil {
			panic(err.Error())
		}
	}

	err := db.CommitTransaction(transaction)
	if err != nil {
		panic(err.Error())
	}
	return
}

func (db *CFGNeo4j) blockDestructure(block string) string {
	return fmt.Sprintf("%s.id AS id, %s.start AS start, %s.end AS end", block, block, block)
}

func (db *CFGNeo4j) GetBlock(blockId id.UUID) Block {
	result, err := db.Query("MATCH (block:Block {id: $id})\n"+
		"RETURN "+db.blockDestructure("block"), map[string]any{"id": blockId})

	if err != nil {
		panic(err.Error())
	}

	var block Block
	err = mapstructure.Decode(result.Records[0], block)
	if err != nil {
		panic(err.Error())
	}

	return block
}

func (db *CFGNeo4j) GetBlockContainingIndex(index ir.OperationIndex) (Block, error) {
	panic("todo")
}

func (db *CFGNeo4j) VisitBlocks(visitor func(block Block)) {
	panic("todo")
}

func (db *CFGNeo4j) CountBlocks() uint {
	result, err := db.Query("MATCH (:Block) RETURN COUNT(*) AS count", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	return uint(result.Records[0].AsMap()["count"].(int64))
}

func (db *CFGNeo4j) SetEdges(edges []Edge) {
	transaction := db.BeginTransaction()

	for _, edge := range edges {
		params := map[string]any{
			"source":      edge.Source,
			"destination": edge.Destination,
			"branch_mode": edge.BranchMode,
			"logic":       edge.Logic,
		}
		_, err := db.RunWithTransaction("MATCH (pred:Block {end: $source}), (succ:Block)\n"+
			"WHERE succ.start <= $destination AND $destination <= succ.end\n"+
			"CREATE (pred)-[:EDGE {branch_mode: $branch_mode, logic: $logic}]->(succ)", params, transaction)
		if err != nil {
			panic(err.Error())
		}
	}

	err := db.CommitTransaction(transaction)
	if err != nil {
		panic(err.Error())
	}
	return
}

func (db *CFGNeo4j) VisitEdges(visitor func(edge Edge)) {
	panic("todo")
}

func (db *CFGNeo4j) CountEdges() uint {
	result, err := db.Query("MATCH (:Block)-[e:EDGE]-(:Block) RETURN COUNT(*) AS count", map[string]any{})
	if err != nil {
		panic(err.Error())
	}
	return uint(result.Records[0].AsMap()["count"].(int64))
}

func (db *CFGNeo4j) edgeDestructure(edge string, pred string, succ string) string {
	return fmt.Sprintf("%s.end AS source, %s.start AS destination, %s.branch_mode AS branch_mode, %s.logic AS logic", pred, succ, edge, edge)
}

func (db *CFGNeo4j) GetEdgesTo(blockId id.UUID) []Edge {
	result, err := db.Query("MATCH (pred:Block)-[e:EDGE]->(succ:Block {id: $bid})\n"+
		"RETURN "+db.edgeDestructure("e", "pred", "succ"), map[string]any{"bid": blockId})
	if err != nil {
		panic(err.Error())
	}
	var edges []Edge
	for _, edgeRecord := range result.Records {
		var edge Edge
		err = mapstructure.Decode(edgeRecord, &edge)
		if err != nil {
			panic(err.Error())
		}
		edges = append(edges, edge)
	}
	return edges
}

func (db *CFGNeo4j) GetPredecessors(blockId id.UUID) []Block {
	result, err := db.Query("MATCH (pred:Block {id: $bid})-[e:EDGE]->(succ:Block)\n"+
		"RETURN pred", map[string]any{"bid": blockId})
	if err != nil {
		panic(err.Error())
	}
	var blocks []Block
	for _, blockRecord := range result.Records {
		var block Block
		err = mapstructure.Decode(blockRecord, &block)
		if err != nil {
			panic(err.Error())
		}
		blocks = append(blocks, block)
	}
	return blocks
}

func (db *CFGNeo4j) GetEdgesFrom(blockId id.UUID) []Edge {
	result, err := db.Query("MATCH (pred:Block {id: $bid})-[e:EDGE]->(succ:Block)\n"+
		"RETURN "+db.edgeDestructure("e", "pred", "succ"), map[string]any{"bid": blockId})
	if err != nil {
		panic(err.Error())
	}
	var edges []Edge
	for _, edgeRecord := range result.Records {
		var edge Edge
		err = mapstructure.Decode(edgeRecord, &edge)
		if err != nil {
			panic(err.Error())
		}
		edges = append(edges, edge)
	}
	return edges
}

func (db *CFGNeo4j) GetSuccessors(blockId id.UUID) []Block {
	result, err := db.Query("MATCH (pred:Block {id: $bid})-[e:EDGE]->(succ:Block)\n"+
		"RETURN succ", map[string]any{"bid": blockId})
	if err != nil {
		panic(err.Error())
	}
	var blocks []Block
	for _, blockRecord := range result.Records {
		var block Block
		err = mapstructure.Decode(blockRecord, &block)
		if err != nil {
			panic(err.Error())
		}
		blocks = append(blocks, block)
	}
	return blocks
}

func (db *CFGNeo4j) GetConditionalSuccessors(blockId id.UUID) (ConditionalSuccessors, error) {
	result, err := db.Query("MATCH (fall:Block)<-[:EDGE]-(block:Block {id: $bid})-[:EDGE]->(cond:Block)\n"+
		"WHERE fall.end = block.start AND cond.id != fall.id\n"+
		"RETURN fall, cond", map[string]any{"bid": blockId})
	if err != nil {
		return ConditionalSuccessors{}, err
	}
	fmt.Println(result)
	panic("todo")
	return ConditionalSuccessors{}, nil
}

func (db *CFGNeo4j) GetAbsoluteEdges() []Edge {
	panic("todo")
	result, err := db.Query("MATCH (edge:Edge {logic: $logic}) RETURN edge", map[string]any{"logic": UNCONDITIONAL})
	if err != nil {
		panic(err.Error())
	}
	var edges []Edge
	err = mapstructure.Decode(result.Records[0], &edges)
	if err != nil {
		panic(err.Error())
	}
	return edges
}

func (db *CFGNeo4j) GetConditionalEdges() []Edge {
	panic("todo")
	result, err := db.Query("MATCH (edge:Edge {logic: $logic}) RETURN edge", map[string]any{"logic": CONDITIONAL})
	if err != nil {
		panic(err.Error())
	}
	var edges []Edge
	err = mapstructure.Decode(result.Records[0], &edges)
	if err != nil {
		panic(err.Error())
	}
	return edges
}

func (db *CFGNeo4j) VerifyBasicBlockBoundaries() error {
	result, err := db.Query("MATCH (b:Block), (nb: Block)\n"+
		"WHERE b.start < nb.start AND nb.start < b.end\n"+
		"RETURN nb.start AS overlapping_start", map[string]any{})

	if err != nil {
		panic(err.Error())
	}

	if len(result.Records) != 0 {
		return errors.New(fmt.Sprintf("found overlapping block start at %d", result.Records[0].AsMap()["overlapping_start"].(int64)))
	}

	return nil
}

func (db *CFGNeo4j) VerifyAll() error {
	err := db.VerifyBasicBlockBoundaries()
	if err != nil {
		return err
	}
	return nil
}
