package ssa_test

import (
	"discern/cfg"
	"discern/domtree"
	"discern/internal/id"
	"discern/internal/testcommon"
	"discern/ir"
	"discern/ssa"
	"fmt"
	"github.com/go-playground/assert/v2"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"testing"
)

func TestSSAOneBlock(t *testing.T) {
	tid := id.TestID{}

	dbcommon := testcommon.TestCommon{}
	dbtm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&dbtm, &dbcommon)
	loader := testcommon.MakeOneBlock(&tid, irdb)
	err := loader.Load()
	if err != nil {
		panic(err)
	}

	cfgdb := testcommon.NewCFGDBTest(irdb)

	cfgraph := cfg.CFG{CFGDB: cfgdb}
	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0, Space: ir.MemoryAddressSpace})
	if err != nil {
		panic(err)
	}

	dtdb := testcommon.NewTestDomTreeDB(&dbcommon, &dbtm, &cfgraph)
	dt := domtree.DomTree{
		Db: dtdb,
	}

	dt.Build()

	ssadb := testcommon.NewTestSSADB(irdb, &cfgraph, &dt)

	s := ssa.SSA{Db: ssadb}

	s.Build()

	root := cfgraph.RootBlockId.MustGet()

	lo.ForEach(ssadb.Entries[root], func(entry ssa.Entry, index int) {
		fmt.Printf("\t[%d] %s\n", index, entry.String())
	})
	assert.Equal(t, len(ssadb.Entries[root]), 4)

	ra := ssa.Variable{Name: "0x0:register", Version: 1, Operand: "1"}
	raDef := ssa.Entry{Defined: ra, SsaSemantic: ssa.OPERATION, IrSemantic: mo.Some(ir.MOV), Block: root, Position: 0, Inputs: []ssa.Variable{
		{Name: "imm(0000000000000004)", Version: 0, Operand: "9"},
	}}

	raDefActual, err := ssadb.GetDefinition(ra)
	assert.Equal(t, err, nil)
	assert.Equal(t, raDefActual.String(), raDef.String())

	rb := ssa.Variable{Name: "0x1:register", Version: 1, Operand: "3"}
	rbDef := ssa.Entry{Defined: rb, SsaSemantic: ssa.OPERATION, IrSemantic: mo.Some(ir.MOV), Block: root, Position: 1, Inputs: []ssa.Variable{
		{Name: "imm(0000000000000002)", Version: 0, Operand: "11"},
	}}

	rbDefActual, err := ssadb.GetDefinition(rb)
	assert.Equal(t, err, nil)
	assert.Equal(t, rbDefActual.String(), rbDef.String())

	raSum := ssa.Variable{Name: "0x0:register", Version: 2, Operand: "1"}
	raSumDef := ssa.Entry{Defined: raSum, SsaSemantic: ssa.OPERATION, IrSemantic: mo.Some(ir.ADD), Block: root, Position: 3, Inputs: []ssa.Variable{
		ra,
		rb,
	}}

	raSumActual, err := ssadb.GetDefinition(raSum)
	assert.Equal(t, err, nil)
	assert.Equal(t, raSumActual.String(), raSumDef.String())

	assert.Equal(t, ssadb.Entries[cfgraph.RootBlockId.MustGet()][0].String(), "0x0:register.1 (1) := MOV [imm(0000000000000004).0 (9)]")

	assert.Equal(t, ssadb.Entries[root][3].String(), "0x64:register.1 (20) := JMP [0xa:register.0 (7)]")
}

func TestSSAIterFib(t *testing.T) {
	tid := id.TestID{}

	dbcommon := testcommon.TestCommon{}
	dbtm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&dbtm, &dbcommon)
	loader := testcommon.MakeIterFib(&tid, irdb)
	err := loader.Load()
	if err != nil {
		panic(err)
	}

	cfgdb := testcommon.NewCFGDBTest(irdb)

	cfgraph := cfg.CFG{CFGDB: cfgdb}
	err = cfgraph.Build()
	if err != nil {
		panic(err)
	}

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0x10113a, Space: ir.MemoryAddressSpace})
	if err != nil {
		panic(err)
	}

	dtdb := testcommon.NewTestDomTreeDB(&dbcommon, &dbtm, &cfgraph)
	dt := domtree.DomTree{
		Db: dtdb,
	}

	dt.Build()

	ssadb := testcommon.NewTestSSADB(irdb, &cfgraph, &dt)

	s := ssa.SSA{Db: ssadb}

	s.Build()

	root := cfgraph.RootBlockId.MustGet()

	rootEntries, err := ssadb.GetBlockEntries(root)
	assert.Equal(t, err, nil)
	assert.Equal(t, len(rootEntries), 6)

	loopBody, err := cfgraph.GetBlockContaining(ir.Address{Offset: 0x10114c, Space: ir.MemoryAddressSpace})
	assert.Equal(t, err, nil)

	loopEntries, err := ssadb.GetBlockEntries(loopBody.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, len(loopEntries), 7)

	ret, err := cfgraph.GetBlockContaining(ir.Address{Offset: 0x101157, Space: ir.MemoryAddressSpace})
	assert.Equal(t, err, nil)

	retEntries, err := ssadb.GetBlockEntries(ret.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, len(retEntries), 3)

	rootTestStrings := ssadb.TestStrings(root)
	//loopBodyTestStrings := ssadb.TestStrings(loopBody.Id)
	//retTestStrings := ssadb.TestStrings(ret.Id)

	assert.Equal(t, rootTestStrings[0], "0x10:register.1 (7) := MOV [imm(0200000000000000).0 (26)]")
	assert.Equal(t, rootTestStrings[4], "0x3e8:register.1 (19) := LT [0x20:register.0 (13) 0x10:register.1 (7)]")
}
