package ssa

import (
	"discern/cfg"
	"discern/domtree"
	"discern/internal/id"
	"discern/ir"
	"fmt"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"github.com/zyedidia/generic/mapset"
)

type Variable struct {
	Name    string
	Version uint
	Operand id.UUID
}

func (v *Variable) String() string {
	return fmt.Sprintf("%s.%d (%s)", v.Name, v.Version, v.Operand)
}

func (v *Variable) FullName() string {
	return fmt.Sprintf("%s.%d", v.Name, v.Version)
}

type Semantic string

const (
	OPERATION Semantic = "OPERATION"
	PHI       Semantic = "PHI"
)

type Entry struct {
	Defined     Variable
	SsaSemantic Semantic
	IrSemantic  mo.Option[ir.Semantic]
	Inputs      []Variable
	Block       id.UUID
	Position    uint
}

func (e *Entry) String() string {
	semantic := string(e.SsaSemantic)
	if e.IrSemantic.IsPresent() {
		semantic = string(e.IrSemantic.MustGet())
	}
	return fmt.Sprintf("%s := %s %v", e.Defined.String(), semantic, lo.Map(e.Inputs, func(input Variable, _ int) string {
		return input.String()
	}))
}

type SSADB interface {
	GetCfg() *cfg.CFG
	GetDomTree() *domtree.DomTree
	GetIr() ir.IRDB

	SetBlockEntries(entries []Entry) error
	GetBlockEntries(block id.UUID) ([]Entry, error)
	GetDefinition(defined Variable) (Entry, error)
	GetUses(defined Variable) ([]Entry, error)
}

type SSA struct {
	Db SSADB
}

func (s *SSA) Build() {
	fmt.Println("[d] SSA: building")

	irdb := s.Db.GetIr()
	cfgraph := s.Db.GetCfg()
	dt := s.Db.GetDomTree()

	fmt.Println("[d] SSA: Preliminary Step: Set initial block entries")

	err := cfgraph.DepthFirstTraversal(func(block cfg.Block) {
		entries := MakeEntriesForBlock(irdb, &block)
		if len(entries) == 0 {
			fmt.Printf("[w] warning: block %s had no operations\n", block.String())
		}
		err := s.Db.SetBlockEntries(entries)
		if err != nil {
			panic(err)
		}
	})
	if err != nil {
		panic(err)
	}

	fmt.Println("[d] SSA: Step 1: Insert phi-nodes")
	// insert phi at each DF+(Defs(v))
	// insert phi nodes at the dominance frontier of all blocks where the variable is defined
	// Algorithm 3.1 from Springer SSA Book

	operands, err := irdb.GetOperands()
	if err != nil {
		panic(err)
	}
	for _, operandId := range operands {
		operand, err := irdb.GetOperand(operandId)
		if err != nil {
			panic(err)
		}
		worklist := make([]id.UUID, 0)
		phisPlaced := mapset.New[id.UUID]()

		operandWrites, err := irdb.GetOperandWrites(operandId)
		if err != nil {
			panic(err)
		}
		operandWriteIds := lo.Map(operandWrites, func(operation ir.Operation, _ int) id.UUID {
			return operation.Id
		})

		for _, def := range operandWrites {
			defBlock, err := cfgraph.GetBlockContainingIndex(def.Index)
			if err != nil {
				panic(err)
			}
			if lo.Contains(worklist, defBlock.Id) {
				continue
			}
			worklist = append(worklist, defBlock.Id)
		}

		for len(worklist) > 0 {
			block := worklist[0]
			worklist = worklist[1:]

			frontier, err := dt.Db.GetDominanceFrontier(block)
			if err != nil {
				continue
			}

			frontier.Each(func(frontierBlock id.UUID) {
				if phisPlaced.Has(frontierBlock) {
					return
				}

				entries, err := s.Db.GetBlockEntries(frontierBlock)
				if err != nil {
					panic(err)
				}

				currentVariable := NewVariableFromOperand(operand, 0)

				inputs := make([]Variable, 0)
				for _ = range len(cfgraph.GetPredecessors(frontierBlock)) {
					inputs = append(inputs, currentVariable)
				}
				entries = append([]Entry{{
					Defined:     currentVariable,
					SsaSemantic: PHI,
					Inputs:      inputs,
				}}, entries...)

				err = s.Db.SetBlockEntries(entries)
				if err != nil {
					panic(err)
				}

				phisPlaced.Put(frontierBlock)

				if lo.None(operandWriteIds, cfgraph.GetBlock(frontierBlock).Operations) {
					worklist = append(worklist, frontierBlock)
				}
			})
		}
	}

	fmt.Println("[d] SSA: Step 2: Re-Version variables")

	// Algorithm 3.3 from Springer SSA Book

	versionTracker := NewSSAVariableVersionTracker(cfgraph.RootBlockId.MustGet(), dt)

	s.Db.GetDomTree().DepthFirstTraversal(func(blockId id.UUID) {
		entries, err := s.Db.GetBlockEntries(blockId)
		if err != nil {
			panic(err)
		}

		reversionedEntries := make([]Entry, len(entries))

		// Re-version in this order:
		// Uses (non-Phi)
		// Definitions (incl Phi)
		// Uses (only Phi)

		for entryIndex, entry := range entries {
			if entry.SsaSemantic != PHI {
				entry.Inputs = lo.Map(entry.Inputs, func(input Variable, _ int) Variable {
					return Variable{Name: input.Name, Version: versionTracker.GetVersion(blockId, input.Name), Operand: input.Operand}
				})
			}

			entry.Defined.Version = versionTracker.NewDefinition(blockId, entry.Defined.Name)

			reversionedEntries[entryIndex] = entry
		}

		err = s.Db.SetBlockEntries(reversionedEntries)
		if err != nil {
			panic(err)
		}

		for _, successor := range cfgraph.GetSuccessors(blockId) {
			successorEntries, err := s.Db.GetBlockEntries(successor.Id)
			if err != nil {
				panic(err)
			}

			updatedEntries := make([]Entry, len(entries))

			for successorEntryIndex, successorEntry := range successorEntries {
				if successorEntry.SsaSemantic != PHI {
					continue
				}

				successorEntry.Inputs = lo.Map(successorEntry.Inputs, func(input Variable, _ int) Variable {
					return Variable{Name: input.Name, Version: versionTracker.GetVersion(blockId, input.Name), Operand: input.Operand}
				})

				updatedEntries[successorEntryIndex] = successorEntry
			}

			err = s.Db.SetBlockEntries(updatedEntries)
			if err != nil {
				panic(err)
			}
		}
	})

	fmt.Println("[d] SSA: build complete")
}

func NewVariableFromOperand(operand ir.Operand, version uint) Variable {
	return Variable{
		Name:    operand.Name(),
		Version: version,
		Operand: operand.GetId(),
	}
}

func MakeEntriesForBlock(irdb ir.IRDB, block *cfg.Block) []Entry {
	return lo.Map(block.Operations, func(operationId id.UUID, positionInBlock int) Entry {
		operation, err := irdb.GetOperation(operationId)
		if err != nil {
			panic(err)
		}

		var defined ir.Operand

		if operation.IsBranch() {
			defined, err = irdb.GetInstructionPointer()
			if err != nil {
				panic(err)
			}
		} else if operation.Output != nil {
			defined = *operation.Output
		} else {
			fmt.Printf("[!] error: operation %s has no output and will not be transformed into an SSA entry\n", operation.String())
			return Entry{Block: block.Id, Position: uint(positionInBlock)}
		}

		return Entry{
			Defined:     NewVariableFromOperand(defined, 0),
			SsaSemantic: OPERATION,
			IrSemantic:  mo.Some(operation.Semantic),
			Inputs: lo.Map(operation.Inputs, func(input ir.Operand, _ int) Variable {
				return NewVariableFromOperand(input, 0)
			}),
			Block:    block.Id,
			Position: uint(positionInBlock),
		}
	})
}

type SSAVariableVersionTracker struct {
	reachingDefinitions map[id.UUID]map[string]uint
	rootBlockId         id.UUID
	dt                  *domtree.DomTree
}

func NewSSAVariableVersionTracker(rootBlockId id.UUID, dt *domtree.DomTree) SSAVariableVersionTracker {
	return SSAVariableVersionTracker{reachingDefinitions: make(map[id.UUID]map[string]uint), rootBlockId: rootBlockId, dt: dt}
}

func (ssav *SSAVariableVersionTracker) GetVersion(blockId id.UUID, name string) uint {
	if _, initialized := ssav.reachingDefinitions[blockId]; !initialized {
		ssav.reachingDefinitions[blockId] = map[string]uint{}
	}

	if _, versionCached := ssav.reachingDefinitions[blockId][name]; versionCached {
		return ssav.reachingDefinitions[blockId][name]
	}

	currentDom := blockId
	var err error = nil
	for !lo.Contains(lo.Keys(ssav.reachingDefinitions[currentDom]), name) {
		if ssav.rootBlockId == currentDom {
			ssav.reachingDefinitions[currentDom][name] = 0
			ssav.reachingDefinitions[blockId][name] = 0
			return 0
		}
		currentDom, err = ssav.dt.Db.GetImmediateDominator(currentDom)
		if err != nil {
			panic(err)
		}
	}

	version := ssav.reachingDefinitions[currentDom][name]
	ssav.reachingDefinitions[blockId][name] = version
	return version
}

func (ssav *SSAVariableVersionTracker) NewDefinition(blockId id.UUID, name string) uint {
	lastVersion := ssav.GetVersion(blockId, name)
	newVersion := lastVersion + 1
	ssav.reachingDefinitions[blockId][name] = newVersion
	return newVersion
}
