package domtree_test

import (
	"discern/cfg"
	"discern/domtree"
	"discern/internal/id"
	"discern/internal/testcommon"
	"discern/ir"
	"github.com/go-playground/assert/v2"
	"github.com/zyedidia/generic/mapset"
	"testing"
)

func TestDomTreeBuild(t *testing.T) {
	tid := id.TestID{}

	dbcommon := testcommon.TestCommon{}
	dbtm := testcommon.TestTransactionManager{}

	irdb := testcommon.NewIRDBTest(&dbtm, &dbcommon)
	loader := testcommon.MakeComplex(&tid, irdb)
	err := loader.Load()
	assert.Equal(t, err, nil)

	cfgdb := testcommon.NewCFGDBTest(irdb)

	cfgraph := cfg.CFG{CFGDB: cfgdb}
	err = cfgraph.Build()
	assert.Equal(t, err, nil)

	err = cfgraph.SetEntrypoint(ir.Address{Offset: 0, Space: ir.MemoryAddressSpace})
	assert.Equal(t, err, nil)

	dtdb := testcommon.NewTestDomTreeDB(&dbcommon, &dbtm, &cfgraph)
	dt := domtree.DomTree{
		Db: dtdb,
	}

	dt.Build()

	assert.Equal(t, len(dtdb.ImmediateDominators), 13)
	//assert.Equal(t, len(dtdb.semiDominators), 13)

	k, err := cfgdb.GetBlockContainingIndex(0)
	assert.Equal(t, err, nil)
	kidom, err := dtdb.GetImmediateDominator(k.Id)
	assert.Equal(t, err, nil)
	// k dominates itself as root
	assert.Equal(t, kidom, k.Id)

	i, err := cfgdb.GetBlockContainingIndex(7)
	assert.Equal(t, err, nil)
	iidom, err := dtdb.GetImmediateDominator(i.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, iidom, k.Id)

	r, err := cfgdb.GetBlockContainingIndex(1)
	assert.Equal(t, err, nil)
	ridom, err := dtdb.GetImmediateDominator(r.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, ridom, k.Id)

	b, err := cfgdb.GetBlockContainingIndex(2)
	assert.Equal(t, err, nil)
	bidom, err := dtdb.GetImmediateDominator(b.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, bidom, r.Id)

	c, err := cfgdb.GetBlockContainingIndex(4)
	assert.Equal(t, err, nil)
	cidom, err := dtdb.GetImmediateDominator(c.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, cidom, r.Id)

	// H is POTENTIALLY semidominated by:
	// 1. E (predecessor of lower depth)
	// 2. D (semidominator of L, predecessor of higher depth)
	// D is also an ANCESTRAL semidominator because it is a semidominator of a potential semidominator
	// H is semidominated by E, because it is the shallowest of the potential semidominators
	// E is NOT an ancestral semidominator, so the immediate dominator of H is the immediate dominator of E,
	// which leads to B

	h, err := cfgdb.GetBlockContainingIndex(11)
	assert.Equal(t, err, nil)
	hidom, err := dtdb.GetImmediateDominator(h.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, hidom, b.Id)

	a, err := cfgdb.GetBlockContainingIndex(3)
	assert.Equal(t, err, nil)
	aidom, err := dtdb.GetImmediateDominator(a.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, aidom, b.Id)

	e, err := cfgdb.GetBlockContainingIndex(9)
	assert.Equal(t, err, nil)
	eidom, err := dtdb.GetImmediateDominator(e.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, eidom, b.Id)

	d, err := cfgdb.GetBlockContainingIndex(8)
	assert.Equal(t, err, nil)
	didom, err := dtdb.GetImmediateDominator(d.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, didom, a.Id)

	l, err := cfgdb.GetBlockContainingIndex(12)
	assert.Equal(t, err, nil)
	lidom, err := dtdb.GetImmediateDominator(l.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, lidom, d.Id)

	g, err := cfgdb.GetBlockContainingIndex(5)
	assert.Equal(t, err, nil)
	gidom, err := dtdb.GetImmediateDominator(g.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, gidom, c.Id)

	f, err := cfgdb.GetBlockContainingIndex(10)
	assert.Equal(t, err, nil)
	fidom, err := dtdb.GetImmediateDominator(f.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, fidom, c.Id)

	j, err := cfgdb.GetBlockContainingIndex(6)
	assert.Equal(t, err, nil)
	jidom, err := dtdb.GetImmediateDominator(j.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, jidom, g.Id)

	// root should not have a frontier despite having more than 2 incoming edges
	_, err = dtdb.GetDominanceFrontier(k.Id)
	assert.NotEqual(t, err, nil)

	// these blocks should not have DFs because they have < 2 incoming edges
	_, err = dtdb.GetDominanceFrontier(r.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(b.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(a.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(d.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(l.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(e.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(c.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(g.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(j.Id)
	assert.NotEqual(t, err, nil)
	_, err = dtdb.GetDominanceFrontier(f.Id)
	assert.NotEqual(t, err, nil)

	ifrontier, err := dtdb.GetDominanceFrontier(i.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, ifrontier.Size(), 5)
	assert.Equal(t, ifrontier, mapset.Of(f.Id, c.Id, r.Id, g.Id, j.Id))

	hfrontier, err := dtdb.GetDominanceFrontier(h.Id)
	assert.Equal(t, err, nil)
	assert.Equal(t, hfrontier.Size(), 4)
	assert.Equal(t, hfrontier, mapset.Of(e.Id, a.Id, d.Id, l.Id))
}
