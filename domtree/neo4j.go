package domtree

import (
	"discern/cfg"
	"discern/internal/db"
	"discern/internal/id"
	"errors"
	"github.com/zyedidia/generic/mapset"
)

type DomTreeNeo4jDB struct {
	*db.Neo4jDB
	Cfg *cfg.CFG
}

func (dtdb *DomTreeNeo4jDB) GetCfg() *cfg.CFG {
	return dtdb.Cfg
}

func (dtdb *DomTreeNeo4jDB) SetImmediateDominator(block id.UUID, immediateDominator id.UUID) error {
	_, err := dtdb.Query("MATCH (b:Block {id: $bid}), (idom:Block {id: $idomid})\n"+
		"CREATE (b)-[:HAS_IDOM]->(idom)", map[string]any{
		"bid":    block,
		"idomid": immediateDominator,
	})
	return err
}

func (dtdb *DomTreeNeo4jDB) GetImmediateDominator(block id.UUID) (id.UUID, error) {
	result, err := dtdb.Query("MATCH (b:Block {id: $bid})-[:HAS_IDOM]->(idom:Block)\n"+
		"RETURN idom.id AS idomid", map[string]any{"bid": block})
	if err != nil {
		return "", err
	}
	idomid, prsent := result.Records[0].Get("idomid")
	if !prsent {
		return "", errors.New("idom not found")
	}
	return id.UUID(idomid.(string)), nil
}

func (dtdb *DomTreeNeo4jDB) SetDominanceFrontier(block id.UUID, dominanceFrontier mapset.Set[id.UUID]) error {
	frontierSlice := make([]id.UUID, 0)
	dominanceFrontier.Each(func(dfbid id.UUID) {
		frontierSlice = append(frontierSlice, dfbid)
	})

	_, err := dtdb.Query("MATCH (b:Block {id: $bid}), (dfb:Block)\n"+
		"WHERE dfb.id IN $dfids\n"+
		"CREATE (b)-[:HAS_DOM_FRONTIER]->(dfb)", map[string]any{
		"bid":    block,
		"dfbids": dominanceFrontier,
	})
	return err
}

func (dtdb *DomTreeNeo4jDB) GetDominanceFrontier(block id.UUID) (mapset.Set[id.UUID], error) {
	result, err := dtdb.Query("MATCH (b:Block {id: $bid})-[:HAS_DOM_FRONTIER]->(dfb:BlocK)\n"+
		"RETURN dfb.id AS dfbids", map[string]any{"bid": block})
	if err != nil {
		return mapset.New[id.UUID](), err
	}

	frontier := mapset.New[id.UUID]()

	for _, record := range result.Records {
		dfbid, exists := record.Get("dfbids")
		if !exists {
			return mapset.New[id.UUID](), errors.New("dfbids not found")
		}
		frontier.Put(id.UUID(dfbid.(string)))
	}

	return frontier, nil
}
