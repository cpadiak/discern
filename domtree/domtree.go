package domtree

import (
	"discern/cfg"
	"discern/internal/db"
	"discern/internal/id"
	"fmt"
	"github.com/samber/lo"
	"github.com/zyedidia/generic/mapset"
)

type DomTreeDB interface {
	db.TransactionManager
	db.Common

	GetCfg() *cfg.CFG

	SetImmediateDominator(block id.UUID, immediateDominator id.UUID) error
	GetImmediateDominator(block id.UUID) (id.UUID, error)

	GetDominated(block id.UUID) ([]id.UUID, error)

	// Dominance Frontier: The set of blocks that dominate a predecessor of the given block
	// but do not stricly dominate the given block
	SetDominanceFrontier(block id.UUID, dominanceFrontier mapset.Set[id.UUID]) error
	GetDominanceFrontier(block id.UUID) (mapset.Set[id.UUID], error)
}

type DomTree struct {
	Db DomTreeDB
}

// use Cooper-Harvey-Kennedy algorithm
// https://www.cs.tufts.edu/comp/150FP/archive/keith-cooper/dom14.pdf
func (dt *DomTree) Build() {
	fmt.Println("[d] DOMTREE: building")
	cfgraph := dt.Db.GetCfg()

	dominators := make([]id.UUID, cfgraph.CountBlocks())
	dominators[0] = cfgraph.RootBlockId.MustGet()

	blocksDFS := make([]id.UUID, 0)
	err := cfgraph.DepthFirstTraversal(func(block cfg.Block) {
		blocksDFS = append(blocksDFS, block.Id)
	})
	if err != nil {
		panic(err)
	}

	getDepthForBlock := func(blockId id.UUID) int {
		if blockId == "" {
			panic("invalid block id")
		}
		blockDepth := lo.IndexOf(blocksDFS, blockId)
		if blockDepth < 0 {
			panic(fmt.Sprintf("could not find block depth %s", blockId))
		}
		return blockDepth
	}

	intersect := func(uDepth int, vDepth int) id.UUID {
		//fmt.Printf("intersecting %d %d\n", uDepth, vDepth)
		leftFinger := uDepth
		rightFinger := vDepth
		for leftFinger != rightFinger {
			for leftFinger < rightFinger {
				rightFinger = getDepthForBlock(dominators[rightFinger])
			}
			for rightFinger < leftFinger {
				leftFinger = getDepthForBlock(dominators[leftFinger])
			}
		}
		return blocksDFS[leftFinger]
	}

	changed := true
	iters := 0
	for changed {
		iters += 1
		//fmt.Printf("iteration: %d\n", iters)
		changed = false

		for _, p := range lo.Reverse(lo.Zip2(lo.Range(len(blocksDFS))[1:], blocksDFS[1:])) {
			depth, blockId := p.Unpack()
			//fmt.Println(depth)
			predecessors := cfgraph.GetPredecessors(blockId)

			processedPredecessors := lo.Filter(predecessors, func(block cfg.Block, _ int) bool {
				return dominators[getDepthForBlock(block.Id)] != ""
			})

			var newIdom id.UUID = ""
			if len(processedPredecessors) > 0 {
				newIdom = processedPredecessors[0].Id
				processedPredecessors = processedPredecessors[1:]
			}

			for _, predecessor := range processedPredecessors {
				predecessorDepth := getDepthForBlock(predecessor.Id)
				newIdom = intersect(predecessorDepth, getDepthForBlock(newIdom))
			}

			if dominators[depth] != newIdom {
				dominators[depth] = newIdom
				changed = true
			}
		}
	}

	fmt.Printf("[d] DOMTREE: built in %d iterations, saving to DB\n", iters)
	for depth, domId := range dominators {
		err = dt.Db.SetImmediateDominator(blocksDFS[depth], domId)
		if err != nil {
			panic(err)
		}
	}

	fmt.Println("[d] DOMTREE: calculating dominance frontiers")

	for _, blockId := range blocksDFS[1:] {
		predecessors := cfgraph.GetPredecessors(blockId)
		if len(predecessors) < 2 {
			continue
		}

		// use algorithm given by Cooper-Harvey-Kennedy in Figure 5:
		// https://www.cs.tufts.edu/comp/150FP/archive/keith-cooper/dom14.pdf
		//	for all nodes b
		//		if len(pred(b)) > 2
		//			for p in pred(b)
		//				runner = p
		//				while runner != idom(b)
		//					domFrontier[runner] append b
		//					runner = idom(runner)

		blockIdom, err := dt.Db.GetImmediateDominator(blockId)
		if err != nil {
			panic(err)
		}

		frontier := mapset.New[id.UUID]()

		// simply walk up the idom tree for each predecessor until we hit
		// the given block's idom
		for _, predecessor := range predecessors {
			potentialFrontierBlock := predecessor.Id
			if potentialFrontierBlock == cfgraph.RootBlockId.MustGet() {
				continue
			}
			for potentialFrontierBlock != blockIdom {
				frontier.Put(potentialFrontierBlock)
				potentialFrontierBlock, err = dt.Db.GetImmediateDominator(potentialFrontierBlock)
				if err != nil {
					panic(err)
				}
			}
		}

		err = dt.Db.SetDominanceFrontier(blockId, frontier)
		if err != nil {
			return
		}
	}

	fmt.Println("[d] DOMTREE: done")
}

// TODO: This is a broken implementation of Lengauer-Tarjan's "A Fast Algorithm for Finding Dominators in a Flowgraph"
// Would really like to fix this someday so we have the option and can use it on potentially large datasets where it
// theoretically is faster
//func (dt *DomTree) Build() {
//	fmt.Println("[d] building dominator tree")
//
//	cfgraph := dt.Db.GetCfg()
//
//	// STEP 1
//	// Number every block by depth first search
//
//	blocksDFSOrdered := make([]id.UUID, 0)
//
//	visitor := func(block cfg.Block) {
//		blocksDFSOrdered = append(blocksDFSOrdered, block.Id)
//	}
//
//	fmt.Println("[d] DOMTREE: Step 1: Numbering blocks")
//
//	err := cfgraph.DepthFirstTraversal(visitor)
//	if err != nil {
//		panic(err)
//	}
//
//	depth := len(blocksDFSOrdered)
//
//	// STEP 2
//	// Calculate semidominator for each block
//	// For w != r
//	// sdom(w) = min(
//	//	{v | (v,w) of E and v < w} // v is a CFG predecessor and depth(v) < depth(w)
//	//	union
//	//	{sdom(u) | u > w and there is an edge (v,w) such that u is ancestor/equal v} // sdom(u) where depth(u) > depth(w) and u is an ancestor of any CFG predecessor of w or a predecessor of w itself
//	// )
//
//	findShallowestBlock := func(blocks []id.UUID) id.UUID {
//		for _, blockId := range blocksDFSOrdered {
//			if lo.Contains(blocks, blockId) {
//				return blockId
//			}
//		}
//		panic(fmt.Sprintf("could not find any block from collection in blocks by depth! %s", blocks))
//	}
//
//	getDepthForBlock := func(blockId id.UUID) uint {
//		blockDepth := lo.IndexOf(blocksDFSOrdered, blockId)
//		if blockDepth < 0 {
//			panic(fmt.Sprintf("could not find block depth %s", blockId))
//		}
//		return uint(blockDepth)
//	}
//
//	semiDominators := map[id.UUID]id.UUID{}
//	ancestralSemiDominators := multimap.NewMapSlice[id.UUID, id.UUID]()
//
//	fmt.Println("[d] DOMTREE: Step 2: Calculating semidominators")
//
//	// skip root because it has no dominators
//	reverseDFSOrderBlocks := lo.Reverse(lo.Zip2(lo.RangeFrom(1, depth), blocksDFSOrdered[1:]))
//	for _, p := range reverseDFSOrderBlocks {
//		blockDepth, blockId := p.Unpack()
//		bdepth := uint(blockDepth)
//		//fmt.Printf("depth: %d block: %s\n", bdepth, blockId)
//		ancestors := cfgraph.GetPredecessors(blockId)
//
//		// The semidominator for any given block is the shallowest block of all the following:
//		// 1) ancestors shallower (forward edge) than the block
//		// 2) semidominators of an ancestor that is deeper (back or cross edge) than the block
//		// By processing from deepest to shallowest, we ensure that we have already calculated
//		// the semidominators for all deeper ancestors.
//		// Also, blocks shallower than the current block will not have been processed but that
//		// doesn't matter because shallower ancestors are easily semidominators. It's the ones
//		// that are deeper and across (to the right in) the Depth-First-Tree that are ancestral
//		// and require a lookup for their semidominator
//
//		var potentialSemiDominators []id.UUID
//
//		for _, ancestor := range ancestors {
//			ancestorId := ancestor.Id
//			ancestorDepth := getDepthForBlock(ancestorId)
//
//			if ancestorDepth < bdepth {
//				// it hasn't been processed yet
//				potentialSemiDominators = append(potentialSemiDominators, ancestorId)
//			} else {
//				// it has already been processed and is either
//				// a back-edge or cross-edge
//				ancestralSemiDom := semiDominators[ancestorId]
//				potentialSemiDominators = append(potentialSemiDominators, ancestralSemiDom)
//				ancestralSemiDominators.Put(blockId, ancestralSemiDom)
//			}
//		}
//
//		semiDominator := findShallowestBlock(potentialSemiDominators)
//		semiDominators[blockId] = semiDominator
//	}
//
//	// STEP 3
//	// Calculate immediate dominator for each block
//	fmt.Println("[d] DOMTREE: Step 3: Calculating immediate dominators")
//
//	immediateDominators := map[id.UUID]id.UUID{}
//
//	// The immediate dominator is the dominator for the given block
//	// that itself is dominated by all other dominators of the block.
//	// sdom(w) is an ancestor of u (and not equal to u) and u is an ancestor of w (possibly equal to w) then
//	//
//
//	// skip root because it has no dominators
//	for _, blockId := range blocksDFSOrdered[1:] {
//		semiDominator, sdomFound := semiDominators[blockId]
//		if !sdomFound {
//			panic(fmt.Sprintf("semidominator not found for %s", blockId))
//		}
//		if semiDominator == blocksDFSOrdered[0] {
//			immediateDominators[blockId] = semiDominator
//		}
//		if lo.Contains(ancestralSemiDominators.Get(blockId), semiDominator) {
//			immediateDominators[blockId] = semiDominator
//		} else {
//			immediateDominators[blockId] = immediateDominators[semiDominator]
//		}
//	}
//
//	// STEP 4
//	// Commit dominators to DomTree
//	fmt.Println("[d] DOMTREE: Step 4: Committing dominators to DB")
//
//	for blockId, idom := range immediateDominators {
//		dt.Db.SetImmediateDominator(blockId, idom)
//	}
//
//	for blockid, sdom := range semiDominators {
//		dt.Db.SetSemiDominator(blockid, sdom)
//	}
//}

func (dt *DomTree) DepthFirstTraversal(visitor func(blockId id.UUID)) {
	if dt.Db.GetCfg().RootBlockId.IsAbsent() {
		panic(fmt.Sprintf("root block must be set on CFG"))
	}

	qu := []id.UUID{dt.Db.GetCfg().RootBlockId.MustGet()}
	visited := mapset.New[id.UUID]()

	for len(qu) > 0 {
		currentBlockId := qu[0]
		qu = qu[1:]

		if visited.Has(currentBlockId) {
			continue
		}

		visitor(currentBlockId)
		visited.Put(currentBlockId)

		dominated, err := dt.Db.GetDominated(currentBlockId)
		if err != nil {
			panic(err)
		}

		qu = append(qu, dominated...)
	}
}
