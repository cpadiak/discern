FROM docker.io/docker:27.3.1

RUN apk update

RUN apk add curl

RUN curl -OL https://go.dev/dl/go1.23.2.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go1.23.2.linux-amd64.tar.gz && \
    ln -s /usr/local/go/bin/go /usr/local/bin/go

RUN go install gotest.tools/gotestsum@latest && \
    cp /root/go/bin/gotestsum /usr/local/bin/